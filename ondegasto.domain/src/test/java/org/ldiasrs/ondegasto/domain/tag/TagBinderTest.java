package org.ldiasrs.ondegasto.domain.tag;

import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;

import org.junit.Test;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * @author ld24078
 */
public class TagBinderTest {

    @Test
    public void testGetTagNamesAsString() {
        User userArg = new User();
        TagBinder binder = new TagBinder(userArg, "posto");
        Collection<Tag> tags = new ArrayList<Tag>();
        tags.add(new Tag("a", userArg));
        tags.add(new Tag("a", userArg));
        tags.add(new Tag("b", userArg));
        tags.add(new Tag("c", userArg));
        binder.bindWithTags(tags);
        Assert.assertEquals("a, b, c", binder.getTagNamesAsString());
    }

}
