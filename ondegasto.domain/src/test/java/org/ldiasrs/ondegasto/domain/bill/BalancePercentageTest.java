package org.ldiasrs.ondegasto.domain.bill;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;

import org.junit.Test;
import org.ldiasrs.ondegasto.domain.user.User;

public class BalancePercentageTest {

    @Test
    public void shouldCalculateParcentageOfBalance() {
        User user = new User("userTest");
        Bill rentPaid = new Bill(user, "rentPaid", BigDecimal.valueOf(800), BillType.RENT);
        rentPaid.pay();
        Bill rentUnPaid = new Bill(user, "rentUnPaid", BigDecimal.valueOf(200), BillType.RENT);

        Bill costPaid = new Bill(user, "costPaid", BigDecimal.valueOf(10), BillType.COST);
        costPaid.pay();
        Bill costUnPaid = new Bill(user, "costUnPaid", BigDecimal.valueOf(5), BillType.COST);

        Bill investementPaid = new Bill(user, "investementPaid", BigDecimal.valueOf(100), BillType.INVESTMENT);
        investementPaid.pay();
        Bill investementUnPaid = new Bill(user, "investementUnPaid", BigDecimal.valueOf(50), BillType.INVESTMENT);

        Collection<Bill> bills = createCollectionWithBills(rentPaid, rentUnPaid, costPaid, costUnPaid, investementPaid, investementUnPaid);

        Balance balance = new Balance(bills);
        BalancePercentage percentage = new BalancePercentage(balance);

        Assert.assertEquals(1.0f, percentage.calculatePercentageOfPaidBillType(BillType.COST));
        Assert.assertEquals(0.5f, percentage.calculatePercentageOfUnPaidBillType(BillType.COST));

        Assert.assertEquals(10.0f, percentage.calculatePercentageOfPaidBillType(BillType.INVESTMENT));
        Assert.assertEquals(5.0f, percentage.calculatePercentageOfUnPaidBillType(BillType.INVESTMENT));

        Assert.assertEquals(80.0f, percentage.calculatePercentageOfPaidBillType(BillType.RENT));
        Assert.assertEquals(20.0f, percentage.calculatePercentageOfUnPaidBillType(BillType.RENT));

        Assert.assertEquals(69.0f, percentage.calculateParcialBalancePercentage());
        Assert.assertEquals(83.5f, percentage.calculateFinalBalancePercentage());
    }

    private Collection<Bill> createCollectionWithBills(final Bill rentPaid, final Bill rentUnPaid, final Bill costPaid, final Bill costUnPaid,
            final Bill investementPaid, final Bill investementUnPaid) {
        Collection<Bill> bills = new ArrayList<Bill>();
        bills.add(rentPaid);
        bills.add(rentUnPaid);
        bills.add(costPaid);
        bills.add(costUnPaid);
        bills.add(investementPaid);
        bills.add(investementUnPaid);
        return bills;
    }
}
