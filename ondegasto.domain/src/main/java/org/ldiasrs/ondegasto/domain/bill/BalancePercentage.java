package org.ldiasrs.ondegasto.domain.bill;

import java.math.BigDecimal;

import org.ldiasrs.ondegasto.common.CalculatePercentage;

public class BalancePercentage {

    private final Balance balance;
    private final BigDecimal totalRent;

    public BalancePercentage(final Balance balance) {
        this.balance = balance;
        totalRent = balance.sumAll(BillType.RENT);
    }

    public float calculatePercentageOfUnPaidBillType(final BillType billType) {
        if (thereIsNoRent()) {
            return 0f;
        }
        return CalculatePercentage.calculatePercentage(getTotalRent(), getBalance().getSumNotPayed(billType));
    }

    private boolean thereIsNoRent() {
        return BigDecimal.ZERO.compareTo(getTotalRent()) >= 0;
    }

    public float calculatePercentageOfPaidBillType(final BillType billType) {
        if (thereIsNoRent()) {
            return 0f;
        }
        return CalculatePercentage.calculatePercentage(getTotalRent(), getBalance().getSumPayed(billType));
    }

    public float calculateParcialBalancePercentage() {
        if (thereIsNoRent()) {
            return 0f;
        }
        return CalculatePercentage.calculatePercentage(getTotalRent(), getBalance().calculatePartialBalance());
    }

    public float calculateFinalBalancePercentage() {
        if (thereIsNoRent()) {
            return 0f;
        }
        return CalculatePercentage.calculatePercentage(getTotalRent(), getBalance().calculateFinalBalance());
    }

    private Balance getBalance() {
        return balance;
    }

    public BigDecimal getTotalRent() {
        return totalRent;
    }
}
