package org.ldiasrs.ondegasto.domain.tag;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Implementação padrão da Tag.
 * @author cardoso
 */
@Entity
@Table(name = "tag")
public class Tag implements Serializable {

    private static final long serialVersionUID = 9028475345020576196L;

    @Id
    private String id;

    private final String name;

    @ManyToOne
    private final User user;

    public Tag(final String nameArg, final User userArg) {
        name = nameArg;
        user = userArg;
        id = userArg.getLoginName() + "-" + nameArg; //$NON-NLS-1$
    }

    public Tag() {
        this("default", User.createSystemUser()); //$NON-NLS-1$
    }

    public String getName() {
        return name;
    }

    public User getUser() {
        return user;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return getName();
    }

    public void setId(final String idArg) {
        id = idArg;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id == null ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Tag other = (Tag) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }
}
