package org.ldiasrs.ondegasto.domain.bill;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Representa uma conta que se repete de acordo com o numero de vezes.
 * @author cardoso
 */
@Entity
@Table(name = "repeated_bill")
public class RepeatedBill extends AbstractEntity {

    private static final long serialVersionUID = -5654986613153003753L;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private final Collection<Bill> repeatedBills;

    @Transient
    private final Collection<Bill> removedCacheBills;

    @Transient
    private User user;

    public RepeatedBill() {
        repeatedBills = new ArrayList<Bill>();
        removedCacheBills = new ArrayList<Bill>();
    }

    public RepeatedBill(final Bill billArg, final int numberOfRepetitionArg) {
        repeatedBills = new ArrayList<Bill>();
        removedCacheBills = new ArrayList<Bill>();
        if (numberOfRepetitionArg > 0) {
            addRepeatedBills(billArg, numberOfRepetitionArg);
        } else {
            throw new InvalidParameterException("Number of repetition must be greater than zero");
        }
    }

    private void addRepeatedBills(final Bill billArg, final int numberOfRepetitionArg) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(billArg.getDateOfBill());
        for (int i = 0; i < numberOfRepetitionArg; i++) {
            Bill repeatbill = new Bill(billArg);
            repeatbill.setId(0);
            repeatedBills.add(repeatbill);
            if (isNotTheFirst(i)) {
                calendar.add(Calendar.MONTH, 1);
                repeatbill.setDateOfBill(calendar.getTime());
            } else {
                user = repeatbill.getUser();
            }
        }
    }

    public void changeValueAndNameFromAllBills(final Bill domainBill) {
        for (Bill bill : getRepeatedBills()) {
            changeValues(domainBill, bill);
        }
    }

    public void changeValueAndNameFromNewersBills(final Bill domainBill) {
        for (Bill bill : getNewersBillsComparingYearAndMonth(domainBill.getDateOfBill())) {
            changeValues(domainBill, bill);
        }
    }

    private void changeValues(final Bill billToCopy, final Bill billToChange) {
        billToChange.setName(billToCopy.getName());
        billToChange.setValueOfBill(billToCopy.getValueOfBill());
        billToChange.setTagList(billToCopy.getTagList());
        billToChange.setObservation(billToCopy.getObservation());
    }

    private boolean isNotTheFirst(final int i) {
        return i > 0;
    }

    public boolean removeBill(final Bill billToRemove) {
        for (Bill bill : getRepeatedBills()) {
            if (bill.equals(billToRemove)) {
                removedCacheBills.add(bill);
                return getRepeatedBills().remove(bill);
            }
        }
        return false;
    }

    public void removeNewerThan(final Calendar calendar) {
        Collection<Bill> beansToRemove = getNewersBillsComparingYearAndMonth(calendar);
        for (Bill bill : beansToRemove) {
            this.removeBill(bill);
        }
    }

    public Collection<Bill> getNewersBillsComparingYearAndMonth(final Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return getNewersBillsComparingYearAndMonth(calendar);
    }

    public Collection<Bill> getNewersBillsComparingYearAndMonth(final Calendar calendar) {
        Collection<Bill> beansToRemove = new ArrayList<Bill>();
        for (Bill bill : getRepeatedBills()) {
            Calendar dateOfBill = new GregorianCalendar();
            dateOfBill.setTime(bill.getDateOfBill());
            if (isYearBiggerThan(calendar, dateOfBill)) {
                beansToRemove.add(bill);
            } else if (isTheSameYear(calendar, dateOfBill) && isMonthEqualsOrBiggerThan(calendar, dateOfBill)) {
                beansToRemove.add(bill);
            }
        }
        return beansToRemove;
    }

    private boolean isYearBiggerThan(final Calendar calendar, final Calendar dateOfBill) {
        return dateOfBill.get(Calendar.YEAR) > calendar.get(Calendar.YEAR);
    }

    private boolean isMonthEqualsOrBiggerThan(final Calendar calendar, final Calendar dateOfBill) {
        return dateOfBill.get(Calendar.MONTH) >= calendar.get(Calendar.MONTH);
    }

    private boolean isTheSameYear(final Calendar calendar, final Calendar dateOfBill) {
        return dateOfBill.get(Calendar.YEAR) == calendar.get(Calendar.YEAR);
    }

    public void addTagOnBills(final Tag tag) {
        for (Bill bean : getRepeatedBills()) {
            bean.addTag(tag);
        }
    }

    public Collection<Bill> getRepeatedBills() {
        return repeatedBills;
    }

    public Collection<Bill> getRemovedCacheBills() {
        return removedCacheBills;
    }

    public User getUser() {
        return user;
    }

}
