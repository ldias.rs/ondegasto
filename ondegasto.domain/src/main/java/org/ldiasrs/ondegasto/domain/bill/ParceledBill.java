/**
 *
 */
package org.ldiasrs.ondegasto.domain.bill;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ldiasrs.ondegasto.common.NumberNormalizer;

/**
 * Administra uma conta parcelada.
 * @author cardoso
 */
@Entity
@Table(name = "parceled_bill")
public class ParceledBill extends AbstractEntity {

    private static final long serialVersionUID = 3506333298007809866L;
    public static final String PROPERTY_PARCELED_BILLS = "parceledBills"; //$NON-NLS-1$
    public static final String PROPERTY_CLASS_NAME = "ParceledBill"; //$NON-NLS-1$

    @Column
    private BigDecimal inValue;
    @Column
    private final int numberOfParceled;
    @OneToMany(orphanRemoval = true, cascade = javax.persistence.CascadeType.ALL)
    private final Collection<Bill> parceledBills;

    public ParceledBill() {
        inValue = BigDecimal.valueOf(-1);
        numberOfParceled = -1;
        parceledBills = new ArrayList<Bill>();
    }

    public ParceledBill(final Bill bill, final BigDecimal inValueArg, final int numberOfParceledArg) {
        inValue = inValueArg;
        numberOfParceled = numberOfParceledArg;
        parceledBills = new ArrayList<Bill>();
        inValue = inValueArg;
        if (inValueArg.intValue() > 0) {
            addInBill(bill);
        }
        if (numberOfParceledArg > 0) {
            addParceledBills(bill);
        } else {
            throw new InvalidParameterException("Number of parceled should be greater than zero");
        }
    }

    private void addParceledBills(final Bill bill) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(bill.getDateOfBill());
        BigDecimal parceledValue = calculateParcelValue(bill.getValueOfBill(), inValue, numberOfParceled);
        for (int i = 1; i <= numberOfParceled; i++) {
            Bill parceledBill = new Bill(bill);
            parceledBill.setId(0);
            parceledBill.setValueOfBill(parceledValue);
            String name = String.format("%s %s/%s", bill.getName(), String.valueOf(i), String.valueOf(numberOfParceled)); //$NON-NLS-1$
            parceledBill.setName(name);
            calendar.add(Calendar.MONTH, 1);
            parceledBill.setDateOfBill(calendar.getTime());
            parceledBills.add(parceledBill);
        }
    }

    private void addInBill(final Bill bill) {
        Bill inBill = new Bill(bill);
        inBill.setValueOfBill(inValue);
        inBill.setId(0);
        String name = String.format("%s 0/%s", bill.getName(), String.valueOf(numberOfParceled)); //$NON-NLS-1$
        inBill.setName(name);
        parceledBills.add(inBill);
    }

    public BigDecimal getInValue() {
        return inValue;
    }

    public int getNumberOfParceled() {
        return numberOfParceled;
    }

    public Collection<Bill> getParceledBills() {
        return parceledBills;
    }

    public void removeBill(final Bill bill) {
        parceledBills.remove(bill);
    }

    public static BigDecimal calculateParcelValue(final BigDecimal totalValue, final BigDecimal inValue, final int numberOfParcels) {
        return NumberNormalizer.normalize(totalValue.subtract(inValue).divide(BigDecimal.valueOf(numberOfParcels), 2, RoundingMode.HALF_UP));
    }
}
