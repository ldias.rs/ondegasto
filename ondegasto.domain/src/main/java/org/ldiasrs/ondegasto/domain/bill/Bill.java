package org.ldiasrs.ondegasto.domain.bill;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ldiasrs.ondegasto.common.NumberNormalizer;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Implementação default para a conta.
 * @author cardoso
 */
@Entity
@Table(name = "bill")
public class Bill extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4501747786227570964L;
    public static final String PROPERTY_NAME = "name"; //$NON-NLS-1$
    public static final String PROPERTY_VALUEOFBILL = "valueOfBill"; //$NON-NLS-1$
    public static final String PROPERTY_DATEOFBILL = "dateOfBill"; //$NON-NLS-1$
    public static final String PROPERTY_OBSERVATION = "observation"; //$NON-NLS-1$
    public static final String PROPERTY_TYPE = "type"; //$NON-NLS-1$
    public static final String PROPERTY_TAGLIST = "tagList"; //$NON-NLS-1$
    public static final String PROPERTY_PAID = "paid"; //$NON-NLS-1$
    public static final String PROPERTY_USER = "user"; //$NON-NLS-1$

    private String name;

    @Column(name = "value_of_bill")
    private BigDecimal valueOfBill;

    @Column(name = "date_of_bill")
    private Date dateOfBill;

    private String observation;

    private BillType type;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Tag> tagList = new HashSet<Tag>();

    private boolean paid;

    @ManyToOne
    private User user;

    public Bill() {
    }

    public Bill(final User userArg, final String nameArg, final double valueArg, final BillType typeArg) {
        this(userArg, nameArg, BigDecimal.valueOf(valueArg), typeArg);
    }

    public Bill(final Bill clone) {
        this(clone.getUser(), clone.getName(), clone.getValueOfBill(), clone.getDateOfBill(), clone.getType(), clone.getObservation(),
                new HashSet<Tag>(clone.getTagList()), clone.isPaid(), clone.getId());

    }

    public Bill(final User userArg, final String nameArg, final BigDecimal valueArg, final BillType typeArg) {
        this(userArg, nameArg, valueArg, GregorianCalendar.getInstance().getTime(), typeArg, "", new HashSet<Tag>(), false, -1L); //$NON-NLS-1$
    }

    public Bill(final User userAeg, final String nameAeg, final BigDecimal valueArg, final Date dateArg, final BillType typeArg,
            final String observationArg, final Set<Tag> tagListArg, final boolean paidArg, final long idArg) {
        setName(nameAeg);
        setValueOfBill(valueArg);
        setDateOfBill(dateArg);
        setType(typeArg);
        setObservation(observationArg);
        setUser(userAeg);
        setTagList(tagListArg);
        setPaid(paidArg);
        setId(idArg);
    }

    public Tag getTagByName(final String tagName) {
        for (Tag tag : getTagList()) {
            if (tag.getName().equals(tagName)) {
                return tag;
            }
        }
        return null;
    }

    public BigDecimal getValueOfBill() {
        return valueOfBill;
    }

    public String getName() {
        return name;
    }

    public Set<Tag> getTagList() {
        return tagList;
    }

    public void setTagList(final Set<Tag> tagListArg) {
        tagList = tagListArg;
    }

    public void addTag(final Tag m) {
        tagList.add(m);
    }

    public void addTags(final Collection<Tag> tagListArg) {
        tagList.addAll(tagListArg);
    }

    public Date getDateOfBill() {
        return dateOfBill;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(final String observationArg) {
        observation = observationArg;
    }

    public void setName(final String nameArg) {
        name = nameArg;
    }

    public void setValueOfBill(final BigDecimal value) {
        valueOfBill = NumberNormalizer.normalize(value);
    }

    public void setDateOfBill(final Date date) {
        dateOfBill = date;
    }

    public BillType getType() {
        return type;
    }

    public void setType(final BillType typeArg) {
        type = typeArg;
    }

    public User getUser() {
        return user;
    }

    public void setUser(final User userArg) {
        user = userArg;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(final boolean paidArg) {
        paid = paidArg;
    }

    public void pay() {
        setPaid(true);
    }

    public void unPay() {
        setPaid(false);
    }

    public void removeAllTags() {
        getTagList().clear();
    }

    @Override
    public String toString() {
        return String.format(
                "Bill [id=%s, name=%s, value=%s, date=%s, observation=%s, type=%s, paid=%s, user=%s, tagList=%s", //$NON-NLS-1$
                String.valueOf(getId()), name, String.valueOf(getValueOfBill()), getDateOfBill().toString(), observation, getType().getLabel(),
                String.valueOf(isPaid()), getUser().getLoginName(), getTagList().toString());
    }

    public static void sortBillsByDate(final List<Bill> bills) {
        Collections.sort(bills, new Comparator<Bill>() {
            @Override
            public int compare(final Bill o1, final Bill o2) {
                return o1.getDateOfBill().compareTo(o2.getDateOfBill());
            }
        });
    }
}
