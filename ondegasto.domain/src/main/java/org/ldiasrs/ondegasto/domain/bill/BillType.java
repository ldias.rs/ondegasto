package org.ldiasrs.ondegasto.domain.bill;

/**
 * Indica os tipos que uma conta pode assumir.
 * @author cardoso
 *
 */
public enum BillType {

    COST("Despesa"),
    RENT("Renda"),
    INVESTMENT("Investimento");

    private final String label;

    private BillType(final String labelArg) {
        this.label = labelArg;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return getLabel();
    }

}
