package org.ldiasrs.ondegasto.domain.tag;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.ldiasrs.ondegasto.common.CollectionUtils;
import org.ldiasrs.ondegasto.common.LabelResolver;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * @author ld24078
 */
@Entity
public class TagBinder {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private final User user;

    private final String name;

    @ManyToMany(fetch = FetchType.EAGER)
    private final Set<Tag> tags;

    public TagBinder() {
        this(User.createSystemUser(), "default"); //$NON-NLS-1$
    }

    public TagBinder(final User userArg, final String nameArg) {
        user = userArg;
        name = nameArg.toLowerCase();
        tags = new HashSet<Tag>();
    }

    public void bindWithTag(final Tag tag) {
        tags.add(tag);
    }

    public void bindWithTags(final Collection<Tag> tagsArgs) {
        for (Tag tag : tagsArgs) {
            bindWithTag(tag);
        }
    }

    public String getTagNamesAsString() {
        LabelResolver<Tag> labelResolver = new LabelResolver<Tag>() {
            @Override
            public String getLabel(final Tag objectThatWeWantLabel) {
                return objectThatWeWantLabel.getName();
            }
        };
        return CollectionUtils.toString(tags, labelResolver, "", "");
    }

    public String getName() {
        return name;
    }

    public User getUser() {
        return user;
    }

    public Long getId() {
        return id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id == null ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TagBinder other = (TagBinder) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public Iterator<Tag> tagNamesIteartor() {
        return tags.iterator();
    }

    @Override
    public String toString() {
        return "TagBinder [id=" + id + ", user=" + user + ", name=" + name + ", tags=" + tags.size() + "]";
    }

}
