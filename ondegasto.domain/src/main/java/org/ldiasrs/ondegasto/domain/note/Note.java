package org.ldiasrs.ondegasto.domain.note;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Representa uma nota de usuario.
 * @author ld24078
 */
@Entity
@Table(name = "note")
public class Note implements Serializable {

    private static final long serialVersionUID = -8487568432749777187L;

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @Column(length = 32600)
    private String html;

    @ManyToOne
    private User user;

    public Note() {
    }

    public Note(final User userArg, final String nameArg, final String htmlArg) {
        user = userArg;
        name = nameArg;
        html = htmlArg;
    }

    public String getHtml() {
        return html;
    }

    public String getName() {
        return name;
    }

    public void setHtml(final String htmlArg) {
        html = htmlArg;
    }
}
