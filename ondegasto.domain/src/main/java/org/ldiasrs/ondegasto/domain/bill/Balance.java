package org.ldiasrs.ondegasto.domain.bill;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.ldiasrs.ondegasto.common.NumberNormalizer;

/**
 * Representa uma lista de balanços de contas.
 * @author cardoso
 */
public class Balance {

    private final Map<BillType, BigDecimal> sumNotPayedMap;
    private final Map<BillType, BigDecimal> sumPayedMap;

    public Balance(final Collection<Bill> bills) {
        sumNotPayedMap = new HashMap<BillType, BigDecimal>();
        sumPayedMap = new HashMap<BillType, BigDecimal>();
        addBillTypes();
        addBills(bills);
    }

    public BigDecimal sumAll(final BillType billType) {
        BigDecimal sumOfCosts = sumNotPayedMap.get(billType);
        sumOfCosts = sumOfCosts.add(sumPayedMap.get(billType));
        return sumOfCosts;
    }

    public BigDecimal calculatePartialBalance() {
        BigDecimal sumOfCosts = getSumPayed(BillType.COST);
        BigDecimal sumOfInvestments = getSumPayed(BillType.INVESTMENT);
        BigDecimal sumOfRent = getSumPayed(BillType.RENT);
        return sumOfRent.subtract(sumOfCosts).subtract(sumOfInvestments);
    }

    public BigDecimal calculateFinalBalance() {
        // sum all COST payed an not payed

        BigDecimal sumOfCosts = sumAll(BillType.COST);

        // sum all INVESTMENT payed an not payed
        BigDecimal sumOfInvestments = sumAll(BillType.INVESTMENT);

        // sum all RENT payed an not payed
        BigDecimal sumOfRent = sumAll(BillType.RENT);

        // Final is: (sum of all rent) - (sum of all cost) - (sum of all
        // insvestment)
        return sumOfRent.subtract(sumOfCosts).subtract(sumOfInvestments);
    }

    private void addBills(final Collection<Bill> bills) {
        for (Bill bill : bills) {
            if (bill.isPaid()) {
                addPayed(bill.getType(), bill.getValueOfBill());
            } else {
                addNotPayed(bill.getType(), bill.getValueOfBill());
            }
        }
    }

    private void addBillTypes() {
        for (BillType billType : BillType.values()) {
            sumPayedMap.put(billType, NumberNormalizer.zero());
            sumNotPayedMap.put(billType, NumberNormalizer.zero());
        }
    }

    private void addNotPayed(final BillType billType, final BigDecimal value) {
        BigDecimal sum = getSumNotPayed(billType).add(value);
        sumNotPayedMap.put(billType, sum);
    }

    private void addPayed(final BillType billType, final BigDecimal value) {
        BigDecimal sum = getSumPayed(billType).add(value);
        sumPayedMap.put(billType, sum);
    }

    public BigDecimal getSumPayed(final BillType billType) {
        return sumPayedMap.get(billType);
    }

    public BigDecimal getSumNotPayed(final BillType billType) {
        return sumNotPayedMap.get(billType);
    }
}
