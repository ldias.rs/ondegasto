package org.ldiasrs.ondegasto.domain.user;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Representa um usuário.
 * @author cardoso
 *
 */
@Entity
@Table(name = "user_app")
public class User {

    public static final String ENTITY_NAME = "User"; //$NON-NLS-1$
    public static final String PROPERTY_LOGINNAME = "loginName"; //$NON-NLS-1$
    public static final String PROPERTY_PASSWORD = "password"; //$NON-NLS-1$

    @Id
    private String loginName;

    private String password;

    public User() {
        this("system", ""); //$NON-NLS-1$ //$NON-NLS-2$
    }

    public User(final String loginNameArg, final String passwordArg) {
        this.loginName = loginNameArg;
        this.password = passwordArg;
    }

    public User(final String loginNameArg) {
        this(loginNameArg, ""); //$NON-NLS-1$
    }

    public static final User createSystemUser() {
        return new User("system"); //$NON-NLS-1$
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(final String loginNameArg) {
        this.loginName = loginNameArg;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String passwordArg) {
        this.password = passwordArg;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((loginName == null) ? 0 : loginName.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        User other = (User) obj;
        if (loginName == null) {
            if (other.loginName != null) {
                return false;
            }
        } else if (!loginName.equals(other.loginName)) {
            return false;
        }
        return true;
    }
}
