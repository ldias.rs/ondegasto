package org.ldiasrs.ondegasto.view.vaadin.tagbinder;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.eventmanager.ModelCrudChangeListener;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.common.AbstractController;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserMessageHandle;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.common.View;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author ld24078
 */
public class TagBinderController extends AbstractController implements ModelCrudChangeListener<TagBinder> {

    private static final long serialVersionUID = -5742717982071716512L;

    private TagBinderLayout layout = null;
    private final UserMessageHandle userMessageHandle;

    public TagBinderController() {
        getView();
        userMessageHandle = new UserMessageHandle(layout);
        addActions();
        loadBeans();
        UserSessionResolver.getUserSession().getServiceEventManager().addListener(this);
        register();
    }

    @Override
    public View getView() {
        return getLayout();
    }

    public TagBinderLayout getLayout() {
        if (layout == null) {
            layout = new TagBinderLayout();
        }
        return layout;
    }

    private void loadBeans() {
        UserSession userSession = UserSessionResolver.getUserSession();
        Collection<TagBinder> beans = userSession.getTagAssociationService().findAllTagsAssociationsByUser(userSession.getUser());
        for (TagBinder tagBinder : beans) {
            addBeanOnTable(new TagBinderForm(tagBinder.getName(), tagBinder.getTagNamesAsString()));
        }
    }

    private void addActions() {
        // add
        getLayout().getBtadd().addListener(new ClickListener() {
            private static final long serialVersionUID = -2791029491680447290L;

            @Override
            public void buttonClick(final ClickEvent event) {
                addTagBinder();
            }

        });
        // add
        getLayout().getBtedit().addListener(new ClickListener() {
            private static final long serialVersionUID = -2791029491680447290L;

            @Override
            public void buttonClick(final ClickEvent event) {
                editTagBinder();
            }
        });
        // remove
        getLayout().getBtremove().addListener(new ClickListener() {
            private static final long serialVersionUID = -2791029491680447290L;

            @Override
            public void buttonClick(final ClickEvent event) {
                removeTagBinder();
            }
        });
    }

    private void addBeanOnTable(final TagBinderForm bean) {
        getLayout().getFormTableContainer().addBean(bean);
    }

    private void editTagBinder() {
        TagBinderForm selectedBean = getSelectedBeansOnTable();
        if (selectedBean == null) {
            userMessageHandle.showWarning(Messages.getString("TagBinderController.0")); //$NON-NLS-1$
        } else {
            getLayout().getTfname().setValue(selectedBean.getName());
            getLayout().getTftags().setValue(selectedBean.getTags());
        }
    }

    private void removeTagBinder() {
        TagBinderForm selectedBean = getSelectedBeansOnTable();
        if (selectedBean == null) {
            userMessageHandle.showWarning(Messages.getString("TagBinderController.1")); //$NON-NLS-1$
        } else {
            UserSession userSession = UserSessionResolver.getUserSession();
            userSession.getTagAssociationService().unAssociateBillName(userSession.getUser(), selectedBean.getName());
        }
    }

    private TagBinderForm getSelectedBeansOnTable() {
        return (TagBinderForm) getLayout().getTable().getValue();
    }

    private void addTagBinder() {
        UserSession userSession = UserSessionResolver.getUserSession();
        String name = getLayout().getTfname().getValue().toString();
        String tags = getLayout().getTftags().getValue().toString();
        if (StringUtils.isEmpty(name)) {
            userMessageHandle.showWarning(Messages.getString("TagBinderController.2")); //$NON-NLS-1$
            return;
        }
        if (StringUtils.isEmpty(tags)) {
            userMessageHandle.showWarning(Messages.getString("TagBinderController.3")); //$NON-NLS-1$
            return;
        }
        User user = userSession.getUser();
        String[] split = tags.split(","); //$NON-NLS-1$
        userSession.getTagAssociationService().associateBillNameOnTags(user, name, split);
    }

    @Override
    public void changeEvent(final ChangeEvent<TagBinder> event) {
        TagBinder tagBinder = event.getEntity();
        switch (event.getChangeType()) {
        case DELETE:
            removeBean(new TagBinderForm(tagBinder.getName(), tagBinder.getTagNamesAsString()));
            break;
        case INSERT:
        case UPDATE:
            addBeanOnTable(new TagBinderForm(tagBinder.getName(), tagBinder.getTagNamesAsString()));
            break;
        default:
            break;
        }
    }

    private void removeBean(final TagBinderForm tagBinderForm) {
        for (TagBinderForm binder : getLayout().getFormTableContainer().getItemIds()) {
            if (binder.getName().equals(tagBinderForm.getName())) {
                getLayout().getTable().removeItem(binder);
            }
        }
    }

    public UserMessageHandle getUserMessageHandle() {
        return userMessageHandle;
    }
}
