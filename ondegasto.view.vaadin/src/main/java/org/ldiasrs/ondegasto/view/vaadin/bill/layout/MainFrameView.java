package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.BillTableContainer;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;

import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.CellStyleGenerator;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;

/**
 * Painel com a tabela de contas.
 * @author cardoso
 */
public class MainFrameView extends VerticalLayout implements View {

    private static final long serialVersionUID = 8465824823976050296L;
    private Table table;
    private FilterPanelLayout searchPanel;
    private AllBalanceItensPanel allBalanceItensPanel;
    private BillTableContainer billTableModel;
    private BillActionsPanel actionPanel;
    private MonthActionsPanelLayout monthActionsPanel;
    private final Button hideBalance;

    public MainFrameView() {
        this.setSpacing(true);
        this.setStyleName("mainframe-panel"); //$NON-NLS-1$
        this.addComponent(getMonthActionsPanel());
        this.addComponent(getFilterPanelLayout());
        this.addComponent(getActionsPanel());
        this.addComponent(getTable());
        hideBalance = new Button(Messages.getString("MainFrameView.1")); //$NON-NLS-1$
        hideBalance.setStyleName(BaseTheme.BUTTON_LINK);
        this.addComponent(hideBalance);
        this.addComponent(getAllBalanceItensPanel());
    }

    public MonthActionsPanelLayout getMonthActionsPanel() {
        if (monthActionsPanel == null) {
            monthActionsPanel = new MonthActionsPanelLayout();
        }
        return monthActionsPanel;
    }

    public AllBalanceItensPanel getAllBalanceItensPanel() {
        if (allBalanceItensPanel == null) {
            allBalanceItensPanel = new AllBalanceItensPanel();
        }
        return allBalanceItensPanel;
    }

    public FilterPanelLayout getFilterPanelLayout() {
        if (searchPanel == null) {
            searchPanel = new FilterPanelLayout();
        }
        return searchPanel;
    }

    public BillActionsPanel getActionsPanel() {
        if (actionPanel == null) {
            actionPanel = new BillActionsPanel();
        }
        return actionPanel;
    }

    public Table getTable() {
        if (table == null) {
            table = new Table();
            // size
            table.setWidth("100%"); //$NON-NLS-1$
            table.setHeight("400px"); //$NON-NLS-1$
            // selectable
            table.setSelectable(true);
            table.setMultiSelect(true);
            // react at once when something is selected
            table.setImmediate(true);
            table.setContainerDataSource(getBillTableModel());
            // turn on column reordering and collapsing
            table.setColumnReorderingAllowed(true);
            table.setColumnCollapsingAllowed(true);
            table.setVisibleColumns(new String[] {Bill.PROPERTY_TYPE, Bill.PROPERTY_PAID, Bill.PROPERTY_NAME, Bill.PROPERTY_VALUEOFBILL,
                    Bill.PROPERTY_TAGLIST, Bill.PROPERTY_OBSERVATION, Bill.PROPERTY_DATEOFBILL });
            table.setColumnHeaders(new String[] {
                    Messages.getString("MainFrameView.8"), Messages.getString("MainFrameView.7"), Messages.getString("MainFrameView.6"), Messages.getString("MainFrameView.5"), Messages.getString("MainFrameView.4"), Messages.getString("MainFrameView.3"), Messages.getString("MainFrameView.2") }); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
            table.setCellStyleGenerator(new CellStyleGenerator() {

                private static final long serialVersionUID = 7157426878657202659L;

                @Override
                public String getStyle(final Object itemId, final Object propertyId) {
                    BillFormBean formBean = (BillFormBean) itemId;
                    String paidSufix = formBean.isPaid() ? "-p" : "-np"; //$NON-NLS-1$ //$NON-NLS-2$
                    String style = null;
                    if (BillFormBean.PROPERTY_TYPE.equals(propertyId)) {
                        switch (formBean.getType()) {
                        case COST:
                            style = "v-table-cost-cell"; //$NON-NLS-1$
                            break;
                        case RENT:
                            style = "v-table-rent-cell"; //$NON-NLS-1$
                            break;
                        case INVESTMENT:
                            style = "v-table-invest-cell"; //$NON-NLS-1$
                            break;
                        default:
                            style = "v-table-cost-cell"; //$NON-NLS-1$
                            break;
                        }
                    }
                    return style + paidSufix;
                }
            });
        }
        return table;
    }

    public BillTableContainer getBillTableModel() {
        if (billTableModel == null) {
            billTableModel = new BillTableContainer();
        }
        return billTableModel;
    }

    public Button getHideBalance() {
        return hideBalance;
    }

}
