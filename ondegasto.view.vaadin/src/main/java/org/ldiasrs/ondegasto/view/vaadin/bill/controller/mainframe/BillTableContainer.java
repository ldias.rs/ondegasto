package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe;

import java.io.Serializable;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;

import com.vaadin.data.util.BeanItemContainer;

/**
 * Container da tabela de contas.
 * @author cardoso
 */
public class BillTableContainer extends BeanItemContainer<BillFormBean> implements Serializable {

    private static final long serialVersionUID = -5010541051325832651L;

    public BillTableContainer() {
        super(BillFormBean.class);
    }
}
