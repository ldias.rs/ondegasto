package org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Bean utilizado em formulario de view.
 * @author cardoso
 */
public class BillFormBean extends Bill {

    private static final long serialVersionUID = -2849954785615615698L;
    public static final String PROPERTY_TAGS_AS_STRING = "tagsAsAtring"; //$NON-NLS-1$
    private static final String TAG_SEPARATOR = ","; //$NON-NLS-1$

    private String tagsAsAtring;

    public BillFormBean(final Bill clone) {
        super(clone);
        tagsAsAtring = concatTags(clone.getTagList());
    }

    private String concatTags(final Set<Tag> tagList) {
        StringBuffer tagsAsAsStr = new StringBuffer();
        for (Tag tag : tagList) {
            tagsAsAsStr.append(tag.getName());
            tagsAsAsStr.append(TAG_SEPARATOR);
        }
        int lstIdxOfTagSep = tagsAsAsStr.lastIndexOf(TAG_SEPARATOR);
        if (lstIdxOfTagSep > 0) {
            tagsAsAsStr.deleteCharAt(lstIdxOfTagSep);
        }
        return tagsAsAsStr.toString();
    }

    public final String getTagsAsAtring() {
        return tagsAsAtring;
    }

    public final void setTagsAsAtring(final String tagsAsAtringParm) {
        tagsAsAtring = tagsAsAtringParm;
    }

    public final void changeDate(final int month, final int year) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        setDateOfBill(calendar.getTime());
    }

    public final void clear() {
        setName(""); //$NON-NLS-1$
        setValueOfBill(new BigDecimal(0.0));
        setDateOfBill(GregorianCalendar.getInstance().getTime());
        setObservation(""); //$NON-NLS-1$
        setTagList(new HashSet<Tag>());
        setTagsAsAtring("geral"); //$NON-NLS-1$
    }

    public final boolean isValidId() {
        return getId() > 0;
    }

    public static BillFormBean createDefault(final User user) {
        return new BillFormBean(new Bill(user, "", 0, BillType.COST)); //$NON-NLS-1$
    }

    public static Bill convertToDomainObject(final BillFormBean bill) {
        return new Bill(bill);
    }

    public static Collection<Bill> convertToDomainObject(final Collection<BillFormBean> billFormList) {
        Collection<Bill> domainBills = new ArrayList<Bill>();
        for (BillFormBean billFormBean : billFormList) {
            domainBills.add(new Bill(billFormBean));
        }
        return domainBills;
    }

    public static Collection<BillFormBean> convertToViewObject(final Collection<Bill> domainBills) {
        Collection<BillFormBean> viewBeans = new ArrayList<BillFormBean>();
        for (Bill domainBill : domainBills) {
            viewBeans.add(new BillFormBean(domainBill));
        }
        return viewBeans;
    }

    public void changeProperties(final Bill bill) {
        setDateOfBill(bill.getDateOfBill());
        setName(bill.getName());
        setValueOfBill(bill.getValueOfBill());
        setPaid(bill.isPaid());
        setTagList(bill.getTagList());
        setObservation(bill.getObservation());
    }
}
