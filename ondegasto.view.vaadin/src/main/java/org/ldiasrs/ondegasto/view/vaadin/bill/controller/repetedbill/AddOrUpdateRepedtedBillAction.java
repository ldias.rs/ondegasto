package org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.AbstractAddUpdateBillFormBeanAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill.RepetedBillFormController.EditModeType;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.ui.Form;
import com.vaadin.ui.Window;

/**
 * Action to add ou update a {@link RepetedBillFormBean}.
 * @author cardoso
 */
public class AddOrUpdateRepedtedBillAction extends AbstractAddUpdateBillFormBeanAction {

    private static final long serialVersionUID = 1426221682978201704L;
    private final RepetedBillFormBean formBean;
    private RepeatedBill domainBean;
    private EditModeType editModeType;

    public AddOrUpdateRepedtedBillAction(final RepetedBillFormBean formBeanArg, final Window billSubWindow, final Form form,
            final RepeatedBill domainBeanArg, final EditModeType editModeTypeArg) {
        super(formBeanArg, billSubWindow, form);
        formBean = formBeanArg;
        domainBean = domainBeanArg;
        editModeType = editModeTypeArg;
    }

    public AddOrUpdateRepedtedBillAction(final RepetedBillFormBean formBeanArg, final Window billSubWindow, final Form form) {
        super(formBeanArg, billSubWindow, form);
        formBean = formBeanArg;
    }

    @Override
    protected void persist() {
        BillList billList = UserSessionResolver.getUserSession().getBillList();
        Bill domainBill = new Bill(getFormBean());
        if (formBean.isValidId()) {
            switch (editModeType) {
            case ALL:
                domainBean.changeValueAndNameFromAllBills(domainBill);
                break;
            case NEWERS:
                domainBean.changeValueAndNameFromNewersBills(domainBill);
                break;
            default:
                break;
            }
            billList.update(domainBean);
        } else {
            RepeatedBill repeatedBill = new RepeatedBill(domainBill, formBean.getNumberOfRepetion());
            billList.add(repeatedBill);
        }
    }

}
