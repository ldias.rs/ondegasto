package org.ldiasrs.ondegasto.view.vaadin.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.validator.DoubleValidator;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.BaseTheme;

/**
 * Utilitário para atribuição de propriedades em um campo.
 * @author cardoso
 */
public final class FieldPropertiesUtil {

    public static final String REGEX_NOT_ALPHANUMERIC = "[^\\p{Alnum}-_]{1,}"; //$NON-NLS-1$
    private static final String COMMON_FIELD_WIDTH = "12em"; //$NON-NLS-1$
    private static final int DEFAULT_MIN_FIELD = 3;
    private static final int DEFAULT_MAX_FIELD = 150;
    private static final int DEFAULT_MIN_PASSWORD_FIELD = 4;
    private static final int DEFAULT_MAX_PASSWORD_FIELD = 8;

    private FieldPropertiesUtil() {
    }

    /**
     * Retorna true se o valor é válido para a expressão regular.
     * @param regex
     * @param value
     */
    public static boolean isValidbyRegex(final String regex, final String value) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(value);
        return m.find();
    }

    public static void applyDoubleValueConverter(final Field f) {
        f.addListener(new ValueChangeListener() {
            private static final long serialVersionUID = -707717749214427507L;

            @Override
            public void valueChange(final ValueChangeEvent event) {
                String value = event.getProperty().getValue().toString().replace(",", "."); //$NON-NLS-1$//$NON-NLS-2$
                event.getProperty().setValue(value);
            }
        });
    }

    public static void setTextFieldDoubleProperties(final String fieldLabel, final Field f) {
        TextField tf = setTextFieldPropertiesWithoutValidator(fieldLabel, f, formatRequiredMsgError(fieldLabel));
        tf.addValidator(new DoubleValidator("O valor precisa ser numerico")); //$NON-NLS-1$
    }

    public static void setTextFieldIntegerProperties(final String fieldLabel, final Field f) {
        TextField tf = setTextFieldPropertiesWithoutValidator(fieldLabel, f, formatRequiredMsgError(fieldLabel));
        tf.addValidator(new IntegerValidator("O valor precisa ser numerico")); //$NON-NLS-1$
    }

    public static void setTextFieldStringProperties(final String fieldLabel, final Field f, final int minStringLengthValue,
            final int maxStringLengthValue) {
        setTextFieldStringProperties(fieldLabel, f, formatRequiredMsgError(fieldLabel),
                String.format("O campo '%s' deve possuir de %s a %s caracteres", fieldLabel, //$NON-NLS-1$
                        String.valueOf(minStringLengthValue), String.valueOf(maxStringLengthValue)), minStringLengthValue, maxStringLengthValue);
    }

    public static void setTextFieldStringProperties(final String fieldLabel, final Field f) {
        setTextFieldStringProperties(
                fieldLabel,
                f,
                formatRequiredMsgError(fieldLabel),
                String.format("O campo '%s' deve possuir de %s a %s caracteres", fieldLabel, DEFAULT_MIN_FIELD, DEFAULT_MAX_FIELD), DEFAULT_MIN_FIELD, DEFAULT_MAX_FIELD); //$NON-NLS-1$
    }

    private static String formatRequiredMsgError(final String fieldLabel) {
        return String.format("Por favor insira um valor para o campo %s", fieldLabel); //$NON-NLS-1$
    }

    public static void setTextFieldStringProperties(final String fieldLabel, final Field f, final String requiredErrorMsg,
            final String stringLengthValidadorErrorMsg, final int minStringLengthValue, final int maxStringLengthValue) {

        TextField tf = setTextFieldPropertiesWithoutValidator(fieldLabel, f, requiredErrorMsg);
        tf.addValidator(new StringLengthValidator(stringLengthValidadorErrorMsg, minStringLengthValue, maxStringLengthValue, false));
    }

    private static TextField setTextFieldPropertiesWithoutValidator(final String fieldLabel, final Field f, final String requiredErrorMsg) {
        return setTextFieldPropertiesWithoutValidator(fieldLabel, f, requiredErrorMsg, true);
    }

    public static TextField setTextFieldPropertiesWithoutValidator(final String fieldLabel, final Field f) {
        return setTextFieldPropertiesWithoutValidator(fieldLabel, f, null, false);
    }

    private static TextField setTextFieldPropertiesWithoutValidator(final String fieldLabel, final Field f, final String requiredErrorMsg,
            final boolean required) {
        TextField tf = (TextField) f;
        tf.setRequired(required);
        tf.setCaption(fieldLabel);
        if (required) {
            tf.setRequiredError(requiredErrorMsg);
        }
        tf.setWidth(COMMON_FIELD_WIDTH);
        return tf;
    }

    public static PasswordField createPasswordFieldProperties(final String fieldLabel) {
        PasswordField pf = new PasswordField();
        pf.setRequired(true);
        pf.setCaption(fieldLabel);
        pf.setRequiredError(formatRequiredMsgError(fieldLabel));
        pf.setWidth(COMMON_FIELD_WIDTH);
        pf.addValidator(new StringLengthValidator(String.format("O campo '%s' deve possuir de 3 a 7 caracteres", fieldLabel), //$NON-NLS-1$
                DEFAULT_MIN_PASSWORD_FIELD, DEFAULT_MAX_PASSWORD_FIELD, false));
        return pf;
    }

    public static void createFormApplyButton(final Form form, final Button.ClickListener confirmButtonAction) {
        createFormApplyButton(form, "Ok", confirmButtonAction); //$NON-NLS-1$
    }

    public static void createFormApplyButton(final Form form, final String confirmButtonLabel, final Button.ClickListener confirmButtonAction) {
        // The cancel / apply buttons
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        Button discardChanges = new Button("Limpar campos", //$NON-NLS-1$
                new Button.ClickListener() {
                    private static final long serialVersionUID = 2166554383870457888L;

                    @Override
                    public void buttonClick(final ClickEvent event) {
                        form.discard();
                    }
                });
        discardChanges.setStyleName(BaseTheme.BUTTON_LINK);
        buttons.addComponent(discardChanges);
        buttons.setComponentAlignment(discardChanges, Alignment.MIDDLE_LEFT);

        Button apply = new Button(confirmButtonLabel, confirmButtonAction);
        buttons.addComponent(apply);
        form.getFooter().addComponent(buttons);
        form.getFooter().setMargin(false, false, true, true);
    }

}
