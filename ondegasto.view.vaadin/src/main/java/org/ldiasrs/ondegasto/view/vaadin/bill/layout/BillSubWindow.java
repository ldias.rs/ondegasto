package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;

import com.vaadin.ui.Form;
import com.vaadin.ui.Window;

/**
 * Painel de propriedades da View.
 * @author cardoso
 */
public class BillSubWindow extends Window implements View {

    private static final long serialVersionUID = 4031258843140240665L;
    private final Form form;

    public BillSubWindow() {
        setWidth("360px"); //$NON-NLS-1$
        setCaption(Messages.getString("BillSubWindow.0")); //$NON-NLS-1$
        form = new Form();
        form.setCaption(Messages.getString("BillSubWindow.1")); //$NON-NLS-1$
        form.setWriteThrough(false);
        form.setInvalidCommitted(false);
        this.addComponent(form);
    }

    public Form getForm() {
        return form;
    }

}
