package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import java.math.BigDecimal;
import java.util.Collection;

import org.ldiasrs.ondegasto.domain.bill.Balance;
import org.ldiasrs.ondegasto.domain.bill.BalancePercentage;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.tableview.ListViewOfBillForm;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.AllBalanceItensPanel;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ListViewOfBillFormListener;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ModifiedListViewOfBillFormEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;

/**
 * Action that calculate the partial and final itens balance's.
 * @author cardoso
 */
public class BalancePanelController implements ListViewOfBillFormListener {

    private final ListViewOfBillForm listViewOfBillForm;
    private final AllBalanceItensPanel allBalanceItensPanel;

    public BalancePanelController(final AllBalanceItensPanel allBalanceItensPanelArg, final ListViewOfBillForm listViewOfBillFormArg) {
        allBalanceItensPanel = allBalanceItensPanelArg;
        listViewOfBillForm = listViewOfBillFormArg;
        ViewEventManager.getInstance().addListener(this);
    }

    private void processBalance() {
        Collection<BillFormBean> billFormlist = listViewOfBillForm.getBillFormList();
        Balance balance = new Balance(BillFormBean.convertToDomainObject(billFormlist));
        updatePanel(balance);
    }

    private void updatePanel(final Balance balance) {
        /**
         * TODO[ALTA][cardoso][ondegasto] IMPROVE THIS ON THE FUTURE - 1) I18
         * the $ 2) This don't smells good, think better.
         */
        BalancePercentage percentage = new BalancePercentage(balance);

        String unpaidCostStr = formatStr(balance.getSumNotPayed(BillType.COST), percentage.calculatePercentageOfUnPaidBillType(BillType.COST));
        String paidCostStr = formatStr(balance.getSumPayed(BillType.COST), percentage.calculatePercentageOfPaidBillType(BillType.COST));
        allBalanceItensPanel.getCostPanel().setPartialValue(unpaidCostStr);
        allBalanceItensPanel.getCostPanel().setFinalValue(paidCostStr);

        String unpaidInvestStr = formatStr(balance.getSumNotPayed(BillType.INVESTMENT),
                percentage.calculatePercentageOfUnPaidBillType(BillType.INVESTMENT));
        String paidInvestStr = formatStr(balance.getSumPayed(BillType.INVESTMENT), percentage.calculatePercentageOfPaidBillType(BillType.INVESTMENT));
        allBalanceItensPanel.getInvestPanel().setPartialValue(unpaidInvestStr);
        allBalanceItensPanel.getInvestPanel().setFinalValue(paidInvestStr);

        String unpaidRentStr = formatStr(balance.getSumNotPayed(BillType.RENT), percentage.calculatePercentageOfUnPaidBillType(BillType.RENT));
        String paidRentStr = formatStr(balance.getSumPayed(BillType.RENT), percentage.calculatePercentageOfPaidBillType(BillType.RENT));
        allBalanceItensPanel.getRentPanel().setPartialValue(unpaidRentStr);
        allBalanceItensPanel.getRentPanel().setFinalValue(paidRentStr);

        String partialBalanceStr = formatStr(balance.calculatePartialBalance(), percentage.calculateParcialBalancePercentage());
        String finalBalanceStr = formatStr(balance.calculateFinalBalance(), percentage.calculateFinalBalancePercentage());
        allBalanceItensPanel.getBalancePanel().setPartialValue(partialBalanceStr); //$NON-NLS-1$
        allBalanceItensPanel.getBalancePanel().setFinalValue(finalBalanceStr); //$NON-NLS-1$

        allBalanceItensPanel.requestRepaintAll();
        allBalanceItensPanel.requestRepaint();
        allBalanceItensPanel.requestRepaintRequests();
    }

    private String formatStr(final BigDecimal value, final float percentage) {
        return String.format("R$ %s (%s", String.valueOf(value), String.valueOf(percentage)) + "%)";
    }

    @Override
    public void listViewOfBillFormModified(final ModifiedListViewOfBillFormEvent event) {
        // TODO[BAIXA][cardoso][ondegasto] can i use only the bill changed and
        // recalculate only with this?
        processBalance();
    }
}
