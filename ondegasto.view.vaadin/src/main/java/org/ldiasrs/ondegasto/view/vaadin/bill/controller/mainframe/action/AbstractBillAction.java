package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;

/**
 * Abstraction to the Bill Actions.
 * @author cardoso
 */
public class AbstractBillAction {

    private MainFrameController mainFrameController;

    public AbstractBillAction(final MainFrameController mainFrameCtrlArg) {
        this.setMainFrameController(mainFrameCtrlArg);
    }

    public MainFrameController getMainFrameController() {
        return mainFrameController;
    }

    public void setMainFrameController(final MainFrameController mainFrameControllerArg) {
        this.mainFrameController = mainFrameControllerArg;
    }

}
