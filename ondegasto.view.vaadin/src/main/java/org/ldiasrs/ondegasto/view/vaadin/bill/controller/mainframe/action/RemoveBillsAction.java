package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.common.ConfirmMiltipleChoiseDialog;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;

/**
 * Action to remove any {@link Bill}.
 * @author cardoso
 */
public class RemoveBillsAction extends AbstractBillAction implements ClickListener {

    private static final long serialVersionUID = -7317455274002668182L;

    public RemoveBillsAction(final MainFrameController mainFrameController) {
        super(mainFrameController);
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        removeSelectedBill();
    }

    private void removeSelectedBill() {
        Table table = getMainFrameController().getBillListView().getTable();
        @SuppressWarnings("unchecked")
        Collection<BillFormBean> selectedBills = (Collection<BillFormBean>) table.getValue();
        if (isSingleSelect(selectedBills)) {
            confirmAndRemoveBill(table, selectedBills.iterator().next());
        } else {
            confirmRemoveOfMultipleBills(table, selectedBills);
        }
    }

    private void confirmRemoveOfMultipleBills(final Table table, final Collection<BillFormBean> selectedBills) {
        final Window window = table.getWindow();
        String msg = String.format("Deseja mesmo remover as '%s' contas simples selecioandas nesse mês?", selectedBills.size());
        final ConfirmMiltipleChoiseDialog confirmRemoveDialog = new ConfirmMiltipleChoiseDialog(msg, "remover"); //$NON-NLS-1$ //$NON-NLS-2$
        confirmRemoveDialog.getAllBillsBt().setVisible(false);
        confirmRemoveDialog.getSingleBillBt().addListener(new ClickListener() {
            private static final long serialVersionUID = 4169367019431559323L;

            @Override
            public void buttonClick(final ClickEvent event) {
                BillList billList = UserSessionResolver.getUserSession().getBillList();
                for (BillFormBean billFormBean : selectedBills) {
                    Bill domainBill = BillFormBean.convertToDomainObject(billFormBean);
                    /*
                     * TODO [ldias] ARRUMAR ESSA GAMBIARRA de puxar a conta toda
                     * para saber se nao eh uma conta repetedida nem parecelada
                     */
                    if (isNotParceledBill(domainBill) && isNotRepedtedBill(domainBill)) {
                        billList.remove(domainBill);
                    }
                }
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        window.addWindow(confirmRemoveDialog.getSubWindow());
    }

    private boolean isNotRepedtedBill(final Bill domainBill) {
        BillList billList = UserSessionResolver.getUserSession().getBillList();
        RepeatedBill repeatedBill = billList.findRepetedBillOfBill(domainBill);
        return repeatedBill == null;
    }

    private boolean isNotParceledBill(final Bill domainBill) {
        BillList billList = UserSessionResolver.getUserSession().getBillList();
        ParceledBill parceledBill = billList.findParceledBillOfBill(domainBill);
        return parceledBill == null;
    }

    private void confirmAndRemoveBill(final Table table, final BillFormBean billToRemove) {
        Window window = table.getWindow();
        Bill bill = BillFormBean.convertToDomainObject(billToRemove);
        BillList billList = UserSessionResolver.getUserSession().getBillList();
        ParceledBill parceledBill = billList.findParceledBillOfBill(bill);
        if (isParceledBill(parceledBill)) {
            confirmAndRemoveOfParceledBill(parceledBill, bill.getName(), window);
        } else {
            RepeatedBill repedtedBill = billList.findRepetedBillOfBill(bill);
            if (isRepetedBill(repedtedBill)) {
                confirmRemoveOfRepetedBill(repedtedBill, bill, window);
            } else {
                confirmAndRemoveOfSimpleBilll(bill, window);
            }
        }
    }

    private boolean isRepetedBill(final RepeatedBill repedtedBill) {
        return repedtedBill != null;
    }

    private boolean isParceledBill(final ParceledBill parceledBill) {
        return parceledBill != null;
    }

    private void confirmRemoveOfRepetedBill(final RepeatedBill repedtedBill, final Bill selectedBill, final Window window) {
        String msg = String.format(Messages.getString("RemoveBillsAction.1"), selectedBill.getName()); //$NON-NLS-1$
        final ConfirmMiltipleChoiseDialog confirmRemoveDialog = new ConfirmMiltipleChoiseDialog(msg,
                Messages.getString("RemoveBillsAction.2"), Messages.getString("RemoveBillsAction.3"), //$NON-NLS-1$ //$NON-NLS-2$
                Messages.getString("RemoveBillsAction.4")); //$NON-NLS-1$
        confirmRemoveDialog.getAllBillsBt().addListener(new ClickListener() {
            private static final long serialVersionUID = -3005777504280232056L;

            @Override
            public void buttonClick(final ClickEvent event) {
                BillList billList = UserSessionResolver.getUserSession().getBillList();
                billList.remove(repedtedBill);
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        confirmRemoveDialog.getNewerThanBt().addListener(new ClickListener() {
            private static final long serialVersionUID = -7577460026875638220L;

            @Override
            public void buttonClick(final ClickEvent event) {
                BillList billList = UserSessionResolver.getUserSession().getBillList();
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(selectedBill.getDateOfBill());
                repedtedBill.removeNewerThan(calendar);
                billList.update(repedtedBill);
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        confirmRemoveDialog.getSingleBillBt().addListener(new ClickListener() {
            private static final long serialVersionUID = -3005777504280232058L;

            @Override
            public void buttonClick(final ClickEvent event) {
                BillList billList = UserSessionResolver.getUserSession().getBillList();
                repedtedBill.removeBill(selectedBill);
                billList.update(repedtedBill);
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        confirmRemoveDialog.getCancelBt().addListener(new ClickListener() {
            private static final long serialVersionUID = -3005777505580232058L;

            @Override
            public void buttonClick(final ClickEvent event) {
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        window.addWindow(confirmRemoveDialog.getSubWindow());
    }

    private boolean isSingleSelect(final Collection<BillFormBean> selectedBills) {
        return selectedBills.size() == 1;
    }

    private void confirmAndRemoveOfParceledBill(final ParceledBill parceledBill, final String billName, final Window window) {
        String msg = String.format(Messages.getString("RemoveBillsAction.9"), billName);
        final ConfirmMiltipleChoiseDialog confirmRemoveDialog = new ConfirmMiltipleChoiseDialog(msg,
                Messages.getString("RemoveBillsAction.5"), Messages.getString("RemoveBillsAction.6")); //$NON-NLS-1$ //$NON-NLS-2$
        confirmRemoveDialog.getSingleBillBt().setVisible(false);
        confirmRemoveDialog.getAllBillsBt().addListener(new ClickListener() {
            private static final long serialVersionUID = -2753846102774032313L;

            @Override
            public void buttonClick(final ClickEvent event) {
                BillList billList = UserSessionResolver.getUserSession().getBillList();
                billList.remove(parceledBill);
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        window.addWindow(confirmRemoveDialog.getSubWindow());
    }

    private void confirmAndRemoveOfSimpleBilll(final Bill bill, final Window window) {
        String msg = String.format(Messages.getString("RemoveBillsAction.10"), bill.getName());
        final ConfirmMiltipleChoiseDialog confirmRemoveDialog = new ConfirmMiltipleChoiseDialog(msg,
                Messages.getString("RemoveBillsAction.7"), Messages.getString("RemoveBillsAction.8")); //$NON-NLS-1$ //$NON-NLS-2$
        confirmRemoveDialog.getAllBillsBt().setVisible(false);
        confirmRemoveDialog.getSingleBillBt().addListener(new ClickListener() {
            private static final long serialVersionUID = -5940736938055362644L;

            @Override
            public void buttonClick(final ClickEvent event) {
                BillList billList = UserSessionResolver.getUserSession().getBillList();
                billList.remove(bill);
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        });
        window.addWindow(confirmRemoveDialog.getSubWindow());
    }

}
