package org.ldiasrs.ondegasto.view.vaadin.tagbinder;

import java.io.Serializable;

/**
 * @author ld24078
 */
public class TagBinderForm implements Serializable {

    private static final long serialVersionUID = 1960299025482141931L;

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_TAGS = "tags";

    private final String name;
    private final String tags;

    public TagBinderForm() {
        this("default", "default");
    }

    public TagBinderForm(final String nameArg, final String tagsArg) {
        name = nameArg;
        tags = tagsArg;
    }

    public String getName() {
        return name;
    }

    public String getTags() {
        return tags;
    }

}
