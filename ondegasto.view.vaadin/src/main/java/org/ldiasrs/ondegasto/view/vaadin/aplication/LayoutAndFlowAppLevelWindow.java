package org.ldiasrs.ondegasto.view.vaadin.aplication;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.LoginListener;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.UserLoggedInEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;
import org.vaadin.navigator7.window.HeaderFooterFixedAppLevelWindow;

import com.github.wolfie.blackboard.annotation.ListenerMethod;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

/**
 * Define o layoyt da aplicação e controla a navegação entre as páginas.
 * @author cardoso
 */
public class LayoutAndFlowAppLevelWindow extends HeaderFooterFixedAppLevelWindow implements LoginListener {

    private static final long serialVersionUID = -6352010110505718447L;
    // Label showing global navigation events (useless, just for the demo).
    private Button logoutBt;
    private AppMenuBar menuBar;

    public LayoutAndFlowAppLevelWindow() {
        ViewEventManager.getInstance().addListener(this);
    }

    @Override
    protected Component createHeader() {
        VerticalLayout header = new VerticalLayout();
        // Application specific style.
        header.addStyleName("header"); //$NON-NLS-1$
        header.setWidth("100%"); //$NON-NLS-1$
        header.setHeight("100px"); //$NON-NLS-1$

        // /// NavigationListener label
        logoutBt = new Button(Messages.getString("LayoutAndFlowAppLevelWindow.0")); //$NON-NLS-1$
        logoutBt.setVisible(false);
        header.addComponent(logoutBt);
        header.setComponentAlignment(logoutBt, Alignment.TOP_RIGHT);
        logoutBt.setStyleName(BaseTheme.BUTTON_LINK);
        logoutBt.addListener(new ClickListener() {
            private static final long serialVersionUID = 6712837923327383047L;

            @Override
            public void buttonClick(final ClickEvent event) {
                event.getComponent().getApplication().close();
            }
        });
        menuBar = new AppMenuBar(getNavigator());
        header.addComponent(menuBar);
        header.setComponentAlignment(menuBar, Alignment.TOP_CENTER);
        return header;
    }

    private void enableUserLogginComponents() {
        this.enableLogoutButton();
        if (menuBar != null) {
            menuBar.removeItems();
            menuBar.addItens();
        }
    }

    @Override
    protected Component createFooter() {
        VerticalLayout vLayout = new VerticalLayout();
        vLayout.setWidth("100%"); //$NON-NLS-1$

        Label ll = new Label(Messages.getString("LayoutAndFlowAppLevelWindow.1")); //$NON-NLS-1$
        ll.setWidth(null);
        vLayout.addComponent(ll);
        vLayout.setComponentAlignment(ll, Alignment.TOP_CENTER);

        Label l = new Label(Messages.getString("LayoutAndFlowAppLevelWindow.2")); //$NON-NLS-1$
        l.setWidth(null);
        vLayout.addComponent(l);
        vLayout.setComponentAlignment(l, Alignment.BOTTOM_CENTER);
        vLayout.setHeight("200px"); //$NON-NLS-1$
        return vLayout;
    }

    @Override
    protected ComponentContainer createComponents() {
        ComponentContainer result = super.createComponents();
        // We apply the footer to the whole outer band, not only to the fixed
        // width inner band.
        this.getFooterBand().addStyleName("footer"); //$NON-NLS-1$
        return result;
    }

    @Override
    public boolean removeWindow(final Window window) {
        // The component ConfirmDialog, dont find this method, so i was forced
        // to Override.
        return super.removeWindow(window);
    }

    public void enableLogoutButton() {
        logoutBt.setVisible(true);
    }

    @Override
    @ListenerMethod
    public void userLogedIn(final UserLoggedInEvent event) {
        enableUserLogginComponents();
    }

}
