/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.ldiasrs.ondegasto.view.vaadin.aplication;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.i18n.LocaleContextHolder;
import org.vaadin.navigator7.NavigableApplication;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.HttpServletRequestListener;

/**
 * The BaseApplication class to extend.
 */
public abstract class BaseApplication extends NavigableApplication implements HttpServletRequestListener {
    private static final long serialVersionUID = 7473748584431194776L;

    private static ThreadLocal<Application> app = new ThreadLocal<Application>();

    // /**
    // * Sets the application.
    // * @param application the new application
    // */
    // private static void setApplication(final Application application) {
    // app.set(application);
    // }
    //
    // /**
    // * Reset application.
    // */
    // private static void resetApplication() {
    // app.remove();
    // }

    /**
     * Gets the application.
     * @return the application
     */
    public static Application getApplication() {
        return app.get();
    }

    @Override
    public Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }

    @Override
    public void setLocale(final Locale locale) {
        LocaleContextHolder.setLocale(locale);
    }

    @Override
    public void onRequestStart(final HttpServletRequest request, final HttpServletResponse response) {

    }

    @Override
    public void onRequestEnd(final HttpServletRequest request, final HttpServletResponse response) {
    }
}
