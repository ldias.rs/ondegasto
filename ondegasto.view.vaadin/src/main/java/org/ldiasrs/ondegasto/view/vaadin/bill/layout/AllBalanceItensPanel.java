package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.ui.HorizontalLayout;

/**
 * Join all the {@link BalanceItemPanelLayout} in one single panel.
 * @author cardoso
 */
public class AllBalanceItensPanel extends HorizontalLayout {

    private static final long serialVersionUID = 734695991454754840L;
    private BalanceItemPanelLayout investPanel;
    private BalanceItemPanelLayout costPanel;
    private BalanceItemPanelLayout rentPanel;
    private BalanceItemPanelLayout balancePanel;

    public AllBalanceItensPanel() {
        addComponent(getInvestPanel());
        addComponent(getCostPanel());
        addComponent(getRentPanel());
        addComponent(getBalancePanel());
        setStyleName("balanceItemPanel-style"); //$NON-NLS-1$
    }

    public BalanceItemPanelLayout getInvestPanel() {
        if (investPanel == null) {
            investPanel = new BalanceItemPanelLayout();
            investPanel.setTitle(Messages.getString("AllBalanceItensPanel.0")); //$NON-NLS-1$
        }
        return investPanel;
    }

    public BalanceItemPanelLayout getCostPanel() {
        if (costPanel == null) {
            costPanel = new BalanceItemPanelLayout();
            costPanel.setTitle(Messages.getString("AllBalanceItensPanel.1")); //$NON-NLS-1$
        }
        return costPanel;
    }

    public BalanceItemPanelLayout getRentPanel() {
        if (rentPanel == null) {
            rentPanel = new BalanceItemPanelLayout();
            rentPanel.setTitle(Messages.getString("AllBalanceItensPanel.2")); //$NON-NLS-1$
        }
        return rentPanel;
    }

    public BalanceItemPanelLayout getBalancePanel() {
        if (balancePanel == null) {
            balancePanel = new BalanceItemPanelLayout();
            balancePanel.setTitle(Messages.getString("AllBalanceItensPanel.3")); //$NON-NLS-1$
        }
        return balancePanel;
    }
}
