package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.exception;

/**
 * Exception when not found the BillForm.
 * @author cardoso
 */
public class CouldNotFoundBillFormBean extends Exception {

    private static final long serialVersionUID = -6886482562066586834L;

}
