package org.ldiasrs.ondegasto.view.vaadin.bill.controller;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Form;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

/**
 * Default behavior for add or Update any children of bill entity.
 * @author cardoso
 */
public abstract class AbstractAddUpdateBillFormBeanAction implements ClickListener {

    private static final long serialVersionUID = -6337718689179368710L;
    private final BillFormBean formBean;
    private final Window subWindow;
    private final Form form;

    public AbstractAddUpdateBillFormBeanAction(final BillFormBean formBeanArg, final Window billSubWindowArg, final Form formArg) {
        formBean = formBeanArg;
        subWindow = billSubWindowArg;
        form = formArg;
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        addOrUpdateBill(event.getComponent().getWindow());
    }

    private void addOrUpdateBill(final Window window) {
        try {
            commitForm();
            showSuccessMsg(window);
            TagStringResolver.resolveTags(formBean);
            persist();
            displayOff();
        } catch (InvalidValueException e) {
            window.showNotification(Messages.getString("AbstractAddUpdateBillFormBeanAction.0"), Notification.TYPE_WARNING_MESSAGE); //$NON-NLS-1$
        }
    }

    private void showSuccessMsg(final Window window) {
        window.showNotification(Messages.getString("AbstractAddUpdateBillFormBeanAction.1"), Notification.TYPE_TRAY_NOTIFICATION); //$NON-NLS-1$
    }

    private void displayOff() {
        subWindow.getParent().removeWindow(subWindow);
    }

    private void commitForm() {
        form.commit();
    }

    public BillFormBean getFormBean() {
        return formBean;
    }

    protected abstract void persist();

}
