package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe;

import java.util.Collection;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormControler;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.filteractions.FilterBillOnTableAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.BalancePanelController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.HideBalanceAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.PayBillAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.RemoveBillsAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.ShowEditBillFormAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.ShowSubWindowAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.UnPayBilllAction;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.monthnavegation.MonthNavegationController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform.ParceledBillFormController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill.RepetedBillFormController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.tableview.TableViewController;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.BillActionsPanel;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.MainFrameView;
import org.ldiasrs.ondegasto.view.vaadin.common.AbstractController;
import org.ldiasrs.ondegasto.view.vaadin.common.UserMessageHandle;
import org.ldiasrs.ondegasto.view.vaadin.common.View;
import org.vaadin.navigator7.Page;

/**
 * Controlador do gerenciamento de contas.
 * @author cardoso
 */
@Page(uriName = "contas")
public class MainFrameController extends AbstractController {

    private static final long serialVersionUID = 5458008661022273326L;

    private MainFrameView mainFrameView;

    private UserMessageHandle userMessageHandle = null;

    public MainFrameController() {
        TableViewController tableViewController = new TableViewController(getBillListTableView().getBillTableModel(), userMessageHandle, this);
        new BalancePanelController(mainFrameView.getAllBalanceItensPanel(), tableViewController);
        new MonthNavegationController(mainFrameView.getMonthActionsPanel());
        addActionsOnBillListView();
        getView();
        register();
    }

    private MainFrameView getBillListTableView() {
        if (mainFrameView == null) {
            mainFrameView = new MainFrameView();
        }
        return mainFrameView;
    }

    private void addActionsOnBillListView() {

        // TODO [cardoso][MEDIA][ondegasto] Isso deve ser refatorado para uma
        // classe especializada
        BillActionsPanel billActionsPanel = mainFrameView.getActionsPanel();

        billActionsPanel.getNewBillActionBt().addListener(new ShowSubWindowAction(this, new BillFormControler().getBillPropertiesSubWindow()));

        billActionsPanel.getNewParceledBillActionBt().addListener(new ShowSubWindowAction(this, new ParceledBillFormController().getSubWindow()));

        billActionsPanel.getNewRepedtedBillActionBt().addListener(new ShowSubWindowAction(this, new RepetedBillFormController().getSubWindow()));

        RemoveBillsAction removeBillAction = new RemoveBillsAction(this);
        billActionsPanel.getRemoveBillActionBt().addListener(removeBillAction);

        ShowEditBillFormAction showEditBillFormAction = new ShowEditBillFormAction(this);
        billActionsPanel.getEditBillActionBt().addListener(showEditBillFormAction);

        PayBillAction payBillAction = new PayBillAction(this);
        billActionsPanel.getPayBillActionBt().addListener(payBillAction);

        UnPayBilllAction unPayBillAction = new UnPayBilllAction(this);
        billActionsPanel.getUnPayBillActionBt().addListener(unPayBillAction);

        FilterBillOnTableAction filterBillOnTableAction = new FilterBillOnTableAction(this);
        mainFrameView.getFilterPanelLayout().getBtActions().addListener(filterBillOnTableAction.newValueChangeListener());
        mainFrameView.getFilterPanelLayout().getBtProcess().addListener(filterBillOnTableAction.newClickListener());
        // mainFrameView.getFilterPanelLayout().getTfValue().addListener(filterBillOnTableAction.newValueChangeListener());
        mainFrameView.getHideBalance().addListener(new HideBalanceAction(this));
    }

    public MainFrameView getBillListView() {
        return mainFrameView;
    }

    @Override
    public View getView() {
        return getBillListTableView();
    }

    public UserMessageHandle getUserMessageHandle() {
        if (userMessageHandle == null) {
            userMessageHandle = new UserMessageHandle(getView());
        }
        return userMessageHandle;
    }

    @SuppressWarnings("unchecked")
    public Collection<BillFormBean> getSelectedBills() {
        return (Collection<BillFormBean>) getBillListView().getTable().getValue();
    }

}
