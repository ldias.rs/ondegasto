package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import com.github.wolfie.blackboard.Blackboard;

/**
 * Event manager for view events.
 * @author cardoso
 */
public class ViewEventManager extends Blackboard {

    private static ViewEventManager instance;

    static {
        instance = new ViewEventManager();
    }

    public ViewEventManager() {
        this.register(ListViewOfBillFormListener.class, ModifiedListViewOfBillFormEvent.class);
        this.register(DateFilterChangeListener.class, DateFilterChangeEvent.class);
        this.register(FilterBillOnTableListener.class, FilterBillOnTableEvent.class);
        this.register(LoginListener.class, UserLoggedInEvent.class);

    }

    public static ViewEventManager getInstance() {
        // // TODO[ALTA][cardoso][ondegasto] this like not good, i think the
        // best way is the spring manage the pages and put this with autoWired
        return instance;
    }

}
