package org.ldiasrs.ondegasto.view.vaadin.tagbinder;

import java.io.Serializable;

import com.vaadin.data.util.BeanItemContainer;

/**
 * @author ld24078
 */
public class TagBinderFormTableContainer extends BeanItemContainer<TagBinderForm> implements Serializable {

    private static final long serialVersionUID = 167087293294599990L;

    public TagBinderFormTableContainer() {
        super(TagBinderForm.class);
    }

}
