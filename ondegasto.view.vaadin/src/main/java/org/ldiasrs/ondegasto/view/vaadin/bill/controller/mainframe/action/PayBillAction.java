package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import java.util.Collection;

import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * Action to pay the selected's {@link BillFormBean}.
 * @author cardoso
 */
public class PayBillAction extends AbstractBillAction implements ClickListener {

    private static final long serialVersionUID = 3724013627329186597L;

    public PayBillAction(final MainFrameController mainFrameController) {
        super(mainFrameController);
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        setPaidBill(true);
    }

    protected void setPaidBill(final boolean paid) {
        Collection<BillFormBean> selectedBills = getMainFrameController().getSelectedBills();
        for (final BillFormBean bill : selectedBills) {
            if (paid) {
                bill.pay();
            } else {
                bill.unPay();
            }
        }
        UserSession userSession = UserSessionResolver.getUserSession();
        userSession.getBillList().update(BillFormBean.convertToDomainObject(selectedBills));
    }

}
