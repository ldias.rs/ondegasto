package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;

/**
 * Define a Listener of date filter change.
 * @author ldias
 */
public interface DateFilterChangeListener extends AppListener {

    void dateFilterChange(DateFilterChangeEvent event);

}
