package org.ldiasrs.ondegasto.view.vaadin.aplication;

import org.mortbay.jetty.Connector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.bio.SocketConnector;
import org.mortbay.jetty.webapp.WebAppContext;

public final class StartJettyOndeGasto {

    private StartJettyOndeGasto() {
        // TODO Auto-generated constructor stub
    }

    public static void main(final String[] args) throws Exception {
        Server server = new Server();
        SocketConnector connector = new SocketConnector();
        connector.setPort(9999);
        server.setConnectors(new Connector[] {connector });
        WebAppContext bb = new WebAppContext();
        bb.setServer(server);
        bb.setContextPath("/"); //$NON-NLS-1$
        bb.setWar("ondegasto.view.vaadin-0.0.1-SNAPSHOT.war"); //$NON-NLS-1$

        // START JMX SERVER
        // MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
        // MBeanContainer mBeanContainer = new MBeanContainer(mBeanServer);
        // server.getContainer().addEventListener(mBeanContainer);
        // mBeanContainer.start();

        server.addHandler(bb);
        try {
            System.out.println(">>> STARTING EMBEDDED JETTY SERVER, PRESS ANY KEY TO STOP"); //$NON-NLS-1$
            server.start();
            System.in.read();
            System.out.println(">>> STOPPING EMBEDDED JETTY SERVER"); //$NON-NLS-1$
            // while (System.in.available() == 0) {
            // Thread.sleep(5000);
            // }
            server.stop();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(100);
        }
    }
}
