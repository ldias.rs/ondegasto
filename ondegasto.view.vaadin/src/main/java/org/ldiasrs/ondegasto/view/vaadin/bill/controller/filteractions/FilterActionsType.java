package org.ldiasrs.ondegasto.view.vaadin.bill.controller.filteractions;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

/**
 * Define the types of actions.
 * @author ld24078
 */
public enum FilterActionsType {

    FILTER_SHOW_ALL(
             Messages.getString("FilterActionsType.0")), //$NON-NLS-1$
    FILTER_BY_TAG(
                 Messages.getString("FilterActionsType.1")), //$NON-NLS-1$
    FILTER_BY_NAME(
                   Messages.getString("FilterActionsType.2")), //$NON-NLS-1$
    ADD_TAGS(
                  Messages.getString("FilterActionsType.3")),
    FILTER_BY_HIDE_TAGS(
                        Messages.getString("FilterActionsType.4"));

    private String filter;

    FilterActionsType(final String filterArg) {
        filter = filterArg;
    }

    public String getFilter() {
        return filter;
    }

    public static FilterActionsType valueOfByFilter(final String key) {
        for (FilterActionsType bean : values()) {
            if (bean.getFilter().equals(key)) {
                return bean;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return filter;
    }

}
