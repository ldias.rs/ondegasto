package org.ldiasrs.ondegasto.view.vaadin.aplication;

import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.view.vaadin.bankextractor.BankExtractImporterController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.NoteController;
import org.ldiasrs.ondegasto.view.vaadin.tagbinder.TagBinderController;
import org.vaadin.navigator7.Navigator;

import com.vaadin.ui.MenuBar;

/**
 * Menu of aplication.
 * @author cardoso
 */
public class AppMenuBar extends MenuBar {

    private static final long serialVersionUID = 4514160505588820587L;
    private final Navigator navigator;

    public AppMenuBar(final Navigator navigatorArg) {
        navigator = navigatorArg;
        setWidth("100%"); //$NON-NLS-1$
    }

    public void addItens() {
        addItem(Messages.getString("AppMenuBar.1"), new AppMenuBar.Command() { //$NON-NLS-1$
                    private static final long serialVersionUID = 3979163237377167364L;

                    @Override
                    public void menuSelected(final MenuItem selectedItem) {
                        navigator.navigateTo(MainFrameController.class);
                    }
                });
        addItem(Messages.getString("AppMenuBar.2"), new AppMenuBar.Command() { //$NON-NLS-1$
                    private static final long serialVersionUID = 7780837258906869959L;

                    @Override
                    public void menuSelected(final MenuItem selectedItem) {
                        getWindow().showNotification(Messages.getString("AppMenuBar.3")); //$NON-NLS-1$
                    }
                });
        addItem(Messages.getString("AppMenuBar.4"), new AppMenuBar.Command() { //$NON-NLS-1$
                    private static final long serialVersionUID = 844605464966182293L;

                    @Override
                    public void menuSelected(final MenuItem selectedItem) {
                        navigator.navigateTo(NoteController.class);
                    }
                });

        addItem(Messages.getString("AppMenuBar.5"), new AppMenuBar.Command() { //$NON-NLS-1$
                    private static final long serialVersionUID = -5545993017251726997L;

                    @Override
                    public void menuSelected(final MenuItem selectedItem) {
                        navigator.navigateTo(TagBinderController.class);
                    }
                });

        addItem(Messages.getString("AppMenuBar.6"), new AppMenuBar.Command() { //$NON-NLS-1$
                    private static final long serialVersionUID = -5545993017251726997L;

                    @Override
                    public void menuSelected(final MenuItem selectedItem) {
                        navigator.navigateTo(BankExtractImporterController.class);
                    }
                });
        addItem(Messages.getString("AppMenuBar.7"), new AppMenuBar.Command() { //$NON-NLS-1$
                    private static final long serialVersionUID = -5545993017251726997L;

                    @Override
                    public void menuSelected(final MenuItem selectedItem) {
                        try {
                            String dir = UserSessionResolver.getUserSession().getBackupDataService().backupData();
                            getWindow().showNotification(Messages.getString("AppMenuBar.8") + dir); //$NON-NLS-1$
                        } catch (OperationServiceException e) {
                            getWindow().showNotification(Messages.getString("AppMenuBar.9") + e.getMessage()); //$NON-NLS-1$
                            e.printStackTrace();
                        }
                    }
                });
    }
}
