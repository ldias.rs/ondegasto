package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

/**
 * Layout for
 * {@link org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform.ParceledBillFormBean}
 * .
 * @author cardoso
 */
public class RepetedBillSubWindow extends BillSubWindow {

    private static final long serialVersionUID = 3798702939807588795L;

    public RepetedBillSubWindow() {
        super();
        getForm().setCaption(Messages.getString("RepetedBillSubWindow.0")); //$NON-NLS-1$
        setCaption(Messages.getString("RepetedBillSubWindow.1")); //$NON-NLS-1$
    }

}
