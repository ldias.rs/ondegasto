package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppEvent;

/**
 * Define a event to inform that the list view of BillForm was modified.
 * @author cardoso
 */
public class ModifiedListViewOfBillFormEvent implements AppEvent {

}
