package org.ldiasrs.ondegasto.view.vaadin.bill.controller.monthnavegation;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * Create's the actions for navegation of Months.
 * @author ldias
 */
public final class MonthActionCreator {

    private MonthActionCreator() {
    }

    public static ClickListener backMonthAction(final MonthNavegationController monthNavegationController) {
        return new ClickListener() {
            private static final long serialVersionUID = 4980940310446584496L;

            @Override
            public void buttonClick(final ClickEvent event) {
                monthNavegationController.backMonth();
            }
        };
    }

    public static ClickListener forwardMonth(final MonthNavegationController monthNavegationController) {
        return new ClickListener() {
            private static final long serialVersionUID = -3259360302042160243L;

            @Override
            public void buttonClick(final ClickEvent event) {
                monthNavegationController.forwardMonth();
            }
        };
    }

    public static ClickListener currentMonth(final MonthNavegationController monthNavegationController) {
        return new ClickListener() {
            private static final long serialVersionUID = 5546906631418877168L;

            @Override
            public void buttonClick(final ClickEvent event) {
                monthNavegationController.currentMonth();
            }
        };
    }

}
