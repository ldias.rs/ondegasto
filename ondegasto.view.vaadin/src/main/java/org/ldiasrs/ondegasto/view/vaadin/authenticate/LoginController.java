package org.ldiasrs.ondegasto.view.vaadin.authenticate;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.view.vaadin.aplication.SessionAplication;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.common.AbstractController;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;
import org.ldiasrs.ondegasto.view.vaadin.common.ViewController;
import org.vaadin.navigator7.Page;

import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.Notification;

/**
 * Ação de logar um usuário.
 * @author cardoso
 */
@Page(uriName = "login")
public class LoginController extends AbstractController implements ViewController {

    private static final long serialVersionUID = 6234314540737839987L;
    private LoginView loginView;
    private User user;

    public LoginController() {
        getView();
        register();
    }

    private ClickListener loginUserAction() {
        return new ClickListener() {
            private static final long serialVersionUID = -4994480126504855643L;

            @Override
            public void buttonClick(final ClickEvent event) {
                try {
                    loginView.getLoginForm().commit();
                    boolean isAuthenticated = SessionAplication.getCurrent().getUserService().authenticate(user);
                    if (isAuthenticated) {
                        SessionAplication.getCurrent().registerUserSession(user);
                        SessionAplication.getCurrentNavigableAppLevelWindow().changePage(new MainFrameController());
                    } else {
                        event.getComponent().getWindow().showNotification(Messages.getString("LoginController.0"), //$NON-NLS-1$
                                Notification.TYPE_ERROR_MESSAGE);
                    }
                } catch (OperationServiceException e) {
                    event.getComponent().getWindow().showNotification(Messages.getString("LoginController.1"), //$NON-NLS-1$
                            Notification.TYPE_ERROR_MESSAGE);
                } catch (InvalidValueException e) {
                    event.getComponent().getWindow().showNotification(Messages.getString("LoginController.2"), //$NON-NLS-1$
                            Notification.TYPE_WARNING_MESSAGE);
                }
            }
        };
    }

    private LoginView getLoginView() {
        if (loginView == null) {
            loginView = new LoginView();
            user = new User("", ""); //$NON-NLS-1$ //$NON-NLS-2$
            BeanItem<User> userDataSource = new BeanItem<User>(user);
            loginView.getLoginForm().setItemDataSource(userDataSource);
            FieldPropertiesUtil.createFormApplyButton(loginView.getLoginForm(), Messages.getString("LoginController.3"), loginUserAction()); //$NON-NLS-1$
            //loginView.getLoginForm().setVisibleItemProperties(Arrays.asList(new String[] {"name", "password"})); //$NON-NLS-1$ //$NON-NLS-2$
        }
        return loginView;
    }

    @Override
    public View getView() {
        return getLoginView();
    }
}
