package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;

/**
 * Layout of Actions for a
 * {@link org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean}
 * .
 * @author cardoso
 */
public class BillActionsPanel extends Panel {

    private static final long serialVersionUID = 9007398158371422293L;
    private Button newBillActionBt;
    private Button newParceledBillActionBt;
    private Button newRepedtedBillActionBt;
    private Button removeBillActionBt;
    private Button editBillActionBt;
    private Button paybillActionBt;
    private Button unPaybillActionBt;

    public BillActionsPanel() {
        HorizontalLayout painelNewBill = new HorizontalLayout();
        painelNewBill.setStyleName("toolbar-new-bill"); //$NON-NLS-1$
        painelNewBill.setDescription(Messages.getString("BillActionsPanel.1")); //$NON-NLS-1$
        Button newBillActionBtArg = new Button(Messages.getString("BillActionsPanel.2")); //$NON-NLS-1$
        newBillActionBtArg.setIcon(new ThemeResource("icons/normal.png")); //$NON-NLS-1$
        setNewBillActionBt(newBillActionBtArg); //$NON-NLS-1$
        painelNewBill.addComponent(getNewBillActionBt());

        Button newParceledBillActionBtArg = new Button(Messages.getString("BillActionsPanel.4")); //$NON-NLS-1$
        newParceledBillActionBtArg.setIcon(new ThemeResource("icons/repetida.png")); //$NON-NLS-1$
        setNewParceledBillActionBt(newParceledBillActionBtArg); //$NON-NLS-1$
        painelNewBill.addComponent(getNewParceledBillActionBt());
        Button newRepedtedBillActionBtArg = new Button(Messages.getString("BillActionsPanel.6")); //$NON-NLS-1$
        newRepedtedBillActionBtArg.setIcon(new ThemeResource("icons/parcelada.png")); //$NON-NLS-1$
        setNewRepedtedBillActionBt(newRepedtedBillActionBtArg);
        painelNewBill.addComponent(getNewRepedtedBillActionBt());

        HorizontalLayout layout = new HorizontalLayout();
        layout.setSpacing(true);
        layout.setStyleName(Messages.getString("BillActionsPanel.8")); //$NON-NLS-1$
        setContent(layout);
        layout.addComponent(painelNewBill);
        Button removeBillActionBtArg = new Button(Messages.getString("BillActionsPanel.9")); //$NON-NLS-1$
        removeBillActionBtArg.setIcon(new ThemeResource("icons/remover.png")); //$NON-NLS-1$
        setRemoveBillActionBt(removeBillActionBtArg); //$NON-NLS-1$
        addComponent(getRemoveBillActionBt());
        Button editBillActionBtArg = new Button(Messages.getString("BillActionsPanel.11")); //$NON-NLS-1$
        editBillActionBtArg.setIcon(new ThemeResource("icons/editar.png")); //$NON-NLS-1$
        setEditBillActionBt(editBillActionBtArg); //$NON-NLS-1$
        addComponent(getEditBillActionBt());
        Button paybillActionBtArg = new Button(Messages.getString("BillActionsPanel.13")); //$NON-NLS-1$
        paybillActionBtArg.setIcon(new ThemeResource("icons/pagar.png")); //$NON-NLS-1$
        setPaybillActionBt(paybillActionBtArg); //$NON-NLS-1$
        addComponent(getPayBillActionBt());
        Button unPaybillActionBtArg = new Button(Messages.getString("BillActionsPanel.15")); //$NON-NLS-1$
        unPaybillActionBtArg.setIcon(new ThemeResource("icons/naopagar.png")); //$NON-NLS-1$
        setUnPaybillActionBt(unPaybillActionBtArg); //$NON-NLS-1$
        addComponent(getUnPayBillActionBt());
    }

    public Button getNewBillActionBt() {
        return newBillActionBt;
    }

    public void setNewBillActionBt(final Button newBillActionBtArg) {
        newBillActionBt = newBillActionBtArg;
    }

    public Button getRemoveBillActionBt() {
        return removeBillActionBt;
    }

    public void setRemoveBillActionBt(final Button removeBillActionBtArg) {
        removeBillActionBt = removeBillActionBtArg;
    }

    public Button getEditBillActionBt() {
        return editBillActionBt;
    }

    public void setEditBillActionBt(final Button editBillActionBtArg) {
        editBillActionBt = editBillActionBtArg;
    }

    public Button getPayBillActionBt() {
        return paybillActionBt;
    }

    public void setPaybillActionBt(final Button paybillActionBtArg) {
        paybillActionBt = paybillActionBtArg;
    }

    public Button getUnPayBillActionBt() {
        return unPaybillActionBt;
    }

    public void setUnPaybillActionBt(final Button unPaybillActionBtArg) {
        unPaybillActionBt = unPaybillActionBtArg;
    }

    public void setNewParceledBillActionBt(final Button newParceledBillActionBtArg) {
        newParceledBillActionBt = newParceledBillActionBtArg;
    }

    public Button getNewParceledBillActionBt() {
        return newParceledBillActionBt;
    }

    public void setNewRepedtedBillActionBt(final Button newRepedtedBillActionBtArg) {
        newRepedtedBillActionBt = newRepedtedBillActionBtArg;
    }

    public Button getNewRepedtedBillActionBt() {
        return newRepedtedBillActionBt;
    }
}
