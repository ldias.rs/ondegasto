package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe;

import java.util.Collection;
import java.util.HashSet;

/**
 * Exceção que controla os erros comuns.
 * @author cardoso
 */
public abstract class AbstractOperationServiceException extends Exception {

    private static final long serialVersionUID = -1472954892151148661L;
    private Collection<String> errorMgs;

    public AbstractOperationServiceException(final String msg) {
        this(msg, null);
    }

    public AbstractOperationServiceException(final String msg, final Throwable e) {
        super(msg, e);
        errorMgs = new HashSet<String>();
        errorMgs.add(msg);
    }

}
