package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * Actions to hide the balance values.
 * @author ld24078
 */
public class HideBalanceAction implements ClickListener {

    private static final long serialVersionUID = -5246295636324574427L;
    private final MainFrameController mainFrameController;
    private boolean isHided = true;

    public HideBalanceAction(final MainFrameController mainFrameControllerArg) {
        mainFrameController = mainFrameControllerArg;
        hideBalancePanel();
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        hideBalancePanel();
    }

    private void hideBalancePanel() {
        Button hideBalance = mainFrameController.getBillListView().getHideBalance();
        if (isHided) {
            hideBalance.setCaption(Messages.getString("HideBalanceAction.0")); //$NON-NLS-1$
            isHided = false;
        } else {
            hideBalance.setCaption(Messages.getString("HideBalanceAction.1")); //$NON-NLS-1$
            isHided = true;
        }
        mainFrameController.getBillListView().getAllBalanceItensPanel().setVisible(isHided);
    }

}
