package org.ldiasrs.ondegasto.view.vaadin.bill.controller.tableview;

import java.util.Collection;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;

/**
 * Define a list view for {@link BillFormBean}.
 * @author ldias
 *
 */
public interface ListViewOfBillForm {

    Collection<BillFormBean> getBillFormList();

}
