package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;

/**
 * Action that show the panel of the
 * {@link org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean}
 * .
 * @author cardoso
 */
public class ShowSubWindowAction extends AbstractBillAction implements ClickListener {

    private static final long serialVersionUID = 4918708367472085476L;

    private final Window subWindow;

    public ShowSubWindowAction(final MainFrameController mainFrameController, final Window subWindowArg) {
        super(mainFrameController);
        this.subWindow = subWindowArg;
        subWindow.setPositionX(500);
        subWindow.setPositionY(100);
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        showBill(event.getComponent().getWindow());
    }

    private void showBill(final Window window) {
        window.addWindow(subWindow);
    }

}
