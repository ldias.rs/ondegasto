package org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.AbstractAddUpdateBillFormBeanAction;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.ui.Form;
import com.vaadin.ui.Window;

/**
 * Action to add ou update a {@link ParceledBillFormBean}.
 * @author cardoso
 */
public class AddOrUpdateParceledBillAction extends AbstractAddUpdateBillFormBeanAction {

    private static final long serialVersionUID = 1426221682978201704L;
    private final ParceledBillFormBean parceledFormBean;

    public AddOrUpdateParceledBillAction(final ParceledBillFormBean formBean, final Window billSubWindow, final Form form) {
        super(formBean, billSubWindow, form);
        this.parceledFormBean = formBean;
    }

    @Override
    protected void persist() {
        BillList billList = UserSessionResolver.getUserSession().getBillList();
        Bill domainBill = new Bill(getFormBean());
        ParceledBill domainParceledBill = new ParceledBill(domainBill, parceledFormBean.getInValue(), parceledFormBean.getNumberOfParceled());
        billList.add(domainParceledBill);
    }

}
