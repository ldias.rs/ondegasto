package org.ldiasrs.ondegasto.view.vaadin.authenticate;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

/**
 * Gerencia a criação de campos para o Usuario.
 * @author cardoso
 */
public class LoginFormFieldFacory extends DefaultFieldFactory {

    private static final long serialVersionUID = 8038297076835038013L;

    @Override
    public Field createField(final Item item, final Object propertyId, final Component uiContext) {
        Field f = super.createField(item, propertyId, uiContext);

        if (User.PROPERTY_LOGINNAME.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldStringProperties(Messages.getString("LoginFormFieldFacory.0"), f); //$NON-NLS-1$
        }
        if (User.PROPERTY_PASSWORD.equals(propertyId)) {
            f = FieldPropertiesUtil.createPasswordFieldProperties(Messages.getString("LoginFormFieldFacory.1")); //$NON-NLS-1$
        }
        return f;
    }
}
