package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;


/**
 * Layout for {@link org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform.ParceledBillFormBean}.
 * @author cardoso
 */
public class ParceledBillSubWindow extends BillSubWindow {

    private static final long serialVersionUID = -8728452952742485044L;

    public ParceledBillSubWindow() {
        super();
        getForm().setCaption(Messages.getString("ParceledBillSubWindow.1")); //$NON-NLS-1$
        setCaption(Messages.getString("ParceledBillSubWindow.0")); //$NON-NLS-1$
    }
}
