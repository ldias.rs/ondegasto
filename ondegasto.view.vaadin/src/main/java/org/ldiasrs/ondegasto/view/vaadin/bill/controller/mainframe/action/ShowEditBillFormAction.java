package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormControler;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill.RepetedBillFormController;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.BillSubWindow;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.RepetedBillSubWindow;
import org.ldiasrs.ondegasto.view.vaadin.common.ConfirmMiltipleChoiseDialog;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window;

/**
 * Actions to edit a {@link BillFormBean}.
 * @author cardoso
 */
public class ShowEditBillFormAction extends AbstractBillAction implements ClickListener {

    private static final long serialVersionUID = 6578501958225886157L;

    public ShowEditBillFormAction(final MainFrameController mainFrameController) {
        super(mainFrameController);
    }

    protected void showEditFormToSelectedBill(final Window window) {
        @SuppressWarnings("unchecked")
        Collection<BillFormBean> selectedBills = (Collection<BillFormBean>) getMainFrameController().getBillListView().getTable().getValue();

        if (isSingleSelect(selectedBills)) {
            BillFormBean selectedBill = selectedBills.iterator().next();
            Bill convertToDomainObject = BillFormBean.convertToDomainObject(selectedBill);
            RepeatedBill repedtedBill = UserSessionResolver.getUserSession().getBillList().findRepetedBillOfBill(convertToDomainObject);
            if (repedtedBill != null) {
                showEditModeTypes(repedtedBill, selectedBill, window);
            } else {
                showSingleEditMode(window, selectedBill);
            }
        } else {
            getMainFrameController().getUserMessageHandle().showWarning(Messages.getString("ShowEditBillFormAction.4")); //$NON-NLS-1$
        }
    }

    private void showSingleEditMode(final Window window, final BillFormBean selectedBill) {
        BillFormControler billFormControler = new BillFormControler(selectedBill);
        BillSubWindow billPropertiesSubWindow = billFormControler.getBillPropertiesSubWindow();
        billPropertiesSubWindow.setPositionX(500);
        billPropertiesSubWindow.setPositionY(100);
        window.addWindow(billPropertiesSubWindow);
    }

    private void showEditModeTypes(final RepeatedBill domainBean, final BillFormBean selectedBill, final Window window) {
        String msg = String.format(Messages.getString("ShowEditBillFormAction.0"), selectedBill.getName()); //$NON-NLS-1$
        final ConfirmMiltipleChoiseDialog confirmRemoveDialog = new ConfirmMiltipleChoiseDialog(msg,
                Messages.getString("ShowEditBillFormAction.1"), Messages.getString("ShowEditBillFormAction.2"), //$NON-NLS-1$ //$NON-NLS-2$
                Messages.getString("ShowEditBillFormAction.3")); //$NON-NLS-1$
        ClickListener editAllRepetedBillsAction = editAllRepetedBillsAction(domainBean, selectedBill, window, confirmRemoveDialog);
        confirmRemoveDialog.getAllBillsBt().addListener(editAllRepetedBillsAction);

        ClickListener editSingleRepedtedBillAction = editSingleRepedtedBillAction(selectedBill, window, confirmRemoveDialog);
        confirmRemoveDialog.getSingleBillBt().addListener(editSingleRepedtedBillAction);

        ClickListener editNewresRepetedBillAction = editNewresRepetedBillAction(domainBean, selectedBill, window, confirmRemoveDialog);
        confirmRemoveDialog.getNewerThanBt().addListener(editNewresRepetedBillAction);

        ClickListener calcelAction = calcelAction(window, confirmRemoveDialog);
        confirmRemoveDialog.getCancelBt().addListener(calcelAction);

        window.addWindow(confirmRemoveDialog.getSubWindow());

    }

    private ClickListener calcelAction(final Window window, final ConfirmMiltipleChoiseDialog confirmRemoveDialog) {
        return new ClickListener() {
            private static final long serialVersionUID = -3005777504280232058L;

            @Override
            public void buttonClick(final ClickEvent event) {
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        };
    }

    private ClickListener editNewresRepetedBillAction(final RepeatedBill domainBean, final BillFormBean selectedBill, final Window window,
            final ConfirmMiltipleChoiseDialog confirmRemoveDialog) {
        return new ClickListener() {
            private static final long serialVersionUID = 3926513978271781068L;

            @Override
            public void buttonClick(final ClickEvent event) {
                RepetedBillFormController controller = new RepetedBillFormController(domainBean, selectedBill,
                        RepetedBillFormController.EditModeType.NEWERS);
                showMultipleEditDialog(window, confirmRemoveDialog, controller);
            }
        };
    }

    private ClickListener editSingleRepedtedBillAction(final BillFormBean selectedBill, final Window window,
            final ConfirmMiltipleChoiseDialog confirmRemoveDialog) {
        return new ClickListener() {
            private static final long serialVersionUID = -3005777504280232058L;

            @Override
            public void buttonClick(final ClickEvent event) {
                showSingleEditMode(window, selectedBill);
                window.removeWindow(confirmRemoveDialog.getSubWindow());
            }
        };
    }

    private ClickListener editAllRepetedBillsAction(final RepeatedBill domainBean, final BillFormBean selectedBill, final Window window,
            final ConfirmMiltipleChoiseDialog confirmRemoveDialog) {
        return new ClickListener() {
            private static final long serialVersionUID = -3005777504280232056L;

            @Override
            public void buttonClick(final ClickEvent event) {
                RepetedBillFormController controller = new RepetedBillFormController(domainBean, selectedBill,
                        RepetedBillFormController.EditModeType.ALL);
                showMultipleEditDialog(window, confirmRemoveDialog, controller);
            }
        };
    }

    private void showMultipleEditDialog(final Window window, final ConfirmMiltipleChoiseDialog confirmRemoveDialog,
            final RepetedBillFormController controller) {
        RepetedBillSubWindow subWindow = controller.getSubWindow();
        subWindow.setPositionX(500);
        subWindow.setPositionY(100);
        window.addWindow(subWindow);
        window.removeWindow(confirmRemoveDialog.getSubWindow());
    }

    private boolean isSingleSelect(final Collection<BillFormBean> selectedBills) {
        return selectedBills.size() == 1;
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        showEditFormToSelectedBill(event.getComponent().getWindow());
    }
}
