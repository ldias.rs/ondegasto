package org.ldiasrs.ondegasto.view.vaadin.aplication;

import org.ldiasrs.ondegasto.view.vaadin.authenticate.LoginController;
import org.ldiasrs.ondegasto.view.vaadin.bankextractor.BankExtractImporterController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.NoteController;
import org.ldiasrs.ondegasto.view.vaadin.tagbinder.TagBinderController;
import org.vaadin.navigator7.WebApplication;

/**
 * Classe que é iniciada uma unica vez na subida da aplicação no
 * container(Jetty, Tomcat).
 */
public class SystemWebApplication extends WebApplication {

    public SystemWebApplication() {

        registerPages(new Class[] {LoginController.class, MainFrameController.class, NoteController.class, TagBinderController.class,
                BankExtractImporterController.class });
    }

    // @Override
    // protected void registerInterceptors() {
    // registerInterceptor(new SecurityInterceptor());
    // super.registerInterceptors();
    // }
}
