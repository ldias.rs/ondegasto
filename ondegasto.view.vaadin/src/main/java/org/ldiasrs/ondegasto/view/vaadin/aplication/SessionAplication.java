/*
 * Copyright 2009 IT Mill Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.ldiasrs.ondegasto.view.vaadin.aplication;

import java.util.NoSuchElementException;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.UserLoggedInEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;
import org.vaadin.navigator7.NavigableApplication;
import org.vaadin.navigator7.window.NavigableAppLevelWindow;

/**
 * Aplicação que sera iniciada sempre que um usuário entrar no sistema.
 */
public class SessionAplication extends BaseApplication {

    private static final long serialVersionUID = 1000715330819092457L;

    private LayoutAndFlowAppLevelWindow layoutAppLevelWindow;
    private UserSession loggedUserSession;
    private UserService userService;

    private SpringContextHelper contextHelper;

    public SessionAplication() {
        setTheme("myTheme"); //$NON-NLS-1$
    }

    @Override
    public NavigableAppLevelWindow createNewNavigableAppLevelWindow() {
        layoutAppLevelWindow = new LayoutAndFlowAppLevelWindow();
        return layoutAppLevelWindow;
    }

    public void registerUserSession(final User user) {
        loggedUserSession = getContectHelper().getUserSession(user);
        ViewEventManager.getInstance().fire(new UserLoggedInEvent(user));
    }

    public boolean isUserLogged() {
        return loggedUserSession != null;
    }

    public UserSession getLogedUserSession() {
        if (isUserLogged()) {
            return loggedUserSession;
        }
        throw new NoSuchElementException("Não exisite usuario logado nessa sessão.");
    }

    public static SessionAplication getCurrent() {
        return (SessionAplication) NavigableApplication.getCurrentNavigableAppLevelWindow().getApplication();
    }

    public UserService getUserService() {
        if (userService == null) {
            userService = getContectHelper().getUserService();
        }
        return userService;
    }

    private SpringContextHelper getContectHelper() {
        if (contextHelper == null) {
            contextHelper = new SpringContextHelper(this);
        }
        return contextHelper;
    }

}
