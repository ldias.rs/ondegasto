package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppEvent;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.filteractions.FilterActionsType;

/**
 * Represent a event to filter the bills on the table.
 * @author ldias
 */
public class FilterBillOnTableEvent implements AppEvent {

    private final String filterStr;
    private final FilterActionsType filerType;

    public FilterBillOnTableEvent(final String filterStrArg, final FilterActionsType filerTypeArg) {
        filterStr = filterStrArg;
        filerType = filerTypeArg;
    }

    public String getFilterStr() {
        return filterStr;
    }

    public FilterActionsType getFilerType() {
        return filerType;
    }

}
