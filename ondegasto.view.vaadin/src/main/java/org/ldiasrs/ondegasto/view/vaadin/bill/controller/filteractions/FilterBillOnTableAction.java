package org.ldiasrs.ondegasto.view.vaadin.bill.controller.filteractions;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action.AbstractBillAction;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.FilterBillOnTableEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

/**
 * Filter the Bill's on the table.
 * @author ldias
 */
public class FilterBillOnTableAction extends AbstractBillAction {

    public FilterBillOnTableAction(final MainFrameController mainFrameCtrlArg) {
        super(mainFrameCtrlArg);
    }

    private void filter(final String filerTypeStr) {
        String filterValue = getMainFrameController().getBillListView().getFilterPanelLayout().getTfValue().getValue().toString();
        ViewEventManager.getInstance().fire(new FilterBillOnTableEvent(filterValue, FilterActionsType.valueOfByFilter(filerTypeStr)));
    }

    public ValueChangeListener newValueChangeListener() {
        return new ValueChangeListener() {

            private static final long serialVersionUID = 1L;

            @Override
            public void valueChange(final ValueChangeEvent event) {
                String action = event.getProperty().getValue().toString();
                filter(action);

            }
        };
    }

    public Button.ClickListener newClickListener() {
        return new Button.ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(final ClickEvent event) {
                String action = getMainFrameController().getBillListView().getFilterPanelLayout().getBtActions().getValue().toString();
                filter(action);
            }

        };
    }

}
