package org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.AbstractAddUpdateBillFormBeanAction;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.ui.Form;
import com.vaadin.ui.Window;

/**
 * Action to add new {@link Bill}.
 * @author cardoso
 */
public class AddOrUpdateBillAction extends AbstractAddUpdateBillFormBeanAction {

    private static final long serialVersionUID = 441563062465895098L;

    public AddOrUpdateBillAction(final BillFormBean formBean, final Window billSubWindow, final Form form) {
        super(formBean, billSubWindow, form);
    }

    @Override
    protected final void persist() {
        BillList billList = UserSessionResolver.getUserSession().getBillList();
        if (getFormBean().isValidId()) {
            billList.update(new Bill(getFormBean()));
        } else {
            billList.add(new Bill(getFormBean()));
        }
    }

}
