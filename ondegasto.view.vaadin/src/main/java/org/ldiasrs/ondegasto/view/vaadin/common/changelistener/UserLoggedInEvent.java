package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppEvent;

/**
 * @author ld24078
 */
public class UserLoggedInEvent implements AppEvent {

    private final User user;

    public UserLoggedInEvent(final User userArg) {
        super();
        user = userArg;
    }

    public User getUser() {
        return user;
    }

}
