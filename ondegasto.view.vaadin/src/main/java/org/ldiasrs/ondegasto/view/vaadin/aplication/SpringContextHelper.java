package org.ldiasrs.ondegasto.view.vaadin.aplication;

import javax.servlet.ServletContext;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.common.eventmanager.ServiceEventManager;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.service.usersession.UserSessionImp;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.vaadin.Application;
import com.vaadin.terminal.gwt.server.WebApplicationContext;

/**
 * Aplication helper do spring.
 * @author cardoso
 */
public class SpringContextHelper {

    private final ApplicationContext context;

    public SpringContextHelper(final Application application) {
        ServletContext servletContext = ((WebApplicationContext) application.getContext()).getHttpSession().getServletContext();
        context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    public Object getBean(final String beanRef) {
        return context.getBean(beanRef);
    }

    public UserSession getUserSession(final User user) {
        UserSessionImp imp = (UserSessionImp) context.getBean("userSessionImp"); //$NON-NLS-1$
        imp.setUser(user);
        return imp;
    }

    public ServiceEventManager getServiceEventManager() {
        return (ServiceEventManager) context.getBean("serviceEventManager"); //$NON-NLS-1$
    }

    public UserService getUserService() {
        return (UserService) context.getBean("userServiceImp"); //$NON-NLS-1$
    }

}
