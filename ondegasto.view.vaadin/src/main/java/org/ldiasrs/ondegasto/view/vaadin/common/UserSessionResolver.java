package org.ldiasrs.ondegasto.view.vaadin.common;

import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.aplication.SessionAplication;

/**
 * Resolve the current {@link UserSession}.
 * @author ldias
 */
public final class UserSessionResolver {

    private UserSessionResolver() {
    }

    public static UserSession getUserSession() {
        return SessionAplication.getCurrent().getLogedUserSession();
    }

    public static boolean isUserLogged() {
        return SessionAplication.getCurrent().isUserLogged();
    }

}
