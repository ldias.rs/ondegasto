package org.ldiasrs.ondegasto.view.vaadin.bill.controller.monthnavegation;

import java.util.Calendar;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.DateUtil;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.MonthActionsPanelLayout;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.DateFilterChangeEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;

/**
 * Controls the navegation of bills by month.
 * @author ldias
 */
public class MonthNavegationController {

    private Calendar currentdMonth;
    private MonthActionsPanelLayout actionsPanel;

    public MonthNavegationController(final MonthActionsPanelLayout actionsPanelArg) {
        this.actionsPanel = actionsPanelArg;
        addActions();
        currentMonth();
    }

    public void currentMonth() {
        currentdMonth = Calendar.getInstance();
        notifyMonthChange();
    }

    public void forwardMonth() {
        currentdMonth.add(Calendar.MONTH, 1);
        notifyMonthChange();
    }

    private void notifyMonthChange() {
        actionsPanel.setCurrentMonthLabel(DateUtil.formatLabelDate(currentdMonth.getTime()));
        ViewEventManager.getInstance().fire(new DateFilterChangeEvent(currentdMonth.getTime()));
    }

    public void backMonth() {
        currentdMonth.add(Calendar.MONTH, -1);
        notifyMonthChange();
    }

    private void addActions() {
        actionsPanel.getBtForward().addListener(MonthActionCreator.forwardMonth(this));
        actionsPanel.getBtBack().addListener(MonthActionCreator.backMonthAction(this));
        actionsPanel.getBtCurrent().addListener(MonthActionCreator.currentMonth(this));
    }
}
