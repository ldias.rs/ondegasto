package org.ldiasrs.ondegasto.view.vaadin.bill.controller;

import java.util.StringTokenizer;

import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

/**
 * Convert the string tags in domain Tags.
 * @author cardoso
 */
public final class TagStringResolver {

    private TagStringResolver() {
    }

    public static void resolveTags(final BillFormBean formBean) {
        if (isTagsNotEmpty(formBean)) {
            formBean.removeAllTags();
            StringTokenizer sk = new StringTokenizer(formBean.getTagsAsAtring(), ","); //$NON-NLS-1$
            while (sk.hasMoreTokens()) {
                String tagAsString = sk.nextToken().trim().toLowerCase();
                UserSession userSession = UserSessionResolver.getUserSession();
                Tag tag = userSession.getBillTagList().getTag(userSession.getUser(), tagAsString);
                formBean.addTag(tag);
            }
        }
    }

    private static boolean isTagsNotEmpty(final BillFormBean formBean) {
        return !formBean.getTagsAsAtring().trim().isEmpty();
    }
}
