package org.ldiasrs.ondegasto.view.vaadin.authenticate;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;
import org.vaadin.navigator7.Page;

import com.vaadin.ui.Form;
import com.vaadin.ui.VerticalLayout;

/**
 * Representa o painel de Login.
 * @author cardoso
 */
@Page
public class LoginView extends VerticalLayout implements View {

    private static final long serialVersionUID = 2368972733881255425L;
    private Form form;

    public LoginView() {
        addComponent(getLoginForm());
    }

    public Form getLoginForm() {
        if (form == null) {
            form = new Form();
            form.setCaption(Messages.getString("LoginView.0")); //$NON-NLS-1$
            form.setWriteThrough(false);
            form.setInvalidCommitted(false);
            form.setFormFieldFactory(new LoginFormFieldFacory());
        }
        return form;
    }
}
