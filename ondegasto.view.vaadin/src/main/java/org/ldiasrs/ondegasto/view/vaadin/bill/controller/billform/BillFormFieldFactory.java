package org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.data.Item;
import com.vaadin.ui.AbstractSelect.Filtering;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;

/**
 * Gerencia a criação de fileds para a classe
 * {@link org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean}
 * .
 * @author cardoso
 */
public class BillFormFieldFactory extends DefaultFieldFactory {

    private static final int MAXSTRINGLENGTHVALUE = 400;
    private static final long serialVersionUID = 5486513955448300169L;

    @Override
    public Field createField(final Item item, final Object propertyId, final Component uiContext) {
        Field f = super.createField(item, propertyId, uiContext);

        if (Bill.PROPERTY_NAME.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldStringProperties(Messages.getString("BillFormFieldFactory.0"), f); //$NON-NLS-1$
        } else if (Bill.PROPERTY_VALUEOFBILL.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldDoubleProperties(Messages.getString("BillFormFieldFactory.1"), f); //$NON-NLS-1$
        } else if (Bill.PROPERTY_OBSERVATION.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldPropertiesWithoutValidator(Messages.getString("BillFormFieldFactory.2"), f); //$NON-NLS-1$
        } else if (BillFormBean.PROPERTY_TAGS_AS_STRING.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldStringProperties(Messages.getString("BillFormFieldFactory.3"), f, 1, MAXSTRINGLENGTHVALUE); //$NON-NLS-1$
        } else if (Bill.PROPERTY_TYPE.equals(propertyId)) {
            f = createTypeField();
        } else if (Bill.PROPERTY_DATEOFBILL.equals(propertyId)) {
            f.setCaption(Messages.getString("BillFormFieldFactory.4")); //$NON-NLS-1$
        } else if (Bill.PROPERTY_PAID.equals(propertyId)) {
            f.setCaption(Messages.getString("BillFormFieldFactory.5")); //$NON-NLS-1$
        }
        return f;
    }

    // private ComboBox createTagListFiled() {
    //        groupField = new ComboBox("Marcadores"); //$NON-NLS-1$
    //        groupField.setWidth("200px"); //$NON-NLS-1$
    // groupField.setFilteringMode(Filtering.FILTERINGMODE_STARTSWITH);
    // groupField.setImmediate(true);
    // //groupField.setNullSelectionAllowed(false);
    // groupField.setNewItemsAllowed(true);
    // //groupField.addValidator(getTagNameValidator());
    // groupField.setNewItemHandler(getGroupNewItemHandler());
    // HashSet<Tag> l = new HashSet<Tag>();
    // l.add(getTag("Geral"));
    // groupField.setContainerDataSource(new IndexedContainer(l));
    // return groupField;
    // }
    //
    // private NewItemHandler getGroupNewItemHandler() {
    // return new NewItemHandler() {
    // @Override
    // public void addNewItem(String newItemCaption) {
    // @SuppressWarnings("unchecked")
    // Collection<Tag> groups = (Collection<Tag>) groupField
    // .getContainerDataSource().getItemIds();
    // if (!findTag(groups, newItemCaption)) {
    // Tag g = getTag(newItemCaption);
    // groupField.addItem(g);
    // groupField.setValue(g);
    // }
    // }
    // };
    // }
    //
    // private Tag getTag(String value) {
    // return userSession.getBillTagList().getTag(userSession.getUser(), value);
    // }
    //
    // private boolean findTag(Collection<Tag> groups, String groupName) {
    // for (Tag groupIt : groups) {
    // if (groupIt.getName().equals(groupName.toUpperCase())) {
    // return true;
    // }
    // }
    // return false;
    // }
    //
    // /**
    // * Realiza a validação de nome de grupo. Utilizando negação.
    // */
    // public static AbstractValidator getTagNameValidator() {
    //        return new AbstractValidator("Grupo inválido") { //$NON-NLS-1$
    //
    // @Override
    // public boolean isValid(Object value) {
    // if (value != null) {
    //                    String name = ""; //$NON-NLS-1$
    // name = ((Tag) value).getName();
    // return !name.isEmpty()
    // && (name).length() <= 60
    // && !isValidbyRegex(REGEX_NOT_ALPHANUMERIC,
    // name);
    // }
    // return false;
    // }
    // };
    // }

    private ComboBox createTypeField() {
        ComboBox c = new ComboBox(Messages.getString("BillFormFieldFactory.6")); //$NON-NLS-1$
        c.setRequired(true);
        c.setFilteringMode(Filtering.FILTERINGMODE_STARTSWITH);
        c.setImmediate(true);
        c.setNullSelectionAllowed(false);
        for (BillType t : BillType.values()) {
            c.addItem(t);
        }
        c.select(BillType.COST);
        return c;
    }
}
