package org.ldiasrs.ondegasto.view.vaadin.note.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.service.common.eventmanager.ModelCrudChangeListener;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;
import org.ldiasrs.ondegasto.service.note.api.NoteList;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.common.AbstractController;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.common.View;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.action.EditNoteAction;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.action.NewNoteAction;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.action.RemoveNoteAction;
import org.ldiasrs.ondegasto.view.vaadin.note.layout.NoteLayout;
import org.ldiasrs.ondegasto.view.vaadin.note.layout.NoteListLayout;

import com.github.wolfie.blackboard.annotation.ListenerMethod;

/**
 * Controle para as view de notas.
 * @author ld24078
 */
public class NoteController extends AbstractController implements ModelCrudChangeListener<Note> {

    private static final long serialVersionUID = -3667570390590635213L;

    private NoteListLayout noteListLayout;

    private Map<String, Note> cachedNotesOnView;

    public NoteController() {
        getView();
        register();
    }

    @Override
    public View getView() {
        if (noteListLayout == null) {
            createView();
        }
        return noteListLayout;
    }

    private void createView() {
        noteListLayout = new NoteListLayout();
        applyActions();
        loadNotesToView();
    }

    private void loadNotesToView() {
        UserSession userSession = UserSessionResolver.getUserSession();
        NoteList noteList = userSession.getNoteList();
        userSession.getServiceEventManager().addListener(this);
        Collection<Note> notes = noteList.getAll(userSession.getUser());
        for (Note note : notes) {
            addNoteOnView(note);
        }
    }

    private void applyActions() {
        noteListLayout.getRemoveBt().addListener(new RemoveNoteAction(noteListLayout, this));
        noteListLayout.getNewBt().addListener(new NewNoteAction());
    }

    @Override
    @ListenerMethod
    public void changeEvent(final ChangeEvent<Note> event) {
        if (event.getEntity() instanceof Note) {
            String name = event.getEntity().getName();
            Note entity = event.getEntity();
            switch (event.getChangeType()) {
            case INSERT:
                addNoteOnView(entity);
                break;
            case DELETE:
                noteListLayout.removeTab(event.getEntity().getName());
                cachedNotesOnView.remove(name);
                break;
            default:
                break;
            }
        }
    }

    private void addNoteOnView(final Note entity) {
        NoteLayout noteLayout = new NoteLayout(entity.getHtml());
        noteLayout.getEditBt().addListener(new EditNoteAction(noteLayout, this));
        noteLayout.setCaption(entity.getName());
        noteListLayout.setMargin(true);
        noteListLayout.addTab(noteLayout);
        getCachedNotesOnView().put(entity.getName(), entity);
    }

    public Note getCachedNoteByName(final String name) {
        return getCachedNotesOnView().get(name);
    }

    public void removeNoteByName(final String name) {
        UserSessionResolver.getUserSession().getNoteList().remove(getCachedNotesOnView().get(name));
    }

    public Map<String, Note> getCachedNotesOnView() {
        if (cachedNotesOnView == null) {
            cachedNotesOnView = new HashMap<String, Note>();
        }
        return cachedNotesOnView;
    }
}
