package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;

import com.github.wolfie.blackboard.annotation.ListenerMethod;

/**
 * Define a listener for user view.
 * @author cardoso
 */
public interface ListViewOfBillFormListener extends AppListener {

    @ListenerMethod
    void listViewOfBillFormModified(ModifiedListViewOfBillFormEvent event);

}
