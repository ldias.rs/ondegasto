package org.ldiasrs.ondegasto.view.vaadin.note.controller.action;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.NoteController;
import org.ldiasrs.ondegasto.view.vaadin.note.layout.NoteListLayout;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
/**
 * Action process the remove of a {@link Note}.
 * @author ld24078
 *
 */
public class RemoveNoteAction implements ClickListener {

    private static final long serialVersionUID = 2827217805138860169L;

    private NoteListLayout layout;
    private NoteController controller;

    public RemoveNoteAction(final NoteListLayout layoutArg, final NoteController controllerArg) {
        this.controller = controllerArg;
        this.layout = layoutArg;
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        String tabName = layout.getSelectedTabName();
        Note note = controller.getCachedNoteByName(tabName);
        UserSessionResolver.getUserSession().getNoteList().remove(note);
    }
}
