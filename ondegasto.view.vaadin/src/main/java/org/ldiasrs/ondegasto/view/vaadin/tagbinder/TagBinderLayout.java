package org.ldiasrs.ondegasto.view.vaadin.tagbinder;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * @author ld24078
 */
public class TagBinderLayout extends VerticalLayout implements View {

    private static final long serialVersionUID = 544900723015493813L;

    private final TextField tfname;
    private final TextField tftags;
    private final TagBinderFormTableContainer formTableContainer;
    private Table table;
    private final Button btadd;
    private final Button btremove;
    private final Button btedit;

    public TagBinderLayout() {
        tfname = new TextField();
        tftags = new TextField();
        btadd = new Button(Messages.getString("TagBinderLayout.0")); //$NON-NLS-1$
        btremove = new Button(Messages.getString("TagBinderLayout.1")); //$NON-NLS-1$
        btedit = new Button(Messages.getString("TagBinderLayout.2")); //$NON-NLS-1$
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.addComponent(new Label(Messages.getString("TagBinderLayout.3"))); //$NON-NLS-1$
        horizontalLayout.addComponent(tfname);
        horizontalLayout.addComponent(new Label(Messages.getString("TagBinderLayout.4"))); //$NON-NLS-1$
        horizontalLayout.addComponent(tftags);
        horizontalLayout.addComponent(new Label(Messages.getString("TagBinderLayout.5"))); //$NON-NLS-1$
        horizontalLayout.addComponent(btadd);
        horizontalLayout.addComponent(btremove);
        horizontalLayout.addComponent(btedit);
        addComponent(horizontalLayout);
        formTableContainer = new TagBinderFormTableContainer();
        addComponent(getTable());
        this.setStyleName("v-tagbinderlayout"); //$NON-NLS-1$
    }

    public TextField getTfname() {
        return tfname;
    }

    public TextField getTags() {
        return tftags;
    }

    public Table getTable() {
        if (table == null) {
            table = new Table();
            table.setSizeFull();
            // selectable
            table.setSelectable(true);
            table.setMultiSelect(false);
            // react at once when something is selected
            table.setImmediate(true);
            table.setContainerDataSource(formTableContainer);
            // turn on column reordering and collapsing
            table.setColumnReorderingAllowed(true);
            table.setColumnCollapsingAllowed(true);
            table.setVisibleColumns(new String[] {TagBinderForm.PROPERTY_NAME, TagBinderForm.PROPERTY_TAGS });
            table.setColumnHeaders(new String[] {Messages.getString("TagBinderLayout.7"), Messages.getString("TagBinderLayout.8") }); //$NON-NLS-1$ //$NON-NLS-2$
        }
        return table;
    }

    public TextField getTftags() {
        return tftags;
    }

    public TagBinderFormTableContainer getFormTableContainer() {
        return formTableContainer;
    }

    public Button getBtadd() {
        return btadd;
    }

    public Button getBtremove() {
        return btremove;
    }

    public Button getBtedit() {
        return btedit;
    }

}
