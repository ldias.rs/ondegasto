package org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform;

import java.math.BigDecimal;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;

/**
 * View bean of ParceledBill.
 * @author cardoso
 */
public class ParceledBillFormBean extends BillFormBean {

    private static final long serialVersionUID = -1303841038527129969L;
    public static final String PROPERTY_NUMBER_OF_PARCELED = "numberOfParceled"; //$NON-NLS-1$
    public static final String PROPERTY_IN_VALUE = "inValue"; //$NON-NLS-1$

    private int numberOfParceled;
    private BigDecimal inValue;

    public ParceledBillFormBean(final Bill clone) {
        super(clone);
        numberOfParceled = 0;
        inValue = BigDecimal.ZERO;
        setTagsAsAtring("parcelada");
    }

    public int getNumberOfParceled() {
        return numberOfParceled;
    }

    public BigDecimal getInValue() {
        return inValue;
    }

    public void setNumberOfParceled(final int numberOfParceledArg) {
        this.numberOfParceled = numberOfParceledArg;
    }

    public void setInValue(final BigDecimal inValueArg) {
        this.inValue = inValueArg;
    }

    public static ParceledBillFormBean createDefault(final User user) {
        return new ParceledBillFormBean(BillFormBean.createDefault(user));
    }
}
