package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.action;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;

import com.vaadin.ui.Button.ClickEvent;

/**
 * Action to unpay the selected
 * {@link org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean}
 * .
 * @author cardoso
 */
public class UnPayBilllAction extends PayBillAction {

    private static final long serialVersionUID = 8266227631421450611L;

    public UnPayBilllAction(final MainFrameController mainFrameController) {
        super(mainFrameController);
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        setPaidBill(false);
    }

}
