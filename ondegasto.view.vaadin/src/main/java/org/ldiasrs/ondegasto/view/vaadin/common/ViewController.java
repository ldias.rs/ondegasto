package org.ldiasrs.ondegasto.view.vaadin.common;

import java.io.Serializable;


/**
 * Define um controlador.
 * @author cardoso
 *
 */
public interface ViewController extends Serializable {

    View getView();

}
