package org.ldiasrs.ondegasto.view.vaadin.common;

import com.vaadin.ui.Component;

/**
 * Interface de View do DmOSS.
 *
 */
public interface View extends Component {

}
