package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;

import com.github.wolfie.blackboard.annotation.ListenerMethod;

/**
 * Listener for filter the bills on the table.
 * @author ldias
 */
public interface FilterBillOnTableListener extends AppListener {

    @ListenerMethod
    void filterBillByText(FilterBillOnTableEvent event);

}
