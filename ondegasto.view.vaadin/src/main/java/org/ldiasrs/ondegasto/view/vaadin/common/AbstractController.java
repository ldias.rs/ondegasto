package org.ldiasrs.ondegasto.view.vaadin.common;

import org.ldiasrs.ondegasto.view.vaadin.authenticate.LoginController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;

import com.vaadin.ui.CustomComponent;

/**
 * Implementação abstrata de um
 * {@link org.springframework.stereotype.Controller}.
 */
public abstract class AbstractController extends CustomComponent implements ViewController {

    private static final long serialVersionUID = -1374671464173548335L;

    private UserMessageHandle messageHandle;

    protected void register() {
        if (isLoginPage()) {
            if (!isUserLogedIn()) {
                setCompositionRoot(getView());
            } else {
                setCompositionRoot(new MainFrameController());
            }
        } else {
            if (!isUserLogedIn()) {
                setCompositionRoot(new LoginController());
            } else {
                // FIXME isto pode ocasionar problema de performance caso o
                // navigator
                // instancie todas as classes de paginas
                // (no caso controllers), visto que vai sobrecarregar em criação
                // de
                // views.
                setCompositionRoot(getView());
            }
        }
    }

    private boolean isUserLogedIn() {
        return UserSessionResolver.isUserLogged();
    }

    private boolean isLoginPage() {
        return this instanceof LoginController;
    }

    public UserMessageHandle getMessageHandle() {
        if (messageHandle == null) {
            messageHandle = new UserMessageHandle(this);
        }
        return messageHandle;
    }
}
