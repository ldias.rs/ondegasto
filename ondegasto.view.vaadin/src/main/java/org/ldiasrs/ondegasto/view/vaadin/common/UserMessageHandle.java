package org.ldiasrs.ondegasto.view.vaadin.common;

import com.vaadin.ui.Component;
import com.vaadin.ui.Window.Notification;

/**
 * Handle for user messages.
 * @author cardoso
 */
public class UserMessageHandle {

    private final Component component;

    public UserMessageHandle(final Component componentArg) {
        component = componentArg;
    }

    public void showErrorMessage(final String msg) {
        component.getWindow().showNotification(msg, Notification.TYPE_ERROR_MESSAGE);
    }

    public void showWarning(final String msg) {
        component.getWindow().showNotification(msg, Notification.TYPE_WARNING_MESSAGE);
    }

    public void showInfo(final String msg) {
        component.getWindow().showNotification(msg, Notification.TYPE_HUMANIZED_MESSAGE);
    }

}
