package org.ldiasrs.ondegasto.view.vaadin.note.layout;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.VerticalLayout;

/**
 * Layout of Note.
 * @author ld24078
 */
public class NoteLayout extends VerticalLayout {

    private static final long serialVersionUID = 1197276875802241334L;
    private final Button editBt;
    private final Label richText;

    private final RichTextArea editor;

    public NoteLayout(final String html) {
        setSpacing(true);
        setStyleName("v-note-layout");
        richText = new Label(html);
        richText.setContentMode(Label.CONTENT_XHTML);
        addComponent(richText);

        editBt = new Button(Messages.getString("NoteLayout.1")); //$NON-NLS-1$
        addComponent(editBt);

        editor = new RichTextArea();
        editor.setSizeFull();
    }

    public Label getRichText() {
        return richText;
    }

    public RichTextArea getEditor() {
        return editor;
    }

    public Button getEditBt() {
        return editBt;
    }

}
