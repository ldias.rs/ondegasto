package org.ldiasrs.ondegasto.view.vaadin.bankextractor;

import java.util.Collection;
import java.util.Date;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.common.AbstractController;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.common.View;
import org.springframework.util.StringUtils;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author ld24078
 */
public class BankExtractImporterController extends AbstractController {

    private static final long serialVersionUID = 6706691771056146058L;

    private final BankExtractImporterLayout layout;

    public BankExtractImporterController() {
        layout = new BankExtractImporterLayout();
        addDefaulTags();
        addActions();
        register();
    }

    private void addDefaulTags() {
        getLayout().getTfTagsToBeAssociated().setValue("credito, visa");
    }

    private void addActions() {
        getLayout().getDatetime().addListener(new Property.ValueChangeListener() {
            private static final long serialVersionUID = 3293933536895311719L;

            @Override
            public void valueChange(final ValueChangeEvent event) {
                Object value = event.getProperty().getValue();
                if (isValidDataTime(value)) {
                    getMessageHandle().showErrorMessage(Messages.getString("BankExtractImporterController.0")); //$NON-NLS-1$
                }
            }
        });
        getLayout().getBtImporBills().addListener(new ClickListener() {
            private static final long serialVersionUID = -3645813697884775105L;

            @Override
            public void buttonClick(final ClickEvent event) {
                importBills();
            }
        });
    }

    private boolean isValidDataTime(final Object value) {
        return value == null || !(value instanceof Date);
    }

    private void importBills() {
        try {
            Object value = getLayout().getDatetime().getValue();
            if (!isValidDataTime(value)) {
                String bankExtract = getLayout().getTextArea().getValue().toString();
                UserSession userSession = UserSessionResolver.getUserSession();
                String tagsToBeAssociatedAsStr = (String) getLayout().getTfTagsToBeAssociated().getValue();
                String[] tagsToAssociate = StringUtils.hasText(tagsToBeAssociatedAsStr) ? tagsToBeAssociatedAsStr.split(",") : null;
                Collection<Bill> bills = userSession.getBankBillsExtractorService().extractBills(userSession.getUser(), bankExtract, (Date) value,
                        tagsToAssociate);
                userSession.getBillList().add(bills);
                getLayout().getTextArea().setValue(""); //$NON-NLS-1$
                getMessageHandle().showInfo(Messages.getString("BankExtractImporterController.2")); //$NON-NLS-1$
            } else {
                getMessageHandle().showErrorMessage(Messages.getString("BankExtractImporterController.3")); //$NON-NLS-1$
            }
        } catch (Exception e) {
            getMessageHandle().showErrorMessage(Messages.getString("BankExtractImporterController.4")); //$NON-NLS-1$
            e.printStackTrace();
        }
    }

    @Override
    public View getView() {
        return getLayout();
    }

    public BankExtractImporterLayout getLayout() {
        return layout;
    }

}
