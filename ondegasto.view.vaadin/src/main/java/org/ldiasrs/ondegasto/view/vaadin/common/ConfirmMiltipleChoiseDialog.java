package org.ldiasrs.ondegasto.view.vaadin.common;

import org.apache.commons.lang.StringUtils;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.BaseTheme;

/**
 * @author ld24078
 */
public class ConfirmMiltipleChoiseDialog {

    private static final String V_PAINEL_CONFIRM_DIALOG = "v-painel-confirm-dialog";
    private static final String V_BT_CONFIRM_DIALOG = "v-bt-confirm-dialog";

    private final Window subWindow;
    private final Button allBillsBt;
    private final Button fromThatBt;
    private final Button singleBillBt;
    private final Button cancelBt;

    public ConfirmMiltipleChoiseDialog(final String title, final String singleBeanBtText) {
        this(title, null, singleBeanBtText, null);

    }

    public ConfirmMiltipleChoiseDialog(final String title, final String allBeansBtText, final String singleBeanBtText) {
        this(title, allBeansBtText, singleBeanBtText, null);

    }

    public ConfirmMiltipleChoiseDialog(final String title, final String allBeansBtText, final String singleBeanBtText, final String fromThatBtText) {
        Layout layout = new HorizontalLayout();
        layout.setStyleName(V_PAINEL_CONFIRM_DIALOG);
        layout.setMargin(true);
        allBillsBt = new Button(allBeansBtText);
        allBillsBt.setStyleName(V_BT_CONFIRM_DIALOG);
        if (!StringUtils.isEmpty(allBeansBtText)) {
            layout.addComponent(allBillsBt);
        }
        fromThatBt = new Button(fromThatBtText);
        fromThatBt.setStyleName(V_BT_CONFIRM_DIALOG);
        if (!StringUtils.isEmpty(fromThatBtText)) {
            layout.addComponent(fromThatBt);
        }
        singleBillBt = new Button(singleBeanBtText);
        singleBillBt.setStyleName(V_BT_CONFIRM_DIALOG);
        layout.addComponent(singleBillBt);
        cancelBt = new Button("Cancelar");
        cancelBt.setStyleName(BaseTheme.BUTTON_LINK);
        layout.addComponent(cancelBt);
        subWindow = new Window();
        subWindow.setCaption(title);
        subWindow.setContent(layout);
        subWindow.setModal(true);
    }

    public Window getSubWindow() {
        return subWindow;
    }

    public Button getAllBillsBt() {
        return allBillsBt;
    }

    public Button getSingleBillBt() {
        return singleBillBt;
    }

    public Button getCancelBt() {
        return cancelBt;
    }

    public Button getNewerThanBt() {
        return fromThatBt;
    }
}
