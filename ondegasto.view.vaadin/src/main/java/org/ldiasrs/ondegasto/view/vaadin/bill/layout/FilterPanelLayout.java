package org.ldiasrs.ondegasto.view.vaadin.bill.layout;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.filteractions.FilterActionsType;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * Panel for filters.
 * @author ld24078
 */
public class FilterPanelLayout extends HorizontalLayout {

    private static final long serialVersionUID = -8022169926194945070L;

    private TextField tfValue;
    private ComboBox btActions;
    private Button btProcess;

    public FilterPanelLayout() {
        // tf
        addValueTexfield();

        addActionComboBox();

        addProcessButton();
    }

    private void addProcessButton() {
        btProcess = new Button(Messages.getString("FilterPanelLayout.3")); //$NON-NLS-1$
        addComponent(btProcess);
    }

    private void addActionComboBox() {
        addComponent(new Label(Messages.getString("FilterPanelLayout.1"))); //$NON-NLS-1$
        btActions = new ComboBox();
        setStyleName("v-actions-filters"); //$NON-NLS-1$
        for (FilterActionsType type : FilterActionsType.values()) {
            btActions.addItem(type);
        }
        btActions.setImmediate(true);
        btActions.setInvalidAllowed(false);
        btActions.setNullSelectionAllowed(false);
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(btActions);
        Label exampleLabel = new Label(Messages.getString("FilterPanelLayout.5"));
        exampleLabel.setStyleName("v-filterPanelLayout-exampleLabel");
        layout.addComponent(exampleLabel);
        addComponent(layout);
    }

    private void addValueTexfield() {
        tfValue = new TextField();
        tfValue.setWidth("480px"); //$NON-NLS-1$
        tfValue.setImmediate(false);
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(tfValue);
        Label exampleLabel = new Label(Messages.getString("FilterPanelLayout.4"));
        exampleLabel.setStyleName("v-filterPanelLayout-exampleLabel");
        layout.addComponent(exampleLabel);
        addComponent(layout);
    }

    public ComboBox getBtActions() {
        return btActions;
    }

    public TextField getTfValue() {
        return tfValue;
    }

    public Button getBtProcess() {
        return btProcess;
    }

}
