package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;

import com.github.wolfie.blackboard.annotation.ListenerMethod;

/**
 * Define a Listener for Login.
 * @author ld24078
 */
public interface LoginListener extends AppListener {

    @ListenerMethod
    void userLogedIn(UserLoggedInEvent event);

}
