package org.ldiasrs.ondegasto.view.vaadin.bankextractor;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * @author ld24078
 */
public class BankExtractImporterLayout extends VerticalLayout implements View {

    private static final long serialVersionUID = 1197276875802241334L;
    private final Button importBills;
    private final TextArea textArea;
    private final PopupDateField datetime;
    private final TextField tfTagsToBeAssociated;

    public BankExtractImporterLayout() {
        setSpacing(true);
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.addComponent(new Label(Messages.getString("BankExtractImporterLayout.0")));
        datetime = new PopupDateField(); //$NON-NLS-1$
        // Set the value of the PopupDateField to current date
        datetime.setValue(new java.util.Date());
        // Set the correct resolution
        datetime.setResolution(PopupDateField.RESOLUTION_DAY);
        datetime.setImmediate(true);
        horizontalLayout.addComponent(datetime);
        tfTagsToBeAssociated = new TextField();
        horizontalLayout.addComponent(tfTagsToBeAssociated);
        horizontalLayout.addComponent(new Label("separado por virgula p.e: visa, master"));
        importBills = new Button(Messages.getString("BankExtractImporterLayout.3")); //$NON-NLS-1$
        horizontalLayout.addComponent(importBills);
        addComponent(horizontalLayout);
        textArea = new TextArea();
        textArea.setWidth("100%"); //$NON-NLS-1$
        textArea.setHeight("100%"); //$NON-NLS-1$
        addComponent(textArea);
        setStyleName("v-bank-importer"); //$NON-NLS-1$
    }

    public Button getBtImporBills() {
        return importBills;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public PopupDateField getDatetime() {
        return datetime;
    }

    public TextField getTfTagsToBeAssociated() {
        return tfTagsToBeAssociated;
    }
}
