package org.ldiasrs.ondegasto.view.vaadin.note.controller.action;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.springframework.util.StringUtils;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.Notification;

/**
 * Acao de nova Nota.
 * @author ld24078
 */
public class NewNoteAction implements ClickListener {

    private static final long serialVersionUID = 1212389128790578272L;

    private void newNoteAction(final Window window) {
        final Window subWindow = new Window(Messages.getString("NewNoteAction.0")); //$NON-NLS-1$
        subWindow.setModal(true);
        VerticalLayout layout = (VerticalLayout) subWindow.getContent();
        final TextField tfTitle = new TextField();
        layout.addComponent(tfTitle);
        layout.setComponentAlignment(tfTitle, Alignment.MIDDLE_CENTER);
        Button closeBt = new Button(Messages.getString("NewNoteAction.1"), new Button.ClickListener() { //$NON-NLS-1$
            private static final long serialVersionUID = -6724573976664301355L;

            @Override
            public void buttonClick(final ClickEvent event) {
                String nomeNota = tfTitle.getValue().toString();
                if (StringUtils.hasText(nomeNota)) {
                    UserSession userSession = UserSessionResolver.getUserSession();
                    User user = userSession.getUser();
                    Note note = new Note(user, nomeNota, "<h1>" + nomeNota + "</h1>"); //$NON-NLS-1$ //$NON-NLS-2$
                    try {
                        userSession.getNoteList().add(note);
                    } catch (OperationServiceException e) {
                        window.showNotification(Messages.getString("NewNoteAction.4"), e.getMessage(), Notification.TYPE_ERROR_MESSAGE); //$NON-NLS-1$
                    }
                }
                subWindow.getParent().removeWindow(subWindow);
            }
        });
        layout.addComponent(closeBt);
        layout.setComponentAlignment(closeBt, Alignment.BOTTOM_CENTER);
        window.addWindow(subWindow);
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        newNoteAction(event.getComponent().getWindow());
    }

}
