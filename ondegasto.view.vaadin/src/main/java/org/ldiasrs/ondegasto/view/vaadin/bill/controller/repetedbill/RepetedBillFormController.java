package org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill;

import java.util.Arrays;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.RepetedBillSubWindow;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Form;

/**
 * Controller for window {@link RepetedBillSubWindow}.
 * @author ld24078
 */
public class RepetedBillFormController {

    /**
     * Define the type os edition.
     */
    public enum EditModeType {
        ALL, NEWERS
    }

    private final RepetedBillFormBean formBean;
    private RepetedBillSubWindow subWindow;
    private RepeatedBill editingDomainBean;
    private EditModeType editModeType;

    /**
     * Contructor on INSERT MODE.
     */
    public RepetedBillFormController() {
        this(RepetedBillFormBean.createDefault(UserSessionResolver.getUserSession().getUser()));
    }

    /**
     * Constructor on EDIT MODE NEWERS.
     * @param domainBeanArg
     */
    public RepetedBillFormController(final RepeatedBill domainBeanArg, final BillFormBean selectedBill, final EditModeType editModeTypeArg) {
        this(new RepetedBillFormBean(domainBeanArg, selectedBill));
        editingDomainBean = domainBeanArg;
        editModeType = editModeTypeArg;
    }

    public RepetedBillFormController(final RepetedBillFormBean bean) {
        formBean = bean;
    }

    public RepetedBillSubWindow getSubWindow() {
        if (subWindow == null) {
            createSubWindow();
        }
        return subWindow;
    }

    private void createSubWindow() {
        subWindow = new RepetedBillSubWindow();
        setFormProperties();
        setVisibleFields();
        setFormAction();
    }

    private void setFormProperties() {
        subWindow.getForm().setItemDataSource(new BeanItem<RepetedBillFormBean>(formBean));
        subWindow.getForm().setFormFieldFactory(new RepetedBillFormFieldFactory(isEditMode()));
        FieldPropertiesUtil.applyDoubleValueConverter(subWindow.getForm().getField(Bill.PROPERTY_VALUEOFBILL));
    }

    private boolean isEditMode() {
        return editingDomainBean != null;
    }

    private void setVisibleFields() {
        subWindow.getForm()
                .setVisibleItemProperties(
                        Arrays.asList(new String[] {RepetedBillFormBean.PROPERTY_NAME, RepetedBillFormBean.PROPERTY_VALUEOFBILL,
                                RepetedBillFormBean.PROPERTY_DATEOFBILL, RepetedBillFormBean.PROPERTY_PAID, RepetedBillFormBean.PROPERTY_OBSERVATION,
                                RepetedBillFormBean.PROPERTY_TAGS_AS_STRING, RepetedBillFormBean.PROPERTY_TYPE,
                                RepetedBillFormBean.PROPERTY_NUMBER_REPETION }));
    }

    private void setFormAction() {
        Form form = subWindow.getForm();
        AddOrUpdateRepedtedBillAction addUpdateAction = new AddOrUpdateRepedtedBillAction(formBean, subWindow, form, editingDomainBean, editModeType);
        FieldPropertiesUtil.createFormApplyButton(form, addUpdateAction);
    }

}
