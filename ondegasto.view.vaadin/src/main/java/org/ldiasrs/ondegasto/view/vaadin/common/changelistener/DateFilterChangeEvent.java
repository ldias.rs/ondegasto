package org.ldiasrs.ondegasto.view.vaadin.common.changelistener;

import java.util.Date;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppEvent;

/**
 * Event invoke when the user change the date filtered.
 * @author ldias
 */
public class DateFilterChangeEvent implements AppEvent {

    private final Date currentDate;

    public DateFilterChangeEvent(final Date currentDateArg) {
        this.currentDate = currentDateArg;
    }

    public Date getDate() {
        return currentDate;
    }

}
