package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe;

/**
 * Exceção na operação de algum serviço.
 * @author cardoso
 */
public class OperationServiceException extends AbstractOperationServiceException {

    private static final long serialVersionUID = 6800689248770318842L;

    public OperationServiceException(final String msg) {
        super(msg, null);
    }

    public OperationServiceException(final String msg, final Throwable e) {
        super(msg, e);
    }

}
