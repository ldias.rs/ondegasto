package org.ldiasrs.ondegasto.view.vaadin.bill.controller.tableview;

import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.service.common.eventmanager.ModelCrudChangeListener;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;
import org.ldiasrs.ondegasto.service.usersession.UserSession;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.filteractions.FilterActionsType;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.BillTableContainer;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.MainFrameController;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe.exception.CouldNotFoundBillFormBean;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserMessageHandle;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.DateFilterChangeEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.DateFilterChangeListener;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.FilterBillOnTableEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.FilterBillOnTableListener;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ModifiedListViewOfBillFormEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;

import com.vaadin.data.Container.Filterable;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.util.filter.Not;
import com.vaadin.ui.Window.Notification;

/**
 * Control the table view of {@link BillFormBean}.
 * @author cardoso
 */
public class TableViewController implements ListViewOfBillForm, ModelCrudChangeListener<Bill>, DateFilterChangeListener, FilterBillOnTableListener {

    private final BillTableContainer billTableContainer;
    private final UserMessageHandle userMessageHandle;
    private int currentMonth;
    private int currentYear;
    private final MainFrameController mainFrameController;

    public TableViewController(final BillTableContainer billTableContainerParm, final UserMessageHandle userMessageHandleParm,
            final MainFrameController mainFrameControllerArg) {
        billTableContainer = billTableContainerParm;
        userMessageHandle = userMessageHandleParm;
        mainFrameController = mainFrameControllerArg;
        UserSessionResolver.getUserSession().getServiceEventManager().addListener(this);
        ViewEventManager.getInstance().addListener(this);
    }

    private void showBills(final Collection<BillFormBean> bills) {
        billTableContainer.removeAllItems();
        for (Bill bill : bills) {
            billTableContainer.addBean(new BillFormBean(bill));
        }
        fireModifiedViewEvent();
    }

    @Override
    public final Collection<BillFormBean> getBillFormList() {
        return billTableContainer.getItemIds();
    }

    private BillFormBean searchBillOnTable(final Bill bill) throws CouldNotFoundBillFormBean {
        for (BillFormBean formBean : billTableContainer.getItemIds()) {
            if (formBean.getId() == bill.getId()) {
                return formBean;
            }
        }
        throw new CouldNotFoundBillFormBean();
    }

    private void updateBillTable(final BillFormBean formBill) {
        if (tableContainsBill(formBill)) {
            BeanItem<BillFormBean> item = billTableContainer.getItem(formBill);
            item.getItemProperty(Bill.PROPERTY_DATEOFBILL).setValue(formBill.getDateOfBill());
            item.getItemProperty(Bill.PROPERTY_PAID).setValue(Boolean.valueOf(formBill.isPaid()));
            item.getItemProperty(Bill.PROPERTY_NAME).setValue(formBill.getName());
            item.getItemProperty(Bill.PROPERTY_OBSERVATION).setValue(formBill.getObservation());
            item.getItemProperty(Bill.PROPERTY_TYPE).setValue(formBill.getType());
            item.getItemProperty(Bill.PROPERTY_VALUEOFBILL).setValue(formBill.getValueOfBill());
        }
    }

    private boolean tableContainsBill(final BillFormBean formBill) {
        return billTableContainer.containsId(formBill);
    }

    @Override
    public final void changeEvent(final ChangeEvent<Bill> event) {
        if (event.getEntity() instanceof Bill) {
            Bill bill = event.getEntity();
            if (changeBillIsCompatibleWithCurrentFilter(bill)) {
                ChangeType changeType = event.getChangeType();
                BillFormBean formBean = null;
                try {
                    switch (changeType) {
                    case INSERT:
                        formBean = new BillFormBean(bill);
                        billTableContainer.addItem(formBean);
                        break;
                    case DELETE:
                        formBean = searchBillOnTable(bill);
                        billTableContainer.removeItem(formBean);
                        break;
                    case UPDATE:
                        formBean = searchBillOnTable(bill);
                        formBean.changeProperties(bill);
                        updateBillTable(formBean);
                        break;
                    default:
                        break;
                    }
                    if (formBean == null) {
                        throw new CouldNotFoundBillFormBean();
                    }
                    fireModifiedViewEvent();
                } catch (CouldNotFoundBillFormBean e) {
                    userMessageHandle.showErrorMessage("Erro na atualização dos dados");
                }
            }
        }
    }

    private boolean changeBillIsCompatibleWithCurrentFilter(final Bill bill) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(bill.getDateOfBill());
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        return currentMonth == month && currentYear == year;
    }

    private void filterByMonth(final int month, final int year) {
        currentMonth = month;
        currentYear = year;
        loadBillsByMonth();
    }

    private void loadBillsByMonth() {
        UserSession userSession = UserSessionResolver.getUserSession();
        List<Bill> bills = userSession.getBillList().findBillByMonthAndYear(userSession.getUser(), currentMonth, currentYear);
        showBills(BillFormBean.convertToViewObject(bills));
    }

    @Override
    public final void dateFilterChange(final DateFilterChangeEvent event) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(event.getDate());
        filterByMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
    }

    @Override
    public void filterBillByText(final FilterBillOnTableEvent event) {
        String filterStr = event.getFilterStr().trim();
        Filterable filterTable = billTableContainer;
        if (!filterStr.isEmpty()) {
            processSelectedFilterAction(event, filterStr, filterTable);
        } else {
            removeAllFilters(filterTable);
        }
        fireModifiedViewEvent();
    }

    private void fireModifiedViewEvent() {
        ViewEventManager.getInstance().fire(new ModifiedListViewOfBillFormEvent());
    }

    private void processSelectedFilterAction(final FilterBillOnTableEvent event, final String filterStr, final Filterable filterTable) {
        StringTokenizer sk = new StringTokenizer(filterStr, ","); //$NON-NLS-1$
        /*
         * TODO [cardoso][MEDIA] Utilizar nomes de headers de tabela em um ponto
         * central
         */
        if (!isAddTagAction(event)) {
            removeAllFilters(filterTable);
        }
        while (sk.hasMoreElements()) {
            String filterElementStr = sk.nextToken().trim();
            if (isAddTagAction(event)) {
                addTagsAction(filterElementStr);
            } else {
                processFilterAction(event, filterTable, filterElementStr);
            }
        }
    }

    private void processFilterAction(final FilterBillOnTableEvent event, final Filterable filterTable, final String filterElementStr) {
        switch (event.getFilerType()) {
        case FILTER_BY_TAG:
            filterByTagsAction(filterTable, filterElementStr);
            break;
        case FILTER_BY_NAME:
            filterByNameAction(filterTable, filterElementStr);
            break;
        case FILTER_BY_HIDE_TAGS:
            filterByHideTagAction(filterTable, filterElementStr);
            break;
        case FILTER_SHOW_ALL:
            removeAllFilters(filterTable);
            break;
        default:
            break;
        }
    }

    private boolean isAddTagAction(final FilterBillOnTableEvent event) {
        return FilterActionsType.ADD_TAGS.equals(event.getFilerType());
    }

    private void filterByHideTagAction(final Filterable filterTable, final String filterElementStr) {
        filterTable.addContainerFilter(new Not(createFilterTagLike(filterElementStr))); //$NON-NLS-1$ //$NON-NLS-2$
    }

    private void removeAllFilters(final Filterable filterTable) {
        filterTable.removeAllContainerFilters();
    }

    private void filterByTagsAction(final Filterable filterTable, final String filterElementStr) {
        filterTable.addContainerFilter(createFilterTagLike(filterElementStr)); //$NON-NLS-1$ //$NON-NLS-2$
    }

    private Like createFilterTagLike(final String filterElementStr) {
        return new Like(BillFormBean.PROPERTY_TAGS_AS_STRING, "%" + filterElementStr + "%", false);
    }

    private void filterByNameAction(final Filterable filterTable, final String filterElementStr) {
        filterTable.addContainerFilter(new Like(BillFormBean.PROPERTY_NAME, "%" + filterElementStr + "%", false)); //$NON-NLS-1$ //$NON-NLS-2$
    }

    private void addTagsAction(final String filterElementStr) {
        Collection<BillFormBean> list = mainFrameController.getSelectedBills();
        if (list.isEmpty()) {
            mainFrameController.getWindow().showNotification(Messages.getString("TableViewController.5"), Notification.TYPE_WARNING_MESSAGE); //$NON-NLS-1$
        } else {
            for (BillFormBean billFormBean : list) {
                UserSession userSession = UserSessionResolver.getUserSession();
                Tag tag = userSession.getBillTagList().getTag(userSession.getUser(), filterElementStr);
                billFormBean.addTag(tag);
                userSession.getBillList().update(BillFormBean.convertToDomainObject(billFormBean));
            }
        }
    }
}
