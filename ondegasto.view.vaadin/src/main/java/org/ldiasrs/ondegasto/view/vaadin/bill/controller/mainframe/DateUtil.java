package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

/**
 * Fornece utilitários para Datas.
 * @author cardoso
 */
public final class DateUtil {

    private DateUtil() {
    }

    public static int getCurrentYear() {
        return GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
    }

    public static int getCurrentMonth() {
        return GregorianCalendar.getInstance().get(GregorianCalendar.MONTH);
    }

    public static Date getStartTimeOfMonth(final int month, final int year) {
        GregorianCalendar startDate = new GregorianCalendar();
        startDate.set(year, month, startDate.getMinimum(GregorianCalendar.DATE), startDate.getMinimum(GregorianCalendar.HOUR),
                startDate.getMinimum(GregorianCalendar.MINUTE));

        return startDate.getTime();
    }

    public static Date getEndTimeOfMonth(final int month, final int year) {
        GregorianCalendar endDate = new GregorianCalendar();
        endDate.set(year, month, endDate.getActualMaximum(GregorianCalendar.DATE), endDate.getActualMaximum(GregorianCalendar.HOUR),
                endDate.getActualMaximum(GregorianCalendar.MINUTE));
        return endDate.getTime();
    }

    @SuppressWarnings("deprecation")
    public static String formatLabelDate(final Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy"); //$NON-NLS-1$
        return String.format("%s %s", getStringOfMonth(date.getMonth()), df.format(date)); //$NON-NLS-1$
    }

    private static String getStringOfMonth(final int mes) {
        // TODO [cardoso][ondegasto][BAIXA] Refatorar esse lixo
        switch (mes) {
        case 0:
            return Messages.getString("DateUtil.2"); //$NON-NLS-1$
        case 1:
            return Messages.getString("DateUtil.3"); //$NON-NLS-1$
        case 2:
            return Messages.getString("DateUtil.4"); //$NON-NLS-1$
        case 3:
            return Messages.getString("DateUtil.5"); //$NON-NLS-1$
        case 4:
            return Messages.getString("DateUtil.6"); //$NON-NLS-1$
        case 5:
            return Messages.getString("DateUtil.7"); //$NON-NLS-1$
        case 6:
            return Messages.getString("DateUtil.8"); //$NON-NLS-1$
        case 7:
            return Messages.getString("DateUtil.9"); //$NON-NLS-1$
        case 8:
            return Messages.getString("DateUtil.10"); //$NON-NLS-1$
        case 9:
            return Messages.getString("DateUtil.11"); //$NON-NLS-1$
        case 10:
            return Messages.getString("DateUtil.12"); //$NON-NLS-1$
        case 11:
            return Messages.getString("DateUtil.13"); //$NON-NLS-1$
        default:
            return Messages.getString("DateUtil.14"); //$NON-NLS-1$
        }
    }

}
