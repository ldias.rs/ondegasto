package org.ldiasrs.ondegasto.view.vaadin.note.controller.action;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.note.controller.NoteController;
import org.ldiasrs.ondegasto.view.vaadin.note.layout.NoteLayout;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.Notification;

/**
 * Action that process the edit of the note.
 * @author ld24078
 */
public class EditNoteAction implements ClickListener {

    private static final long serialVersionUID = -8942601002995009340L;
    private final NoteLayout noteLayout;
    private final NoteController controller;

    public EditNoteAction(final NoteLayout noteLayoutArg, final NoteController controllerArg) {
        noteLayout = noteLayoutArg;
        controller = controllerArg;
    }

    @Override
    public void buttonClick(final ClickEvent event) {
        if (noteLayout.getComponentIterator().next() == noteLayout.getRichText()) {
            noteLayout.getEditor().setValue(noteLayout.getRichText().getValue());
            noteLayout.replaceComponent(noteLayout.getRichText(), noteLayout.getEditor());
            noteLayout.getEditBt().setCaption(Messages.getString("EditNoteAction.0")); //$NON-NLS-1$
        } else {
            String value = noteLayout.getEditor().getValue().toString();
            noteLayout.getRichText().setValue(value);
            noteLayout.replaceComponent(noteLayout.getEditor(), noteLayout.getRichText());
            Note editedNote = controller.getCachedNoteByName(noteLayout.getCaption());
            editedNote.setHtml(value);
            try {
                UserSessionResolver.getUserSession().getNoteList().update(editedNote);
            } catch (OperationServiceException e) {
                event.getButton().getWindow().showNotification(e.getMessage(), Notification.TYPE_ERROR_MESSAGE);
            }
            noteLayout.getEditBt().setCaption(Messages.getString("EditNoteAction.1")); //$NON-NLS-1$
        }
    }

}
