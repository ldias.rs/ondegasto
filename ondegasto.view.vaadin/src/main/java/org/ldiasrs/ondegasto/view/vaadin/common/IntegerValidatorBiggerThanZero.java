package org.ldiasrs.ondegasto.view.vaadin.common;

import com.vaadin.data.validator.IntegerValidator;

/**
 * Validate if the value is a Integer an bigger than zero.
 * @author ld24078
 */
public class IntegerValidatorBiggerThanZero extends IntegerValidator {

    private static final long serialVersionUID = 1L;

    public IntegerValidatorBiggerThanZero(final String errorMessage) {
        super(errorMessage);
    }

    @Override
    protected boolean isValidString(final String value) {
        if (super.isValidString(value)) {
            return Integer.valueOf(value).intValue() > 0;
        }
        return false;
    }

}
