package org.ldiasrs.ondegasto.view.vaadin.note.layout;

import java.util.HashMap;
import java.util.Map;

import org.ldiasrs.ondegasto.view.vaadin.common.Messages;
import org.ldiasrs.ondegasto.view.vaadin.common.View;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

/**
 * Layout para lista de notas.
 * @author ld24078
 */
public class NoteListLayout extends VerticalLayout implements View {

    private static final long serialVersionUID = 1130755066967312555L;

    private final TabSheet tabSheet;
    private final Map<String, NoteLayout> tabComponents;

    private final Button newBt;

    private final Button removeBt;

    public NoteListLayout() {
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.setStyleName("v-note-layout");
        newBt = new Button(Messages.getString("NoteListLayout.2")); //$NON-NLS-1$
        buttonsLayout.addComponent(newBt);
        removeBt = new Button(Messages.getString("NoteListLayout.3")); //$NON-NLS-1$
        buttonsLayout.addComponent(removeBt);
        addComponent(buttonsLayout);
        tabSheet = new TabSheet();
        addComponent(tabSheet);
        tabComponents = new HashMap<String, NoteLayout>();
    }

    public void addTab(final NoteLayout layout) {
        tabSheet.addTab(layout, layout.getCaption());
        tabComponents.put(layout.getCaption(), layout);
    }

    public void removeTab(final String tabname) {
        tabSheet.removeTab(tabSheet.getTab(tabComponents.remove(tabname)));
    }

    public String getSelectedTabName() {
        return tabSheet.getSelectedTab().getCaption();
    }

    public Button getRemoveBt() {
        return removeBt;
    }

    public Button getNewBt() {
        return newBt;
    }

}
