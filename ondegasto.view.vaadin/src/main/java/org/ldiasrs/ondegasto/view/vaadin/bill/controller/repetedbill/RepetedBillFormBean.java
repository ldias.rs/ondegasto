package org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormBean;

/**
 * Form to create a
 * {@link org.ldiasrs.ondegasto.service.domain.bill.RepeatedBill}.
 * @author ld24078
 */
public class RepetedBillFormBean extends BillFormBean {

    private static final int INITAIL_NUMEBR_OF_REPETITION = 12;

    private static final long serialVersionUID = -7521600835815179724L;

    public static final String PROPERTY_NUMBER_REPETION = "numberOfRepetion";//$NON-NLS-1$

    private int numberOfRepetion;

    public RepetedBillFormBean(final Bill clone) {
        super(clone);
        numberOfRepetion = INITAIL_NUMEBR_OF_REPETITION;
        super.setTagsAsAtring("repetida"); //$NON-NLS-1$
    }

    public RepetedBillFormBean(final RepeatedBill repedtedBill, final Bill selectedBill) {
        super(selectedBill);
        setId(repedtedBill.getId());
        setNumberOfRepetion(repedtedBill.getRepeatedBills().size());
    }

    public int getNumberOfRepetion() {
        return numberOfRepetion;
    }

    public void setNumberOfRepetion(final int numberOfRepetionArg) {
        numberOfRepetion = numberOfRepetionArg;
    }

    public static RepetedBillFormBean createDefault(final User user) {
        return new RepetedBillFormBean(new Bill(user, "", 0, BillType.COST)); //$NON-NLS-1$
    }

    public static RepeatedBill convertToDomainObject(final RepetedBillFormBean bill) {
        RepeatedBill repeatedBill = new RepeatedBill(bill, bill.getNumberOfRepetion());
        repeatedBill.setId(bill.getId());
        return repeatedBill;
    }

}
