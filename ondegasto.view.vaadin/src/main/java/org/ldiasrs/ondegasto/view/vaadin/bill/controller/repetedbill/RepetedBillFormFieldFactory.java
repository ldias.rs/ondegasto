package org.ldiasrs.ondegasto.view.vaadin.bill.controller.repetedbill;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormFieldFactory;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.IntegerValidatorBiggerThanZero;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;

/**
 * Factory of fields for {@link RepetedBillFormBean}.
 * @author ld24078
 */
public class RepetedBillFormFieldFactory extends BillFormFieldFactory {

    private static final long serialVersionUID = 9048744433855844374L;
    private final boolean isEditMode;

    public RepetedBillFormFieldFactory(final boolean isEditModeArg) {
        isEditMode = isEditModeArg;
    }

    @Override
    public Field createField(final Item item, final Object propertyId, final Component uiContext) {
        Field f = super.createField(item, propertyId, uiContext);

        if (RepetedBillFormBean.PROPERTY_NUMBER_REPETION.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldPropertiesWithoutValidator(Messages.getString("RepetedBillFormFieldFactory.1"), f);
            f.addValidator(new IntegerValidatorBiggerThanZero(Messages.getString("RepetedBillFormFieldFactory.0"))); //$NON-NLS-1$
            if (isEditMode) {
                f.setEnabled(false);
            }
        }
        return f;
    }
}
