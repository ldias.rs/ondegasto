package org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform;

import java.util.Arrays;

import org.ldiasrs.ondegasto.common.StringUtil;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.BillSubWindow;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.DateFilterChangeEvent;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.DateFilterChangeListener;
import org.ldiasrs.ondegasto.view.vaadin.common.changelistener.ViewEventManager;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Component.Event;
import com.vaadin.ui.Component.Listener;
import com.vaadin.ui.TextField;

/**
 * Controlador da view {@link BillSubWindow}.
 * @author cardoso
 */
public class BillFormControler implements DateFilterChangeListener {

    private final BillFormBean formBean;
    private BillSubWindow billSubWindow;

    public BillFormControler() {
        this(BillFormBean.createDefault(UserSessionResolver.getUserSession().getUser()));
        ViewEventManager.getInstance().addListener(this);
    }

    public BillFormControler(final BillFormBean formBeanParam) {
        formBean = formBeanParam;
    }

    public final BillSubWindow getBillPropertiesSubWindow() {
        if (billSubWindow == null) {
            billSubWindow = new BillSubWindow();
            createFormBill();
        }
        return billSubWindow;
    }

    private void createFormBill() {
        // set the form bean on form
        billSubWindow.getForm().setItemDataSource(new BeanItem<BillFormBean>(formBean));
        billSubWindow.getForm().setFormFieldFactory(new BillFormFieldFactory());
        // set visible fields
        billSubWindow.getForm().setVisibleItemProperties(
                Arrays.asList(new String[] {Bill.PROPERTY_NAME, Bill.PROPERTY_VALUEOFBILL, Bill.PROPERTY_DATEOFBILL, Bill.PROPERTY_PAID,
                        Bill.PROPERTY_OBSERVATION, BillFormBean.PROPERTY_TAGS_AS_STRING, Bill.PROPERTY_TYPE, }));
        // set converter's
        FieldPropertiesUtil.applyDoubleValueConverter(billSubWindow.getForm().getField(Bill.PROPERTY_VALUEOFBILL));
        // add action on form
        AddOrUpdateBillAction addOrUpdateBillAction = new AddOrUpdateBillAction(formBean, billSubWindow, billSubWindow.getForm());
        FieldPropertiesUtil.createFormApplyButton(billSubWindow.getForm(), addOrUpdateBillAction);
        addAutomaticTagEvent();
    }

    private void addAutomaticTagEvent() {
        final TextField nameField = (TextField) billSubWindow.getForm().getField(Bill.PROPERTY_NAME);
        nameField.setImmediate(true);
        nameField.addListener(new Listener() {
            private static final long serialVersionUID = -5656626542862313261L;

            @Override
            public void componentEvent(final Event event) {
                if (event instanceof ValueChangeEvent) {
                    TextField tagsTf = (TextField) billSubWindow.getForm().getField(BillFormBean.PROPERTY_TAGS_AS_STRING);
                    String typeBillStr = formBean.getType().getLabel();
                    String existTagsAsString = tagsTf.getValue().toString();
                    String newNameTagsAsString = nameField.getValue().toString().replace(" ", ",");
                    String mergedTagsAsString = StringUtil.mergeStringTags(new String[] {existTagsAsString.toLowerCase() + "," + typeBillStr,
                            newNameTagsAsString.toLowerCase() });
                    tagsTf.setValue(mergedTagsAsString.toLowerCase());
                }
            }
        });
    }

    public final BillFormBean getBillBean() {
        return formBean;
    }

    @Override
    public void dateFilterChange(final DateFilterChangeEvent event) {
        formBean.setDateOfBill(event.getDate());
    }
}
