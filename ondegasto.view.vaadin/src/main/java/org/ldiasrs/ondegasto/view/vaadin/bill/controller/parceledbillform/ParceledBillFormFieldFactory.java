package org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform;

import org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform.BillFormFieldFactory;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.IntegerValidatorBiggerThanZero;
import org.ldiasrs.ondegasto.view.vaadin.common.Messages;

import com.vaadin.data.Item;
import com.vaadin.data.validator.IntegerValidator;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;

/**
 * Create the view field's for entity {@link ParceledBillFormBean}.
 * @author cardoso
 */
public class ParceledBillFormFieldFactory extends BillFormFieldFactory {

    private static final long serialVersionUID = 1733212196624398501L;

    @Override
    public Field createField(final Item item, final Object propertyId, final Component uiContext) {
        Field f = super.createField(item, propertyId, uiContext);

        if (ParceledBillFormBean.PROPERTY_NUMBER_OF_PARCELED.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldIntegerProperties(Messages.getString("ParceledBillFormFieldFactory.2"), f);
            f.addValidator(new IntegerValidatorBiggerThanZero(Messages.getString("ParceledBillFormFieldFactory.0"))); //$NON-NLS-1$
        } else if (ParceledBillFormBean.PROPERTY_IN_VALUE.equals(propertyId)) {
            FieldPropertiesUtil.setTextFieldDoubleProperties(Messages.getString("ParceledBillFormFieldFactory.3"), f);
            f.addValidator(new IntegerValidator(Messages.getString("ParceledBillFormFieldFactory.1"))); //$NON-NLS-1$
        } else {
            super.createField(item, propertyId, uiContext);
        }
        return f;
    }

}
