package org.ldiasrs.ondegasto.view.vaadin.bill.controller.parceledbillform;

import java.util.Arrays;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.view.vaadin.bill.layout.ParceledBillSubWindow;
import org.ldiasrs.ondegasto.view.vaadin.common.FieldPropertiesUtil;
import org.ldiasrs.ondegasto.view.vaadin.common.UserSessionResolver;

import com.vaadin.data.util.BeanItem;

/**
 * Controller of view {@link ParceledBillSubWindow}.
 * @author cardoso
 */
public class ParceledBillFormController {

    private ParceledBillFormBean formBean;
    private ParceledBillSubWindow subWindow;

    public ParceledBillFormController() {
        this(ParceledBillFormBean.createDefault(UserSessionResolver.getUserSession().getUser()));
    }

    public ParceledBillFormController(final ParceledBillFormBean bean) {
        this.formBean = bean;
    }

    public ParceledBillSubWindow getSubWindow() {
        if (subWindow == null) {
            createSubWindow();
        }
        return subWindow;
    }

    private void createSubWindow() {
        subWindow = new ParceledBillSubWindow();
        setFormProperties();
        setVisibleFields();
        setFormAction();
    }

    private void setFormProperties() {
        subWindow.getForm().setItemDataSource(new BeanItem<ParceledBillFormBean>(formBean));
        subWindow.getForm().setFormFieldFactory(new ParceledBillFormFieldFactory());
        FieldPropertiesUtil.applyDoubleValueConverter(subWindow.getForm().getField(Bill.PROPERTY_VALUEOFBILL));
    }

    private void setVisibleFields() {
        subWindow.getForm().setVisibleItemProperties(
                Arrays.asList(new String[] {ParceledBillFormBean.PROPERTY_NAME, ParceledBillFormBean.PROPERTY_VALUEOFBILL,
                        ParceledBillFormBean.PROPERTY_DATEOFBILL, ParceledBillFormBean.PROPERTY_PAID, ParceledBillFormBean.PROPERTY_OBSERVATION,
                        ParceledBillFormBean.PROPERTY_TAGS_AS_STRING, ParceledBillFormBean.PROPERTY_TYPE,
                        ParceledBillFormBean.PROPERTY_NUMBER_OF_PARCELED, ParceledBillFormBean.PROPERTY_IN_VALUE

                }));
    }

    private void setFormAction() {
        AddOrUpdateParceledBillAction addUpdateAction = new AddOrUpdateParceledBillAction(formBean, subWindow, subWindow.getForm());
        FieldPropertiesUtil.createFormApplyButton(subWindow.getForm(), addUpdateAction);
    }
}
