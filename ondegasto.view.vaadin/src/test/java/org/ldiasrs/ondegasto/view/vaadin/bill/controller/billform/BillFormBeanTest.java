package org.ldiasrs.ondegasto.view.vaadin.bill.controller.billform;

import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Test;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.user.User;
/**
 * Test for the utilitys.
 * @author cardoso
 *
 */
public class BillFormBeanTest {

    @Test
    public final void testConvertToDomainObject() {
        //// TODO [ALTA][cardoso][type] Utilizar o BillDataProvider do projeto de serviço
        //given
        User user = new User(BillFormBeanTest.class.getName());
        Bill cost = new Bill(user, "testConvertToDomainObject", 30, BillType.COST); //$NON-NLS-1$
        Bill invest = new Bill(user, "testConvertToDomainObject-Invest", 30, BillType.INVESTMENT); //$NON-NLS-1$

        Collection<Bill> domainBills = new ArrayList<Bill>();
        domainBills.add(cost);
        domainBills.add(invest);

        Collection<BillFormBean> formBills = new ArrayList<BillFormBean>();
        formBills.add(new BillFormBean(cost));
        formBills.add(new BillFormBean(invest));
        //when
        Collection<Bill> convertedDomainBills = BillFormBean.convertToDomainObject(formBills);
        //then
        @SuppressWarnings("unchecked")
        Collection<Bill> emptylist = CollectionUtils.subtract(domainBills, convertedDomainBills);
        Assert.assertTrue(emptylist.isEmpty());
    }

}
