package org.ldiasrs.ondegasto.view.vaadin.bill.controller.mainframe;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

/**
 * Test for {@link DateUtil}.
 */
public class DateUtilTest {

    @Test
    public void testFormatLabelDate() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.MONTH, Calendar.MARCH);
        calendar.set(Calendar.YEAR, 1985);
        String printDate = DateUtil.formatLabelDate(calendar.getTime());
        System.out.println(printDate);
    }

}
