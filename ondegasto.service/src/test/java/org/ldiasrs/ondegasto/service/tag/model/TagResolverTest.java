package org.ldiasrs.ondegasto.service.tag.model;

import java.util.Collection;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ld24078
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class TagResolverTest {

    @Autowired
    private TagResolver tagResolver;

    @Autowired
    private TagList tagList;

    @Autowired
    private UserDataProvider userDataProvider;

    @Test
    public void shouldResolveTagsByName() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.cacheAndPersistNewUser();
        String billName = "condominio";
        String[] tagsBindName = {"casa", "gastos fixos", "vencimento dia 10" };
        TagBinder tagBinder = new TagBinder(user, billName);
        tagBinder.bindWithTags(tagList.getTags(user, tagsBindName));
        tagResolver.addOrUpdateTagBinder(tagBinder);
        Collection<Tag> tags = tagResolver.resolveTagsByName(user, billName);
        Assert.assertNotNull(tags);
        Assert.assertEquals(tagsBindName.length, tags.size());
    }

    @Test
    public void shouldNotaddDuplicatedTagBinders() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.cacheAndPersistNewUser();
        String billName = "condominio";
        String[] tagsBindName = {"casa", "gastos fixos", "vencimento dia 10" };
        TagBinder tagBinder = new TagBinder(user, billName);
        tagBinder.bindWithTags(tagList.getTags(user, tagsBindName));
        tagResolver.addOrUpdateTagBinder(new TagBinder(user, billName));
        tagResolver.addOrUpdateTagBinder(tagBinder);
        Collection<Tag> tags = tagResolver.resolveTagsByName(user, billName);
        Assert.assertNotNull(tags);
        Assert.assertEquals(tagsBindName.length, tags.size());
    }
}
