package org.ldiasrs.ondegasto.service.note;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.note.api.NoteList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testa a manipulacao de multiplas notas.
 * @author ld24078
 */
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class MultipleNotesTest {

    @Autowired
    private UserDataProvider userDataProvider;

    @Autowired
    private NoteList noteList;

    @Test
    public void shouldWorkWithMultipleNotes() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.cacheAndPersistNewUser();
        int countBeforeTest = noteList.getAll(user).size();
        String htmlmsg = "<h1> this is a HTML message<//h1> this msg must persist on DB";
        Note note = new Note(user, "my note", htmlmsg);
        Note copyNote = new Note(user, "my copy of note", htmlmsg);
        noteList.add(note);
        noteList.add(copyNote);
        Note loadedNote = noteList.getNoteByName("my note", user);
        assertNoteProperties("my note", htmlmsg, loadedNote);
        Note loadedCopyNote = noteList.getNoteByName("my copy of note", user);
        assertNoteProperties("my copy of note", htmlmsg, loadedCopyNote);
        Assert.assertEquals(countBeforeTest + 2, noteList.getAll(user).size());
    }

    private void assertNoteProperties(final String name, final String htmlmsg, final Note loadedNote) {
        Assert.assertNotNull(loadedNote);
        Assert.assertEquals(htmlmsg, loadedNote.getHtml());
        Assert.assertEquals(name, loadedNote.getName());
    }
}
