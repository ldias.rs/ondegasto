package org.ldiasrs.ondegasto.service.bill;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.bill.api.ParceledBillRepository;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.BillDataProvider;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for custom methods of  {@link ParceledBillRepositoryImp}.
 * @author cardoso
 *
 */
@ContextConfiguration (locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class ParceledBillRepositoryImpTest {

    @Autowired
    private ParceledBillRepository parceledBillRepository;

    @Autowired
    private UserDataProvider userDataProvider;

    @Autowired
    private BillDataProvider billDataProvider;

    @Test
    public final void testFindParceledBillOfBill() throws OperationServiceException, UserAlreadyRegisterException {
        //given
        userDataProvider.cacheAndPersistNewUser();
        BigDecimal totalValue = BigDecimal.valueOf(1500);
        BigDecimal inValue = BigDecimal.valueOf(500);
        int numberOfParcels = 2;
        ParceledBill parceledBill = billDataProvider.createCarParceledBill(totalValue, inValue, numberOfParcels);
        Long orginalId = parceledBillRepository.create(parceledBill);
        //when
        Bill someBill = parceledBill.getParceledBills().iterator().next();
        ParceledBill parceledbillFound = parceledBillRepository.findParceledBillOfBill(someBill);
        //then
        Assert.assertNotNull(parceledbillFound);
        Assert.assertEquals(orginalId.longValue(), parceledbillFound.getId());
    }

}
