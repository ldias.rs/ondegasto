package org.ldiasrs.ondegasto.service.dataprovider;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Prove dados para usuário.
 * @author cardoso
 */
@Service
public class UserDataProvider extends AbstractDataProvider {

    @Autowired
    private UserService userService;

    private User userCached;

    private static final String USER_NAME = "admin-junit"; //$NON-NLS-1$

    private User cacheUser(final User user) {
        userCached = user;
        return userCached;
    }

    public User cacheAndPersistNewUser() throws OperationServiceException, UserAlreadyRegisterException {
        return cacheUser(persistNewUser());
    }

    public User getUserCached() {
        if (userCached == null) {
            try {
                cacheAndPersistNewUser();
            } catch (Exception e) {
                throw new RuntimeException("Problem caching a new user", e);
            }
        }
        return userCached;
    }

    public User persistNewUser() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userService.addUser(generateNewUser());
        return user;
    }

    public User generateNewUser() {
        return new User(nextString(USER_NAME), USER_NAME);
    }

}
