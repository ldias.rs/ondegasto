package org.ldiasrs.ondegasto.service.tag.model;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testes para as tags.
 * @author cardoso
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class TagListImpTest {

    @Autowired
    private UserDataProvider userDataProvider;

    @Autowired
    private TagList tagList;

    @Before
    public final void beforeRunninTest() throws OperationServiceException, UserAlreadyRegisterException {
        userDataProvider.cacheAndPersistNewUser();
    }

    public User getUser() {
        return userDataProvider.getUserCached();
    }

    @Test
    public final void shouldCreateTag() {
        Tag t = tagList.getTag(getUser(), "test"); //$NON-NLS-1$
        Assert.assertNotNull(t);
        Assert.assertEquals("test", t.getName()); //$NON-NLS-1$
    }

    @Test
    public final void shouldCreateDiferentTagsForSameUser() {
        Tag t1 = tagList.getTag(getUser(), "test"); //$NON-NLS-1$
        Tag t2 = tagList.getTag(getUser(), "test2"); //$NON-NLS-1$
        Assert.assertNotNull(t1);
        Assert.assertNotNull(t2);
        Assert.assertEquals("test", t1.getName()); //$NON-NLS-1$
        Assert.assertEquals("test2", t2.getName()); //$NON-NLS-1$
        Assert.assertFalse(t1.equals(t2));
    }

    @Test
    public final void shouldCreateSameTagsForOtherUsers() throws OperationServiceException, UserAlreadyRegisterException {
        Tag t1 = tagList.getTag(getUser(), "lunch"); //$NON-NLS-1$
        Assert.assertNotNull(t1);

        User anotherUser = userDataProvider.persistNewUser();
        Tag t2 = tagList.getTag(anotherUser, "lunch"); //$NON-NLS-1$
        Assert.assertNotNull(t2);

        Assert.assertFalse(t1.equals(t2));
    }
}
