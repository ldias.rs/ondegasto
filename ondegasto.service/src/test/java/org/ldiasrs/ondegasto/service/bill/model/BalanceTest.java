package org.ldiasrs.ondegasto.service.bill.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.common.NumberNormalizer;
import org.ldiasrs.ondegasto.domain.bill.Balance;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class will teste the Balancing of the Bills.
 * @author cardoso
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class BalanceTest {

    @Autowired
    private UserDataProvider userDataProvider;


    @Before
    public final void beforeRunninTest() throws OperationServiceException, UserAlreadyRegisterException {
        userDataProvider.cacheAndPersistNewUser();
    }


    @Test
    public final void shouldMakeBalancingOfBills() {
        //TODO [cardoso][MEDIA][ondegasto] Implementar mais casos de teste
        Collection<Bill> bills = new ArrayList<Bill>();
        //given
        Bill rent = new Bill(userDataProvider.getUserCached(), "salario", BigDecimal.valueOf(1000), BillType.RENT); //$NON-NLS-1$
        rent.pay();
        bills.add(rent);
        
        Bill investment = new Bill(userDataProvider.getUserCached(), "poupança", BigDecimal.valueOf(150), BillType.INVESTMENT); //$NON-NLS-1$
        investment.unPay();
        bills.add(investment);
        
        Bill dinner = new Bill(userDataProvider.getUserCached(), "Dinner", BigDecimal.valueOf(10), BillType.COST); //$NON-NLS-1$
        dinner.pay();
        bills.add(dinner);
        
        Bill lunch = new Bill(userDataProvider.getUserCached(), "Lunch", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        lunch.unPay();
        bills.add(lunch);

        //when
        Balance balance = new Balance(bills);

        //then
        sumOfCostMustBeCorrect(balance);
        sumOfRentMustBeCorrect(balance);
        sumOfInvestmentMustBeCorrect(balance);
        finalBalenceMustBeCorrect(balance);
    }


    private void finalBalenceMustBeCorrect(Balance balance) {
        //Valor parcial é a soma da receita menos as somas das desepesas e investimentos pagos
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(990)), balance.calculatePartialBalance());
        //Valor final é a soma da receita menos as somas das desepesas e investimentos pagos e não pagos
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(825)), balance.calculateFinalBalance());
    }

    private void sumOfInvestmentMustBeCorrect(Balance balance) {
        Assert.assertEquals(NumberNormalizer.zero(), balance.getSumPayed(BillType.INVESTMENT));
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(150)), balance.getSumNotPayed(BillType.INVESTMENT));
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(150)), balance.sumAll(BillType.INVESTMENT));
    }

    private void sumOfRentMustBeCorrect(Balance balance) {
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(1000)), balance.getSumPayed(BillType.RENT));
        Assert.assertEquals(NumberNormalizer.zero(), balance.getSumNotPayed(BillType.RENT));
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(1000)), balance.sumAll(BillType.RENT));
    }

    private void sumOfCostMustBeCorrect(Balance balance) {
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(10)), balance.getSumPayed(BillType.COST));
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(15)), balance.getSumNotPayed(BillType.COST));
        Assert.assertEquals(NumberNormalizer.normalize(BigDecimal.valueOf(25)), balance.sumAll(BillType.COST));
    }
}