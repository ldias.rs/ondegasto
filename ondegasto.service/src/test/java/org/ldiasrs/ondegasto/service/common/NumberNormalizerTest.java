package org.ldiasrs.ondegasto.service.common;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;
import org.ldiasrs.ondegasto.common.NumberNormalizer;

/**
 * Teste for normalization of numbers.
 * @author cardoso
 *
 */
public class NumberNormalizerTest {

    @Test
    public final void testNormalize() {
        BigDecimal value =  BigDecimal.TEN.setScale(2);
        Assert.assertEquals(value, NumberNormalizer.normalize(BigDecimal.TEN));
    }

    @Test
    public final void testZero() {
        BigDecimal value =  BigDecimal.ZERO.setScale(2);
        Assert.assertEquals(value, NumberNormalizer.zero());
    }

}
