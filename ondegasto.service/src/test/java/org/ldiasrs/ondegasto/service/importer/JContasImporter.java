package org.ldiasrs.ondegasto.service.importer;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JContasImporter {

    private void importBills() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, ParseException,
            OperationServiceException, UserAlreadyRegisterException {
        Collection<Bill> bills = new ArrayList<Bill>();
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        BillList list = context.getBean(BillList.class);
        UserService userService = context.getBean(UserService.class);
        User user = new User("ldias", "");
        userService.addUser(user);
        Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
        Connection connection = DriverManager
                .getConnection("jdbc:derby:C:\\Users\\ld24078\\Documents\\dados\\files\\pessoal\\JContas\\JContas-2010-06-02\\data\\jcontasdb;create=true");
        System.out.println(connection);
        PreparedStatement ps = connection.prepareStatement("SELECT * FROM CONTAS");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Bill bill = new Bill();
            bill.setName(rs.getString("nome"));
            bill.setValueOfBill(BigDecimal.valueOf(rs.getDouble("valor")));
            bill.setDateOfBill(new SimpleDateFormat("yyyy/MM/dd").parse(rs.getString("dataentrada")));
            bill.setPaid(rs.getString("ispaga").equals("1"));
            bill.setObservation(rs.getString("observacao"));
            bill.setUser(user);
            int cod = rs.getInt("categoria");
            if (cod == 0) {
                bill.setType(BillType.COST);
            } else if (cod == 1) {
                bill.setType(BillType.RENT);
            } else if (cod == 2) {
                bill.setType(BillType.INVESTMENT);
            }
            System.out.println(bill);
            list.add(bill);
        }
    }

    public static void main(final String[] args) throws Exception, ParseException {
        new JContasImporter().importBills();
    }
}
