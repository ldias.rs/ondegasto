package org.ldiasrs.ondegasto.service.backup;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test for backup service.
 * @author ld24078
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
@Ignore
public class DerbyBackupDataBaseServiceTest {

    @Autowired
    private BackupDataService backupDataService;

    @Test
    public void testBackupData() throws OperationServiceException {
        backupDataService.backupData();
    }

}
