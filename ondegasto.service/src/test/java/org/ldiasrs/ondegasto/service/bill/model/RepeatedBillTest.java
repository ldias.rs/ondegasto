package org.ldiasrs.ondegasto.service.bill.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.bill.api.RepeatedBillRepository;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.BillDataProvider;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Tests for the {@link RepeatedBill}.
 * @author ld24078
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class RepeatedBillTest {

    @Autowired
    private BillDataProvider billDataProvider;

    @Autowired
    private BillList billList;

    @Autowired
    private RepeatedBillRepository repeatedBillRepository;

    @Autowired
    private UserDataProvider userDataProvider;

    @Autowired
    private TagList tagList;

    @Test
    public void shouldEditNewerBills() throws OperationServiceException, UserAlreadyRegisterException {
        // given a repeted bill
        userDataProvider.cacheAndPersistNewUser();
        int numberOfRepetition = 9;
        BigDecimal valueOf = BigDecimal.valueOf(100);
        Bill carBill = billDataProvider.createCarBill(valueOf);
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldRemoveNewerBills"); //$NON-NLS-1$
        carBill.addTag(tag);
        RepeatedBill repeatedBill = new RepeatedBill(carBill, numberOfRepetition);
        billList.add(repeatedBill);
        // when edit newers bills
        ArrayList<Bill> listOfBills = new ArrayList<Bill>(repeatedBill.getRepeatedBills());
        Bill editedBill = listOfBills.get(2);
        editedBill.setName("new name");
        editedBill.setObservation("new observation");
        editedBill.setValueOfBill(BigDecimal.valueOf(50));
        repeatedBill.changeValueAndNameFromNewersBills(editedBill);
        // then the newers bills should change
        for (Bill bill : repeatedBill.getNewersBillsComparingYearAndMonth(editedBill.getDateOfBill())) {
            Assert.assertEquals(editedBill.getName(), bill.getName());
            Assert.assertEquals(editedBill.getObservation(), bill.getObservation());
            Assert.assertEquals(editedBill.getValueOfBill(), bill.getValueOfBill());
        }
        // when update
        billList.update(repeatedBill);
        RepeatedBill dbBean = repeatedBillRepository.get(repeatedBill.getId());
        for (Bill bill : dbBean.getNewersBillsComparingYearAndMonth(editedBill.getDateOfBill())) {
            Assert.assertEquals(editedBill.getName(), bill.getName());
            Assert.assertEquals(editedBill.getObservation(), bill.getObservation());
            Assert.assertEquals(editedBill.getValueOfBill(), bill.getValueOfBill());
        }
    }

    @Test
    public void shouldRemoveNewerBills() throws OperationServiceException, UserAlreadyRegisterException {
        // given a repeted bill
        userDataProvider.cacheAndPersistNewUser();
        int numberOfRepetition = 9;
        BigDecimal valueOf = BigDecimal.valueOf(100);
        Bill carBill = billDataProvider.createCarBill(valueOf);
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldRemoveNewerBills"); //$NON-NLS-1$
        carBill.addTag(tag);
        RepeatedBill repeatedBill = new RepeatedBill(carBill, numberOfRepetition);
        billList.add(repeatedBill);
        // when remove newers bills
        Calendar nextMonth = getNextMonth();
        repeatedBill.removeNewerThan(nextMonth);
        Assert.assertEquals(1, repeatedBill.getRepeatedBills().size());
        billList.update(repeatedBill);
        // then should have Only Older Bills
        List<Bill> bills = billList.findBillsByTags(tag);
        Assert.assertNotNull(bills);
        Assert.assertEquals(1, bills.size());
    }

    private Calendar getNextMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        return calendar;
    }

    @Test
    public void shouldAddRepeatedBill() throws OperationServiceException, UserAlreadyRegisterException {
        // given
        userDataProvider.cacheAndPersistNewUser();
        int numberOfRepetition = 9;
        BigDecimal valueOf = BigDecimal.valueOf(100);
        Bill carBill = billDataProvider.createCarBill(valueOf);
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldAddRepeatedBill"); //$NON-NLS-1$
        carBill.addTag(tag);
        RepeatedBill repeatedBill = new RepeatedBill(carBill, numberOfRepetition);
        // when
        billList.add(repeatedBill);
        // then
        List<Bill> bills = billList.findBillsByTags(tag);
        Assert.assertEquals("The number of bills must be equals numberOfRepetition", numberOfRepetition, bills.size()); //$NON-NLS-1$
        for (Bill bill : bills) {
            Assert.assertEquals(carBill.getValueOfBill(), bill.getValueOfBill());
            Assert.assertEquals(carBill.getName(), bill.getName());
            Assert.assertEquals(carBill.getObservation(), bill.getObservation());
            Assert.assertTrue(bill.getTagList().contains(tag));
        }
    }

    @Test
    public void shouldRemoveJustOneBill() throws OperationServiceException, UserAlreadyRegisterException {
        // given
        userDataProvider.cacheAndPersistNewUser();
        int numberOfRepetition = 9;
        BigDecimal valueOf = BigDecimal.valueOf(100);
        Bill carBill = billDataProvider.createCarBill(valueOf);
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldRemoveJustOneBill"); //$NON-NLS-1$
        carBill.addTag(tag);
        RepeatedBill repeatedBill = new RepeatedBill(carBill, numberOfRepetition);
        billList.add(repeatedBill);
        // when
        int numberofBillAfterRemove = repeatedBill.getRepeatedBills().size();
        Iterator<Bill> iterator = repeatedBill.getRepeatedBills().iterator();
        Bill removedBill = iterator.next();
        Assert.assertTrue(repeatedBill.removeBill(removedBill));
        Assert.assertEquals(numberofBillAfterRemove - 1, repeatedBill.getRepeatedBills().size());
        Assert.assertFalse(repeatedBill.getRepeatedBills().contains(removedBill));
        billList.update(repeatedBill);
        List<Bill> bills = billList.findBillsByTags(tag);
        Assert.assertEquals(numberofBillAfterRemove - 1, bills.size());
    }

}
