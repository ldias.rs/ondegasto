package org.ldiasrs.ondegasto.service.dataprovider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * Prove metodos comuns para todos os provedores de dados para teste.
 * @author cardoso
 *
 */
public abstract class AbstractDataProvider {

    private int seed = 10000;

    @Autowired
    private ApplicationContext applicationContext;

    public String nextString(String prefix) {
        return prefix + nextInt();
    }

    public int nextInt() {
        return ++seed;
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
}
