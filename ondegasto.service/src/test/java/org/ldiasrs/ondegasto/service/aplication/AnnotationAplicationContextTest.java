package org.ldiasrs.ondegasto.service.aplication;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

/**
 * Testa o load da configuração.
 * @author cardoso
 *
 */
@ContextConfiguration (locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@RunWith(SpringJUnit4ClassRunner.class)
public class AnnotationAplicationContextTest {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private UserService userService;

    @Test
    public void shouldLoadConfigurationByAnnotation() {
        Assert.notNull(applicationContext);
        Assert.notNull(userService);
    }
}
