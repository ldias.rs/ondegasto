package org.ldiasrs.ondegasto.service.aplication;


/**
 * Especifica o path para testes que iram utilizar todo o contexto.
 * @author cardoso
 *
 */
public final class ContextTestPath {

    public static final String LOAD_ALL_SERVICES_CONTEXT_PATH = "/org/ldiasrs/ondegasto/service/aplication/loadallservices-context.xml";  //$NON-NLS-1$

}
