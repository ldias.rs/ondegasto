package org.ldiasrs.ondegasto.service.dataprovider;

import java.math.BigDecimal;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Teste.
 * @author cardoso
 */
@Service
public class BillDataProvider {

    @Autowired
    private BillList billList;

    @Autowired
    private TagList billTagList;

    @Autowired
    private UserDataProvider userDataProvider;

    public void addBillsMock() {
        billList.add(new Bill(userDataProvider.getUserCached(), "almoço", BigDecimal.valueOf(15), BillType.COST)); //$NON-NLS-1$
        billList.add(new Bill(userDataProvider.getUserCached(), "salario", BigDecimal.valueOf(100), BillType.RENT)); //$NON-NLS-1$
    }

    public Bill addRentBillWithTag() {
        Bill rent = new Bill(userDataProvider.getUserCached(), "salario-A", BigDecimal.valueOf(100), BillType.RENT); //$NON-NLS-1$
        rent.addTag(billTagList.getTag(userDataProvider.getUserCached(), "rent")); //$NON-NLS-1$
        billList.add(rent);
        return rent;
    }

    public Bill addLunchBillWithTag() {
        Bill lunch = new Bill(userDataProvider.getUserCached(), "almoço-A", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        lunch.addTag(billTagList.getTag(userDataProvider.getUserCached(), "lunch")); //$NON-NLS-1$
        billList.add(lunch);
        return lunch;
    }

    public void addDiferentBillsWithSameTag() {
        Tag tag = billTagList.getTag(userDataProvider.getUserCached(), "lunch"); //$NON-NLS-1$

        Bill lunch = new Bill(userDataProvider.getUserCached(), "almoço-A", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        lunch.addTag(tag);
        billList.add(lunch);

        Bill rent = new Bill(userDataProvider.getUserCached(), "salario-A", BigDecimal.valueOf(100), BillType.RENT); //$NON-NLS-1$
        rent.addTag(tag);
        billList.add(rent);
    }

    public Bill createDinnerBill() {
        return new Bill(userDataProvider.getUserCached(), "Dinner", BigDecimal.valueOf(10), BillType.COST); //$NON-NLS-1$
    }

    public Bill createInvestmentBillWithoutTag() {
        return new Bill(userDataProvider.getUserCached(), "poupança", BigDecimal.valueOf(150), BillType.INVESTMENT); //$NON-NLS-1$
    }

    public Bill createCarBill(final BigDecimal totalvalue) {
        return new Bill(userDataProvider.getUserCached(), "Carro parecelado", totalvalue, BillType.COST); //$NON-NLS-1$
    }

    public ParceledBill createCarParceledBill(final BigDecimal totalValue, final BigDecimal inValue, final int numberOfParcels, final Tag tag) {
        Bill bill = createCarBill(totalValue);
        if (tag != null) {
            bill.addTag(tag);
        }
        return new ParceledBill(bill, inValue, numberOfParcels);
    }

    public ParceledBill createCarParceledBill(final BigDecimal totalValue, final BigDecimal inValue, final int numberOfParcels) {
        return createCarParceledBill(totalValue, inValue, numberOfParcels, null);
    }

    public Tag addTagOnBill(final String tagStr, final Bill bill) {
        Tag tag = billTagList.getTag(bill.getUser(), tagStr);
        bill.addTag(tag);
        return tag;
    }

}
