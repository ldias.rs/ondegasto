package org.ldiasrs.ondegasto.service.bill.model;

import java.math.BigDecimal;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.BillDataProvider;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Teste.
 * @author cardoso
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class BillListTest {

    @Autowired
    private BillDataProvider billDataProvider;

    @Autowired
    private BillList billList;

    @Autowired
    private TagList billTagList;

    @Autowired
    private UserDataProvider userDataProvider;

    @Before
    public final void beforeRunninTest() throws OperationServiceException, UserAlreadyRegisterException {
        userDataProvider.cacheAndPersistNewUser();
    }

    @Test
    public final void shouldFoundBillsOfCurrentMonth() {
        billDataProvider.addBillsMock();
        List<Bill> bills = billList.findCurrentMonthBills(userDataProvider.getUserCached());
        Assert.assertEquals(2, bills.size());
    }

    @Test
    public final void shouldCreateBillWithSameTag() {
        billDataProvider.addDiferentBillsWithSameTag();
    }

    @Test
    public final void shouldCreateBillsForDiferentUsers() throws OperationServiceException, UserAlreadyRegisterException {
        User userCached = userDataProvider.getUserCached();
        User anotherUser = userDataProvider.persistNewUser();

        billDataProvider.addLunchBillWithTag();
        billDataProvider.addRentBillWithTag();

        Bill anotherLunch = new Bill(anotherUser, "almoço-B", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        anotherLunch.addTag(billTagList.getTag(anotherUser, "lunch")); //$NON-NLS-1$
        billList.add(anotherLunch);

        List<Bill> billsOfUserCached = billList.findBillsByTags(billTagList.getTag(userCached, "rent"), //$NON-NLS-1$
                billTagList.getTag(userCached, "lunch")); //$NON-NLS-1$

        List<Bill> billsOfAnotherUser = billList.findBillsByTags(billTagList.getTag(anotherUser, "rent"), //$NON-NLS-1$
                billTagList.getTag(anotherUser, "lunch")); //$NON-NLS-1$

        Assert.assertEquals(2, billsOfUserCached.size());
        Assert.assertEquals(billsOfUserCached.get(0).getUser().getLoginName(), userCached.getLoginName());
        Assert.assertEquals(billsOfUserCached.get(1).getUser().getLoginName(), userCached.getLoginName());

        Assert.assertEquals(1, billsOfAnotherUser.size());
        Assert.assertEquals(billsOfAnotherUser.get(0).getUser().getLoginName(), anotherUser.getLoginName());
    }

    @Test
    public final void shouldRemoveBillsWithTagsAssociated() {
        Bill lunch = billDataProvider.addLunchBillWithTag();
        Tag lunchTag = lunch.getTagList().iterator().next();
        billDataProvider.addRentBillWithTag();
        billList.remove(lunch);
        Assert.assertTrue(billList.findBillsByTags(lunchTag).isEmpty());
    }

    @Test
    public final void shouldUpdateBillAndSearchByTag() {
        Bill lunch = billDataProvider.addLunchBillWithTag();
        Tag lunchTag = lunch.getTagList().iterator().next();
        lunch.setName("almoço-B"); //$NON-NLS-1$
        billList.update(lunch);
        List<Bill> billsByTags = billList.findBillsByTags(lunchTag);
        Assert.assertFalse(billsByTags.isEmpty());
        Assert.assertEquals("almoço-B", billsByTags.get(0).getName()); //$NON-NLS-1$
    }

}
