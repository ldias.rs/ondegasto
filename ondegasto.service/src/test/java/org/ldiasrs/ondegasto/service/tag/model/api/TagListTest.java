package org.ldiasrs.ondegasto.service.tag.model.api;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * {@link TagList} Test's.
 * @author cardoso
 */
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@RunWith(SpringJUnit4ClassRunner.class)
public class TagListTest {

    @Autowired
    private TagList tagList;

    @Autowired
    private UserDataProvider userDataProvider;

    @Test
    public final void shouldCreateDiferentTags() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.cacheAndPersistNewUser();
        String homeTagName = "home"; //$NON-NLS-1$
        String workTagName = "work"; //$NON-NLS-1$
        Tag homeTag = tagList.getTag(user, homeTagName);
        Tag workTag = tagList.getTag(user, workTagName);
        Assert.assertNotNull(homeTag);
        Assert.assertNotNull(workTag);
        Assert.assertTrue("the name of the tah must me same on creating", homeTagName.equals(homeTag.getName())); //$NON-NLS-1$
        Assert.assertTrue("the name of the tah must me same on creating", workTagName.equals(workTag.getName())); //$NON-NLS-1$
    }

    @Test
    public final void shouldNotPersistSameTag() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.cacheAndPersistNewUser();
        String homeTagName = "home"; //$NON-NLS-1$
        Tag homeTag = tagList.getTag(user, homeTagName);
        Tag sameHomeTag = tagList.getTag(user, homeTagName);
        Assert.assertNotNull(homeTag);
        Assert.assertNotNull(sameHomeTag);
        Assert.assertTrue("the name of the tah must me same on creating", homeTagName.equals(homeTag.getName())); //$NON-NLS-1$
        Assert.assertTrue("the name of the same tag should be equal to original", homeTag.getName().equals(sameHomeTag.getName())); //$NON-NLS-1$
    }

}
