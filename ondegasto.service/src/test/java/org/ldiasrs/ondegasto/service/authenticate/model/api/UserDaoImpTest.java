package org.ldiasrs.ondegasto.service.authenticate.model.api;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.ExpectedException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;


/**
 * Teste para o DAO de usuario.
 * @author cardoso
 */
@ContextConfiguration (locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class UserDaoImpTest {

    private static final String NOT_REGISTER_USER_NAME = "admin-NOT_REGISTER_USER_NAME"; //$NON-NLS-1$

    @Autowired
    private UserService userService;

    @Autowired
    private UserDataProvider userDataProvider;


    @Test
    public void shouldCreateNewUser() throws OperationServiceException, UserAlreadyRegisterException {
        Assert.assertNotNull(userDataProvider.persistNewUser());
    }

    @Test
    public final void shoudlBeRegistered() throws OperationServiceException, UserAlreadyRegisterException  {
        User user = userDataProvider.persistNewUser();
        Assert.assertTrue(userService.isUserRegistered(user.getLoginName()));
    }

    @Test
    public final void shoudlBeNotRegistered()  {
        Assert.assertFalse(userService.isUserRegistered(NOT_REGISTER_USER_NAME));
    }

    @Test
    public void shouldAuthenticateUser() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.persistNewUser();
        Assert.assertTrue(userService.authenticate(user));
    }

    @Test
    public void shouldNotAuthenticate() throws OperationServiceException {
        User user = userDataProvider.generateNewUser();
        Assert.assertFalse(userService.authenticate(user));
    }

    @Test
    public void shouldCreateDiferentUsers() throws UserAlreadyRegisterException, OperationServiceException {
        userDataProvider.persistNewUser();
        userDataProvider.persistNewUser();
    }

    @Test
    @ExpectedException(UserAlreadyRegisterException.class)
    public void shouldNotCreateSameUserName() throws OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.generateNewUser();
        User another = userDataProvider.generateNewUser();
        another.setLoginName(user.getLoginName());
        userService.addUser(user);
        userService.addUser(another);
    }
}
