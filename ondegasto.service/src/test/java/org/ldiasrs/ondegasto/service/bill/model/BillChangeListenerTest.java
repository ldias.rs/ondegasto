package org.ldiasrs.ondegasto.service.bill.model;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.common.eventmanager.ModelCrudChangeListener;
import org.ldiasrs.ondegasto.service.common.eventmanager.ServiceEventManager;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test the observer of {@link Bill}.
 * @author cardoso
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class BillChangeListenerTest {

    @Autowired
    private BillList billList;

    @Autowired
    private UserDataProvider userDataProvider;

    @Autowired
    private ServiceEventManager eventManager;


    @Before
    public final void beforeRunninTest() throws OperationServiceException, UserAlreadyRegisterException {
        userDataProvider.cacheAndPersistNewUser();
    }

    @Test
    public void shouldNotifyInsertion() {
        final Bill lunch = new Bill(userDataProvider.getUserCached(), "almoço-A", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        ModelCrudChangeListener listener = new ModelCrudChangeListener<Bill>() {
            @Override
            public void changeEvent(final ChangeEvent<Bill> event) {
                Assert.assertNotNull(event.getEntity());
                Assert.assertEquals(lunch, event.getEntity());
                Assert.assertEquals(ChangeType.INSERT, event.getChangeType());
            }
        };
        eventManager.addListener(listener);
        billList.add(lunch);
        eventManager.removeListener(listener);
    }

    @Test
    public void shouldNotifyDelete() {
        final Bill lunch = new Bill(userDataProvider.getUserCached(), "almoço-A", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        billList.add(lunch);
        ModelCrudChangeListener listener = new ModelCrudChangeListener<Bill>() {
            @Override
            public void changeEvent(final ChangeEvent<Bill> event) {
                Assert.assertNotNull(event.getEntity());
                Assert.assertEquals(lunch, event.getEntity());
                Assert.assertEquals(ChangeType.DELETE, event.getChangeType());
            }
        };
        eventManager.addListener(listener);
        billList.remove(lunch);
        eventManager.removeListener(listener);
    }

    @Test
    public void shouldNotifyUpdate() {
        final String name = "name-updated"; //$NON-NLS-1$
        final Bill lunch = new Bill(userDataProvider.getUserCached(), "almoço-A", BigDecimal.valueOf(15), BillType.COST); //$NON-NLS-1$
        billList.add(lunch);
        ModelCrudChangeListener listener = new ModelCrudChangeListener<Bill>() {
            @Override
            public void changeEvent(final ChangeEvent<Bill> event) {
                Assert.assertNotNull(event.getEntity());
                Assert.assertEquals(lunch, event.getEntity());
                Assert.assertEquals(lunch.getName(), event.getEntity().getName());
                Assert.assertEquals(ChangeType.UPDATE, event.getChangeType());
            }
        };
        eventManager.addListener(listener);
        lunch.setName(name);
        billList.update(lunch);
        eventManager.removeListener(listener);
    }
}
