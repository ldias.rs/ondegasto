package org.ldiasrs.ondegasto.service.authenticate.model;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.AddAdminUserOnStartup;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testa a inserção do usuário padrão do sistema.
 * @author cardoso
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class AddAdminUserOnStartupTest {

    @Autowired
    private UserService userService;

    @Test
    public final void shouldHaveAdminUserOnStartup() throws OperationServiceException {
        Assert.assertTrue(userService.authenticate(AddAdminUserOnStartup.ADMIN_USER));
    }

}
