package org.ldiasrs.ondegasto.service.bill.model;

import java.math.BigDecimal;
import java.security.InvalidParameterException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.BillDataProvider;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Teste.
 * @author cardoso
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class PaceledBillsTest {

    @Autowired
    private BillDataProvider billDataProvider;

    @Autowired
    private BillList billList;

    @Autowired
    private UserDataProvider userDataProvider;

    @Autowired
    private TagList tagList;

    @Before
    public final void beforeRunninTest() throws OperationServiceException, UserAlreadyRegisterException {
        userDataProvider.cacheAndPersistNewUser();
    }

    @Test
    public void shouldAddParceledBills() {
        // given
        BigDecimal totalValue = BigDecimal.valueOf(1500);
        BigDecimal inValue = BigDecimal.valueOf(500);
        int numberOfParcels = 2;
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldAddParceledBills"); //$NON-NLS-1$
        ParceledBill parceledBill = billDataProvider.createCarParceledBill(totalValue, inValue, numberOfParcels, tag);
        // when
        billList.add(parceledBill);
        // then
        List<Bill> bills = billList.findBillsByTags(tag);
        Assert.assertEquals("The number of bills must be (inBill + number of parceled)", 1 + numberOfParcels, bills.size()); //$NON-NLS-1$
        BigDecimal parcelValue = ParceledBill.calculateParcelValue(totalValue, inValue, numberOfParcels);
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(bills.get(0).getDateOfBill());
        Bill.sortBillsByDate(bills);
        for (Bill bill : bills) {
            Assert.assertEquals(parcelValue, bill.getValueOfBill());
            Assert.assertEquals(calendar.getTime(), bill.getDateOfBill());
            calendar.add(Calendar.MONTH, 1);
        }
    }

    @Test
    public void shouldRemoveParceledBillAndSubBills() {
        // given
        BigDecimal totalValue = BigDecimal.valueOf(1500);
        BigDecimal inValue = BigDecimal.valueOf(500);
        int numberOfParcels = 2;
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldAddParceledBills"); //$NON-NLS-1$
        ParceledBill parceledBill = billDataProvider.createCarParceledBill(totalValue, inValue, numberOfParcels, tag);
        billList.add(parceledBill);
        // when
        billList.remove(parceledBill);
        // then
        List<Bill> bills = billList.findBillsByTags(tag);
        Assert.assertEquals("Should not exist bill's after removed the parceled bill", 0, bills.size()); //$NON-NLS-1$
    }

    @Test
    public void shouldRemoveSubBillOfParceledBill() {
        // given
        BigDecimal totalValue = BigDecimal.valueOf(1300);
        BigDecimal inValue = BigDecimal.valueOf(300);
        int numberOfParcels = 2;
        Tag tag = tagList.getTag(userDataProvider.getUserCached(), "shouldRemoveBillOfParceledBill"); //$NON-NLS-1$
        ParceledBill parceledBill = billDataProvider.createCarParceledBill(totalValue, inValue, numberOfParcels, tag);
        billList.add(parceledBill);
        parceledBill.removeBill(parceledBill.getParceledBills().iterator().next());
        // when
        billList.update(parceledBill);
        // then
        List<Bill> bills = billList.findBillsByTags(tag);
        Assert.assertEquals("Should not exist bill's after removed the parceled bill", 2, bills.size()); //$NON-NLS-1$
        Iterator<Bill> iterator = bills.iterator();
        BigDecimal parceledValue = ParceledBill.calculateParcelValue(totalValue, inValue, numberOfParcels);
        Assert.assertEquals(parceledValue, iterator.next().getValueOfBill());
        Assert.assertEquals(parceledValue, iterator.next().getValueOfBill());
    }

    @Test
    public final void shouldNotCreateParceledBillsWithZeroNumberOfParceled() {
        boolean foundException = false;
        try {
            // given
            BigDecimal totalValue = BigDecimal.valueOf(0);
            BigDecimal inValue = BigDecimal.valueOf(0);
            int numberOfParcels = 0;
            // when
            billDataProvider.createCarParceledBill(totalValue, inValue, numberOfParcels);
        } catch (InvalidParameterException e) {
            // then
            Assert.assertNotNull(e);
            foundException = true;
        }
        Assert.assertTrue(foundException);
    }
}
