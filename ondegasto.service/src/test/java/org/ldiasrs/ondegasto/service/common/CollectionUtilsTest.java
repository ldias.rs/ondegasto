package org.ldiasrs.ondegasto.service.common;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;
import org.ldiasrs.ondegasto.common.CollectionUtils;
import org.ldiasrs.ondegasto.common.LabelResolver;

/**
 * Testes para {@link CollectionUtils}.
 * @author jonas.jeske
 * @author caesar.hoppen
 */
public class CollectionUtilsTest {
    private static final String APPENDER = ", "; //$NON-NLS-1$
    private static final String RIGHT_DELIMITER = "]"; //$NON-NLS-1$
    private static final String LEFT_DELIMITER = "["; //$NON-NLS-1$
    private static final Object A = "A"; //$NON-NLS-1$
    private static final Object B = "B"; //$NON-NLS-1$
    private static final Object C = "C"; //$NON-NLS-1$

    private Collection<Object> toSubtractFrom;
    private Collection<Object> itemsToBeSubtracted;
    private Collection<Object> subtractedResult;

    @Test
    public void shouldBeEmptyWhenCollectionIsNull() {
        // given
        Collection<?> nullCollection = null;
        // when
        boolean isEmpty = CollectionUtils.isEmpty(nullCollection);
        // then
        assertTrue(isEmpty);
    }

    @Test
    public void shouldBeEmptyWithEmptyCollection() {
        // given
        Collection<?> emptyCollection = Collections.emptyList();
        // when
        boolean isEmpty = CollectionUtils.isEmpty(emptyCollection);
        // then
        assertTrue(isEmpty);
    }

    @Test
    public void shouldNotBeEmpty() {
        // given
        Collection<?> notEmptyCollection = mock(Collection.class);
        when(Boolean.valueOf(notEmptyCollection.isEmpty())).thenReturn(Boolean.FALSE);
        // when
        boolean isEmpty = CollectionUtils.isEmpty(notEmptyCollection);
        // then
        assertFalse(isEmpty);
    }

    @Test
    public void subtractWithOneElement() {
        givenCollections(toCollection(A, B), toCollection(B));

        whenSubtracted();

        thenResultShouldBeEqual(toCollection(A));
    }

    @Test
    public void subtractWithEntagledElements() {
        givenCollections(toCollection(C, A, B), toCollection(B, C));

        whenSubtracted();

        thenResultShouldBeEqual(toCollection(A));
    }

    @Test
    public void subtractWithNoItemsToSubtract() {
        givenCollections(toCollection(A, B, C), toCollection());

        whenSubtracted();

        thenResultShouldBeEqualTheFirstCollection();
    }

    private void thenResultShouldBeEqualTheFirstCollection() {
        thenResultShouldBeEqual(toSubtractFrom);
    }

    @Test
    public void subtractWithNoItemsToSubtractFrom() {
        givenCollections(toCollection(), toCollection(A, B, C));

        whenSubtracted();

        thenResultShouldBeEmpty();
    }

    @Test
    public void subtractEqualCollections() {
        Collection<Object> mainCollection = toCollection(A, B, C);
        givenCollections(mainCollection, mainCollection);

        whenSubtracted();

        thenResultShouldBeEmpty();
    }

    private void thenResultShouldBeEmpty() {
        thenResultShouldBeEqual(toCollection());
    }

    private void givenCollections(final Collection<Object> toSubtractFrom, final Collection<Object> itemsToBeSubtracted) {
        this.toSubtractFrom = toSubtractFrom;
        this.itemsToBeSubtracted = itemsToBeSubtracted;
    }

    private void whenSubtracted() {
        subtractedResult = CollectionUtils.subtract(toSubtractFrom, itemsToBeSubtracted);
    }

    private void thenResultShouldBeEqual(final Collection<Object> expectedResult) {
        assertArrayEquals(expectedResult.toArray(), subtractedResult.toArray());
    }

    private <B> Collection<B> toCollection(final B... items) {
        return Arrays.asList(items);
    }

    @Test
    public void shouldBeAbleToTransformToString() {
        // given
        Collection<Object> toBeLabeledAsString = toCollection(A, B, C);
        // when
        String result = CollectionUtils.toString(toBeLabeledAsString, new LabelResolver<Object>() {
            @Override
            public String getLabel(final Object objectThatWeWantLabel) {
                return objectThatWeWantLabel.toString();
            }
        });
        // then
        assertEquals(LEFT_DELIMITER + A + APPENDER + B + APPENDER + C + RIGHT_DELIMITER, result);
    }

    @Test
    public void shouldBeAbleToTransformWithEmptyCollection() {
        // given
        Collection<Object> emptyCollection = new ArrayList<Object>();
        // when
        String result = simpleToString(emptyCollection);
        // then
        assertEquals(LEFT_DELIMITER + RIGHT_DELIMITER, result);
    }

    private String simpleToString(final Collection<Object> emptyCollection) {
        return CollectionUtils.toString(emptyCollection, new LabelResolver<Object>() {

            @Override
            public String getLabel(final Object objectThatWeWantLabel) {
                return objectThatWeWantLabel.toString();
            }
        });
    }

    @Test
    public void shouldBeAbleToTransformWithLabelResolver() {
        // given
        Collection<Object> toBeLabeledAsString = toCollection(A, B, C);
        final String suffix = "-LABEL"; //$NON-NLS-1$
        // when
        String result = CollectionUtils.toString(toBeLabeledAsString, new LabelResolver<Object>() {
            @Override
            public String getLabel(final Object objectThatWeWantLabel) {
                return objectThatWeWantLabel + suffix;
            }

        });
        // then
        assertEquals(LEFT_DELIMITER + A + suffix + APPENDER + B + suffix + APPENDER + C + suffix + RIGHT_DELIMITER, result);
    }

    @Test
    public void shouldBeAbleToTransformSingleElementCollection() {
        // given
        Collection<Object> singleElementCollection = toCollection(A);
        // when
        String result = simpleToString(singleElementCollection);
        // then
        assertEquals(LEFT_DELIMITER + A + RIGHT_DELIMITER, result);
    }

    @Test
    public void shouldReturnEmptyList() {
        // given
        Collection<Object> col = CollectionUtils.emptyListOf(Object.class);
        // when
        boolean isEmpty = col.isEmpty();
        // then
        assertTrue(isEmpty);
    }

    @Test
    public void shouldReturnDifferentInstances() {
        // given
        Collection<Object> listOne = CollectionUtils.emptyListOf(Object.class);
        Collection<Object> listTwo = CollectionUtils.emptyListOf(Object.class);
        // when
        boolean isSameInstance = listOne == listTwo;
        // then
        assertFalse(isSameInstance);
    }

    @Test
    public void shouldBeAbleToManipulateEmptyListWithNewElements() {
        // given
        Collection<Object> list = CollectionUtils.emptyListOf(Object.class);
        // when
        list.add(new Object());
        // then
        assertTrue(list.size() == 1);
    }
}
