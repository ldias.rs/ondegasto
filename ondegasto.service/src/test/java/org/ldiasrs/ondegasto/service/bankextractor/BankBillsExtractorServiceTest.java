package org.ldiasrs.ondegasto.service.bankextractor;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.aplication.ContextTestPath;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.dataprovider.UserDataProvider;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagAssociationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ld24078
 */
@ContextConfiguration(locations = {ContextTestPath.LOAD_ALL_SERVICES_CONTEXT_PATH })
@RunWith(SpringJUnit4ClassRunner.class)
@TransactionConfiguration(transactionManager = "myTransactionManager")
@Transactional
public class BankBillsExtractorServiceTest {

    @Autowired
    private BankBillsExtractorService bankBillsExtractorService;

    @Autowired
    private TagAssociationService tagBinderBuilder;

    @Autowired
    private UserDataProvider userDataProvider;

    @Test
    public void testExtractBills() throws IOException, OperationServiceException, UserAlreadyRegisterException {
        User user = userDataProvider.cacheAndPersistNewUser();
        // GIVEN a bank extract
        String extractContent = givenABankExtractAsString();
        // GIVEN a tags associations
        tagBinderBuilder.associateBillNameOnTags(user, "netflix", new String[] {"casa" });
        tagBinderBuilder.associateBillNameOnTags(user, "POSTO", new String[] {"carro" });
        tagBinderBuilder.associateBillNameOnTags(user, "MAKENJI", new String[] {"compras" });
        tagBinderBuilder.associateBillNameOnTags(user, "BOURBON", new String[] {"alimentacao" });
        // when ask for extract bills
        Collection<Bill> listOfBills = bankBillsExtractorService.extractBills(user, extractContent, new Date(), new String[] {"tagInformedByUserA",
                "tagInformedByUserB" });
        // THEN the bills must be found with the tags associations
        Assert.assertNotNull(listOfBills);
        Assert.assertEquals(4, listOfBills.size());
        Set<String> allBillsWasProcessed = new HashSet<String>();
        for (Bill bill : listOfBills) {
            assertTagOnBill(bill, "credito");
            assertTagOnBill(bill, "tagInformedByUserA");
            assertTagOnBill(bill, "tagInformedByUserB");
            Assert.assertTrue(BillType.COST.equals(bill.getType()));
            assertMonthBill(bill);
            if (bill.getName().equals("NETFLIX")) {
                assertTagOnBill(bill, "casa");
                allBillsWasProcessed.add("-NETFLIX-WAS-PROCESSED-");
            }
            if (bill.getName().equals("POSTO NONEMACHER")) {
                assertTagOnBill(bill, "carro");
                allBillsWasProcessed.add("-POSTO-WAS-PROCESSED-");
            }
            if (bill.getName().equals("MAKENJI MKJ PO 33 01/02")) {
                assertTagOnBill(bill, "compras");
                allBillsWasProcessed.add("-MAKENJI-WAS-PROCESSED-");
            }
            if (bill.getName().equals("BOURBON LOJA 24")) {
                assertTagOnBill(bill, "alimentacao");
                allBillsWasProcessed.add("-BOURBON-WAS-PROCESSED-");
            }
        }
        Assert.assertTrue(allBillsWasProcessed.contains("-NETFLIX-WAS-PROCESSED-"));
        Assert.assertTrue(allBillsWasProcessed.contains("-POSTO-WAS-PROCESSED-"));
        Assert.assertTrue(allBillsWasProcessed.contains("-MAKENJI-WAS-PROCESSED-"));
        Assert.assertTrue(allBillsWasProcessed.contains("-BOURBON-WAS-PROCESSED-"));
    }

    private void assertMonthBill(final Bill bill) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(bill.getDateOfBill());
        // the month must be the same is on the extractor
        int mounth = calendar.get(Calendar.MONTH);
        Assert.assertEquals(new GregorianCalendar().get(Calendar.MONTH), mounth);
    }

    private void assertTagOnBill(final Bill bill, final String tagTobeFoundOnBill) {
        // the tag list must not be empty
        Assert.assertNotNull(bill.getTagList());
        Assert.assertFalse(bill.getTagList().isEmpty());
        // the tag associated must be there
        Assert.assertNotNull("Tag could not be found: " + tagTobeFoundOnBill, bill.getTagByName(tagTobeFoundOnBill));
    }

    private String givenABankExtractAsString() throws IOException {
        String filePath = ClassLoader.getSystemResource("bank-extract-input-test.txt").getFile();
        String extractContent = FileUtils.readFileToString(new File(filePath));
        return extractContent;
    }

}
