package org.ldiasrs.ondegasto.service.common.eventmanager;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppEvent;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;

/**
 * Define a ServiceEventManager.
 * @author cardoso
 *
 */
public interface ServiceEventManager {

    void fire(AppEvent event);

    void addListener(AppListener listener);

    void removeListener(AppListener listener);

}
