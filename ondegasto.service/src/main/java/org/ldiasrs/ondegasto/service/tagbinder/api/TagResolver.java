package org.ldiasrs.ondegasto.service.tagbinder.api;

import java.util.Collection;
import java.util.Set;

import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Resolve tags with name.
 * @author ld24078
 */
public interface TagResolver {

    Set<Tag> resolveTagsByName(User user, String billName);

    void addOrUpdateTagBinder(TagBinder tagBinder);

    Collection<TagBinder> findAllTagBindersByUser(User user);

    void removeByName(User user, String tagBinder);

}
