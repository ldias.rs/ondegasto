package org.ldiasrs.ondegasto.service.common;

/**
 * Exceção na operação de algum serviço.
 * @author cardoso
 */
public class OperationServiceException extends AbstractOperationServiceException {

    private static final long serialVersionUID = -9123621286426959337L;

    public OperationServiceException(final String msg) {
        super(msg, null);
    }

    public OperationServiceException(final String msg, final Throwable e) {
        super(msg, e);
    }

}
