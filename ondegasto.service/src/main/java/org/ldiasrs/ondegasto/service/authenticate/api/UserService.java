package org.ldiasrs.ondegasto.service.authenticate.api;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.UserAlreadyRegisterException;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;



/**
 * Gerencia os serviços relacionados a authenticação.
 * @author cardoso
 *
 */
public interface UserService {

    User addUser(User user) throws OperationServiceException, UserAlreadyRegisterException;

    boolean isUserRegistered(String userName);

    boolean authenticate(User user) throws OperationServiceException;

}
