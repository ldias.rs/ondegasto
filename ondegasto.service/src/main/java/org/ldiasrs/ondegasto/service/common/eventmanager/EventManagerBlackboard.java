package org.ldiasrs.ondegasto.service.common.eventmanager;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppEvent;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;
import org.springframework.stereotype.Component;

import com.github.wolfie.blackboard.Blackboard;

/**
 * Implementation for {@link ServiceEventManager}.
 * @author cardoso
 */
@Component("serviceEventManager")
public class EventManagerBlackboard extends Blackboard implements ServiceEventManager {

    public EventManagerBlackboard() {
        this.register(ModelCrudChangeListener.class, ChangeEvent.class);
    }

    @Override
    public void fire(final AppEvent event) {
        super.fire(event);
    }

    @Override
    public void addListener(final AppListener listener) {
        super.addListener(listener);

    }

    @Override
    public void removeListener(final AppListener listener) {
        super.removeListener(listener);
    }
}
