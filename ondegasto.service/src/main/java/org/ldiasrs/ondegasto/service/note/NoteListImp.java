package org.ldiasrs.ondegasto.service.note;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.CrudNotifyService;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;
import org.ldiasrs.ondegasto.service.note.api.NoteList;
import org.ldiasrs.ondegasto.service.note.api.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implmentacao da lista de notas.
 * @author ld24078
 */
@Service
public class NoteListImp implements NoteList {

    @Autowired
    private NoteRepository repository;

    @Autowired
    private CrudNotifyService<Note> crudNotifyService;

    @Override
    public void add(final Note note) throws OperationServiceException {
        validateNoteSize(note);
        repository.create(note);
        crudNotifyService.notifyListener(note, ChangeType.INSERT);
    }

    private void validateNoteSize(final Note note) throws OperationServiceException {
        if (note.getHtml().length() > 32600) {
            throw new OperationServiceException("Nota muito grande para ser armazenada");
        }
    }

    @Override
    public void remove(final Note note) {
        repository.delete(note);
        crudNotifyService.notifyListener(note, ChangeType.DELETE);
    }

    @Override
    public Note getNoteByName(final String name, final User user) {
        return repository.getNoteByName(name, user);
    }

    @Override
    public Collection<Note> getAll(final User user) {
        return repository.getAll(user);
    }

    @Override
    public void update(final Note note) throws OperationServiceException {
        validateNoteSize(note);
        repository.update(note);
        crudNotifyService.notifyListener(note, ChangeType.UPDATE);
    }
}
