package org.ldiasrs.ondegasto.service.common.eventmanager.event;

import com.github.wolfie.blackboard.Listener;

/**
 * Define a application listener, to hide de API listener.
 * @author cardoso
 */
public interface AppListener extends Listener {

}
