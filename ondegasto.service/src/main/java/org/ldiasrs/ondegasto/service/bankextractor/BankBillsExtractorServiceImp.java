package org.ldiasrs.ondegasto.service.bankextractor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.ldiasrs.bankbillsreader.BankBillsReaderService;
import org.ldiasrs.bankbillsreader.BankParsedBill;
import org.ldiasrs.bankbillsreader.ItauCreditCardBillsReader;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.BillType;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author ld24078
 */
@Service
public class BankBillsExtractorServiceImp implements BankBillsExtractorService {

    @Autowired
    private TagResolver tagResolver;

    @Autowired
    private TagList tagList;

    @Override
    public Collection<Bill> extractBills(final User user, final String bankExtract, final Date date, final String[] tagnames) {
        Collection<Bill> extractedBills = new ArrayList<Bill>();
        BankBillsReaderService bankBillsReaderService = new ItauCreditCardBillsReader(bankExtract);
        Collection<BankParsedBill> parsedBills = bankBillsReaderService.parseBills();
        for (BankParsedBill bankParsedBill : parsedBills) {
            Bill bill = createBill(user, date, bankParsedBill);
            addTagResolvedByBillName(bill);
            addTagsInformedByUser(bill, tagnames);
            extractedBills.add(bill);
        }
        return extractedBills;
    }

    private void addTagsInformedByUser(final Bill bill, final String[] tagnames) {
        if (tagnames != null) {
            bill.addTags(tagList.getTags(bill.getUser(), tagnames));
        }
    }

    private void addTagResolvedByBillName(final Bill bill) {
        String[] subNames = bill.getName().split("\\s");
        for (String subName : subNames) {
            if (StringUtils.hasLength(subName)) {
                bill.addTags(tagResolver.resolveTagsByName(bill.getUser(), subName));
            }
        }
    }

    private Bill createBill(final User user, final Date date, final BankParsedBill bankParsedBill) {
        Bill bill = new Bill();
        bill.setType(BillType.COST);
        bill.setUser(user);
        bill.setDateOfBill(date);
        bill.setName(bankParsedBill.getName());
        bill.setValueOfBill(BigDecimal.valueOf(bankParsedBill.getValue()));
        bill.addTag(tagList.getTag(user, "credito"));
        bill.setObservation(bankParsedBill.getDescription());
        return bill;
    }

}
