package org.ldiasrs.ondegasto.service.tag;

import org.hibernate.criterion.Restrictions;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.ldiasrs.ondegasto.service.tag.api.TagRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementação do repositorio.
 * @author cardoso
 */
@Repository
public class TagRepositoryImp extends HibernateGenericDao<Tag, String> implements TagRepository {

    @Override
    @Transactional(readOnly = true)
    public Tag getTagByName(final String tagName, final User user) {
        return findByCriteriaAsUnicObject(Restrictions.and(Restrictions.eq("name", tagName), //$NON-NLS-1$
                Restrictions.eq("user", user))); //$NON-NLS-1$
    }
}
