package org.ldiasrs.ondegasto.service.bill;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.bill.api.BillRepository;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.springframework.stereotype.Repository;

/**
 * Implementação do DAO da lista de contas.
 * @author cardoso
 */
@Repository("billRepository")
public class BillRepositoryImp extends HibernateGenericDao<Bill, Long> implements BillRepository {

    @SuppressWarnings("unchecked")
    @Override
    public List<Bill> getBillsByInterval(final Date startDate, final Date endDate, final User user) {
        Criteria criteria = super.createCriteria(Restrictions.and(Restrictions.between("dateOfBill", startDate, endDate), //$NON-NLS-1$
                Restrictions.eq("user.loginName", user.getLoginName()))); //$NON-NLS-1$
        criteria.addOrder(Order.asc(Bill.PROPERTY_TYPE));
        criteria.addOrder(Order.desc(Bill.PROPERTY_VALUEOFBILL));
        criteria.addOrder(Order.desc(Bill.PROPERTY_PAID));
        criteria.addOrder(Order.desc(Bill.PROPERTY_NAME));
        return criteria.list();
    }

    @Override
    public List<Bill> getBillsByTags(final Tag[] tags) {
        if (tags.length < 0) {
            return new ArrayList<Bill>();
        }
        Criteria c = super.createCriteria();
        c.add(Restrictions.eq("user", tags[0].getUser())); //$NON-NLS-1$
        c.createAlias("tagList", "tag"); //$NON-NLS-1$ //$NON-NLS-2$
        Disjunction tagConjuction = Restrictions.disjunction();
        for (Tag tag : tags) {
            tagConjuction.add(Restrictions.eq("tag.name", tag.getName())); //$NON-NLS-1$
        }
        c.add(tagConjuction);
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return findByCriteria(c);
    }

}
