package org.ldiasrs.ondegasto.service.bill.api;

import java.util.Collection;
import java.util.List;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Representa uma lista de contas.
 * @author cardoso
 */
public interface BillList {

    void add(Bill cost);

    void add(Collection<Bill> bill);

    List<Bill> findBillsByTags(Tag... tag);

    List<Bill> findBillByMonthAndYear(User user, int month, int year);

    List<Bill> findCurrentMonthBills(User user);

    void remove(Collection<Bill> bills);

    void remove(Bill bill);

    void update(Bill bill);

    void update(Collection<Bill> bills);

    void add(ParceledBill parceledBill);

    void remove(ParceledBill parceledBill);

    void add(RepeatedBill repeatedBill);

    ParceledBill findParceledBillOfBill(Bill bill);

    RepeatedBill findRepetedBillOfBill(Bill bill);

    void update(ParceledBill parceledBill);

    void remove(RepeatedBill repedtedBill);

    void update(RepeatedBill repeatedBill);

}
