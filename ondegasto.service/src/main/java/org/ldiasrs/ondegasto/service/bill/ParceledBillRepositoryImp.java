package org.ldiasrs.ondegasto.service.bill;

import java.util.List;

import org.hibernate.Query;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.service.bill.api.ParceledBillRepository;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.springframework.stereotype.Repository;

/**
 * Repositorio das contas parceladas.
 * @author cardoso
 */
@Repository("parceledBillRepository")
public class ParceledBillRepositoryImp extends HibernateGenericDao<ParceledBill, Long> implements ParceledBillRepository {

    @Override
    public ParceledBill findParceledBillOfBill(final Bill bill) {
        String queryStr = "SELECT p FROM ParceledBill p JOIN p.parceledBills b WHERE b = :paramBill";
        Query query = super.createQuery(queryStr);
        query.setEntity("paramBill", bill);
        List<ParceledBill> foundEntrys = super.findByQuery(query);
        ParceledBill foundEntity = null;
        if (!foundEntrys.isEmpty()) {
            foundEntity = foundEntrys.iterator().next();
        }
        return foundEntity;
    }

}
