package org.ldiasrs.ondegasto.service.authenticate.api;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Definição de um DAO.
 * @author cardoso
 *
 */
public interface UserRepository extends GenericDao<User, String> {

    boolean authenticateUser(User credential);

    boolean isUserRegitered(String userName);

}
