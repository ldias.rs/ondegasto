package org.ldiasrs.ondegasto.service.tag.api;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Representa uma lista de Tags.
 * @author cardoso
 */
public interface TagList {

    Tag getTag(User user, String tagName);

    Collection<Tag> getTags(User user, String[] tagnames);

}
