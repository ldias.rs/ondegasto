package org.ldiasrs.ondegasto.service.tag;

import java.util.ArrayList;
import java.util.Collection;

import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tag.api.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementação padrão da lista de tags.
 * @author cardoso
 */
@Service
public class TagListImp implements TagList {

    @Autowired
    private TagRepository repository;

    @Override
    public Tag getTag(final User user, final String tagName) {
        Tag b = repository.getTagByName(tagName, user);
        if (b == null) {
            b = new Tag(tagName, user);
            repository.create(b);
        }
        return b;
    }

    @Override
    public Collection<Tag> getTags(final User user, final String[] tagnames) {
        Collection<Tag> tags = new ArrayList<Tag>();
        for (String name : tagnames) {
            tags.add(getTag(user, name));
        }
        return tags;
    }
}
