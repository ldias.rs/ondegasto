package org.ldiasrs.ondegasto.service.note.api;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Repositorio para notas.
 * @author ld24078
 */
public interface NoteRepository extends GenericDao<Note, Long> {

    Note getNoteByName(String name, User user);

    Collection<Note> getAll(User user);

}
