package org.ldiasrs.ondegasto.service.common.eventmanager.event;

import com.github.wolfie.blackboard.Event;

/**
 * Define a event for the application to hide the api event.
 * @author cardoso
 */
public interface AppEvent extends Event {

}
