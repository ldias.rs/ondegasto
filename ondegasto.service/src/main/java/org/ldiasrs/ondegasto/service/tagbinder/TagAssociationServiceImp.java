package org.ldiasrs.ondegasto.service.tagbinder;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagAssociationService;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ld24078
 */
@Service
public class TagAssociationServiceImp implements TagAssociationService {

    @Autowired
    private TagList tagList;

    @Autowired
    private TagResolver tagResolver;

    @Override
    public void associateBillNameOnTags(final User user, final String billName, final String[] tagNames) {
        TagBinder tagBinder = new TagBinder(user, billName);
        for (String tagName : tagNames) {
            tagBinder.bindWithTag(tagList.getTag(user, tagName));
        }
        tagResolver.addOrUpdateTagBinder(tagBinder);
    }

    @Override
    public void unAssociateBillName(final User user, final String billName) {
        tagResolver.removeByName(user, billName);
    }

    @Override
    public Collection<TagBinder> findAllTagsAssociationsByUser(final User user) {
        return tagResolver.findAllTagBindersByUser(user);
    }

}
