package org.ldiasrs.ondegasto.service.common.eventmanager.event;



/**
 * Define a change event of a entity.
 * @author cardoso
 * @param <T> Type of entity tha was changed.
 */
public class ChangeEvent<T> implements AppEvent {

    private T entity;
    private ChangeType changeType;

    public ChangeEvent(final T entityArg, final ChangeType changeTypeArg) {
        super();
        this.entity = entityArg;
        this.changeType = changeTypeArg;
    }

    public T getEntity() {
        return entity;
    }

    public ChangeType getChangeType() {
        return changeType;
    }
}
