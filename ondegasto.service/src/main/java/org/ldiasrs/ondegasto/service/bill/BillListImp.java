package org.ldiasrs.ondegasto.service.bill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.ldiasrs.ondegasto.common.DateUtil;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.bill.api.BillRepository;
import org.ldiasrs.ondegasto.service.bill.api.ParceledBillRepository;
import org.ldiasrs.ondegasto.service.bill.api.RepeatedBillRepository;
import org.ldiasrs.ondegasto.service.common.CrudNotifyService;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Gerencia as contas de um usuário.
 * @author cardoso
 */
@Service
public class BillListImp implements BillList {

    @Autowired
    private BillRepository billRepository;

    @Autowired
    private ParceledBillRepository parceledBillRepository;

    @Autowired
    private RepeatedBillRepository repeatedBillRepository;

    @Autowired
    private CrudNotifyService<Bill> crudNotifyService;

    @Autowired
    private TagList tagList;

    @Override
    public void remove(final Bill bill) {
        billRepository.delete(bill);
        crudNotifyService.notifyListener(bill, ChangeType.DELETE);
    }

    @Override
    public void remove(final Collection<Bill> bills) {
        billRepository.deleteAll(bills);
        for (Bill bill : bills) {
            crudNotifyService.notifyListener(bill, ChangeType.DELETE);
        }
    }

    @Override
    public void add(final Bill bill) {
        bill.setId(billRepository.create(bill).longValue());
        crudNotifyService.notifyListener(bill, ChangeType.INSERT);
    }

    @Override
    public void add(final ParceledBill parceledBill) {
        Collection<Bill> parceledBills = parceledBill.getParceledBills();
        parceledBillRepository.create(parceledBill);
        crudNotifyService.notifyListener(parceledBills, ChangeType.INSERT);
    }

    @Override
    public void remove(final ParceledBill parceledBill) {
        parceledBillRepository.delete(parceledBill);
        crudNotifyService.notifyListener(parceledBill.getParceledBills(), ChangeType.DELETE);
    }

    @Override
    public void remove(final RepeatedBill repeadtedBill) {
        repeatedBillRepository.delete(repeadtedBill);
        crudNotifyService.notifyListener(repeadtedBill.getRepeatedBills(), ChangeType.DELETE);
    }

    @Override
    public ParceledBill findParceledBillOfBill(final Bill bill) {
        return parceledBillRepository.findParceledBillOfBill(bill);
    }

    @Override
    public RepeatedBill findRepetedBillOfBill(final Bill bill) {
        return repeatedBillRepository.findRepetedBillOfBill(bill);
    }

    @Override
    public void add(final Collection<Bill> bill) {
        for (Bill b : bill) {
            add(b);
        }
    }

    @Override
    public void add(final RepeatedBill repeatedBill) {
        Collection<Bill> repeatedBills = repeatedBill.getRepeatedBills();
        repeatedBill.addTagOnBills(tagList.getTag(repeatedBill.getUser(), "repetida"));
        repeatedBillRepository.create(repeatedBill);
        crudNotifyService.notifyListener(repeatedBills, ChangeType.INSERT);
    }

    @Override
    public void update(final Bill bill) {
        billRepository.update(bill);
        crudNotifyService.notifyListener(bill, ChangeType.UPDATE);
    }

    @Override
    public void update(final ParceledBill parceledBill) {
        parceledBillRepository.update(parceledBill);
        crudNotifyService.notifyListener(parceledBill.getParceledBills(), ChangeType.UPDATE);
    }

    @Override
    public void update(final RepeatedBill repeatedBill) {
        // Verifica quais as contas foram removidas
        Collection<Bill> oldBeans = repeatedBill.getRemovedCacheBills();
        repeatedBillRepository.update(repeatedBill);
        // Notifica contas removidas
        if (!oldBeans.isEmpty()) {
            crudNotifyService.notifyListener(oldBeans, ChangeType.DELETE);
            oldBeans.clear();
        }
        // notifica a atualizacao do restante das contas
        crudNotifyService.notifyListener(repeatedBill.getRepeatedBills(), ChangeType.UPDATE);
    }

    @Override
    public List<Bill> findCurrentMonthBills(final User user) {
        return findBillByMonthAndYear(user, DateUtil.getCurrentMonth(), DateUtil.getCurrentYear());
    }

    @Override
    public List<Bill> findBillsByTags(final Tag... tag) {
        if (tag.length > 0) {
            return billRepository.getBillsByTags(tag);
        }
        return new ArrayList<Bill>();
    }

    @Override
    public List<Bill> findBillByMonthAndYear(final User user, final int month, final int year) {
        Date startTimeOfMonth = DateUtil.getStartTimeOfMonth(month, year);
        Date endTimeOfMonth = DateUtil.getEndTimeOfMonth(month, year);
        List<Bill> billsByInterval = billRepository.getBillsByInterval(startTimeOfMonth, endTimeOfMonth, user);
        addFixedBills(billsByInterval, user);
        return billsByInterval;
    }

    private void addFixedBills(final List<Bill> billsByInterval, final User user) {
        // billsByInterval.add(new Bill(user, "FIXA", BigDecimal.ONE,
        // BillType.COST));
    }

    @Override
    public void update(final Collection<Bill> bills) {
        for (Bill bill : bills) {
            update(bill);
        }
    }

    public void setBillRepository(final BillRepository billRepositoryArg) {
        billRepository = billRepositoryArg;
    }
}
