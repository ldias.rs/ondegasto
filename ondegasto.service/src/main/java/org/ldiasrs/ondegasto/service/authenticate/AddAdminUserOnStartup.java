package org.ldiasrs.ondegasto.service.authenticate;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Adiciona o usuário admin no inicio do contexto.
 * @author cardoso
 */
@Service
public class AddAdminUserOnStartup {

    public static final User ADMIN_USER = new User("admin", "admin"); //$NON-NLS-1$//$NON-NLS-2$

    @Autowired
    public AddAdminUserOnStartup(final UserService service) throws OperationServiceException {
        try {
            service.addUser(ADMIN_USER);
        } catch (UserAlreadyRegisterException e) {
            // se já existe nao faz nada mesmo.
        }
    }
}
