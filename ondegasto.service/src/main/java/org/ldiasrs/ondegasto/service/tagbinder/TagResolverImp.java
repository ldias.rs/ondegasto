package org.ldiasrs.ondegasto.service.tagbinder;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.collections.list.AbstractLinkedList;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.CrudNotifyService;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagBinderRepository;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementantion for tag Resolver.
 * @author ld24078
 */
@Service("tagResolver")
public class TagResolverImp extends AbstractLinkedList implements TagResolver {

    @Autowired
    private TagBinderRepository repository;

    @Autowired
    private CrudNotifyService<TagBinder> crudNotifyService;

    @Autowired
    private TagList tagList;

    @Override
    public Set<Tag> resolveTagsByName(final User user, final String billName) {
        TagBinder binder = repository.findByUserName(user, billName.toLowerCase());
        Set<Tag> tags = new HashSet<Tag>();
        if (binder != null) {
            Iterator<Tag> iterator = binder.tagNamesIteartor();
            while (iterator.hasNext()) {
                tags.add(iterator.next());
            }
        }
        return tags;
    }

    @Override
    public void addOrUpdateTagBinder(final TagBinder tagBinder) {
        TagBinder duplicated = repository.findByUserName(tagBinder.getUser(), tagBinder.getName());
        if (duplicated != null) {
            repository.delete(duplicated);
            crudNotifyService.notifyListener(duplicated, ChangeType.DELETE);
        }
        repository.createOrUpdate(tagBinder);
        crudNotifyService.notifyListener(tagBinder, ChangeType.INSERT);
    }

    @Override
    public void removeByName(final User user, final String tagBinder) {
        TagBinder tagToRemobe = repository.findByUserName(user, tagBinder);
        repository.delete(tagToRemobe);
        crudNotifyService.notifyListener(tagToRemobe, ChangeType.DELETE);
    }

    @Override
    public Collection<TagBinder> findAllTagBindersByUser(final User user) {
        return repository.findAllTagBindersByUser(user);
    }

}
