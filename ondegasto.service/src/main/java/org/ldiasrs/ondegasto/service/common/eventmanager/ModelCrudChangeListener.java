package org.ldiasrs.ondegasto.service.common.eventmanager;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.AppListener;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;

import com.github.wolfie.blackboard.annotation.ListenerMethod;

/**
 * Define a change lister for
 * {@link org.ldiasrs.ondegasto.service.domain.bill.Bill}.
 * @author cardoso
 * @param <ENTITY>
 */
public interface ModelCrudChangeListener<ENTITY> extends AppListener {

    @ListenerMethod
    void changeEvent(ChangeEvent<ENTITY> event);

}
