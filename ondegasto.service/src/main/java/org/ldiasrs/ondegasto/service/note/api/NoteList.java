package org.ldiasrs.ondegasto.service.note.api;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;

/**
 * Define os metodos necessarios na lista de notas.
 * @author Leonardo Cardoso Dias
 */
public interface NoteList {

    void add(Note note) throws OperationServiceException;

    Note getNoteByName(String name, User user);

    void remove(Note note);

    void update(Note note) throws OperationServiceException;

    Collection<Note> getAll(User user);

}
