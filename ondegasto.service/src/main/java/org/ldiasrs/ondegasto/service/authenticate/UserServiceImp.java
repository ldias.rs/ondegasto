package org.ldiasrs.ondegasto.service.authenticate;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.authenticate.api.UserRepository;
import org.ldiasrs.ondegasto.service.authenticate.api.UserService;
import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.ldiasrs.ondegasto.service.common.eventmanager.ServiceEventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

/**
 * Implementação do serviço de autenticação do sistema.
 * @author cardoso
 */
@Service
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ServiceEventManager eventManager;

    @Override
    public User addUser(final User user) throws OperationServiceException, UserAlreadyRegisterException {
        try {
            if (userRepository.isUserRegitered(user.getLoginName())) {
                String msg = String.format("This user loginName '%s' is alrealdy in use", user.getLoginName()); //$NON-NLS-1$
                throw new UserAlreadyRegisterException(msg);
            }
            userRepository.create(user);
            return user;
        } catch (DataAccessException e) {
            throw new OperationServiceException("Data acess error. ERROR: " + e.getMessage(), e); //$NON-NLS-1$
        }
    }

    @Override
    public boolean isUserRegistered(final String userName) {
        return userRepository.isUserRegitered(userName);
    }

    @Override
    public boolean authenticate(final User user) throws OperationServiceException {
        try {
            boolean authenticateUser = userRepository.authenticateUser(user);
            return authenticateUser;

        } catch (DataAccessException e) {
            throw new OperationServiceException("Data acess error. ERROR: " + e.getMessage(), e); //$NON-NLS-1$
        }
    }

}
