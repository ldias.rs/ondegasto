package org.ldiasrs.ondegasto.service.tagbinder;

import java.util.Collection;

import org.hibernate.criterion.Restrictions;
import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagBinderRepository;
import org.springframework.stereotype.Repository;

/**
 * Respository for {@link TagBinder}.
 * @author ld24078
 */
@Repository
public class TagBinderRepositoryImp extends HibernateGenericDao<TagBinder, Long> implements TagBinderRepository {

    @Override
    public TagBinder findByUserName(final User user, final String name) {
        return findByCriteriaAsUnicObject(Restrictions.and(Restrictions.eq("name", name), //$NON-NLS-1$
                Restrictions.eq("user", user))); //$NON-NLS-1$
    }

    @Override
    public Collection<TagBinder> findAllTagBindersByUser(final User user) {
        return findByCriteria(Restrictions.eq("user", user));
    }
}
