package org.ldiasrs.ondegasto.service.bill;

import java.util.List;

import org.hibernate.Query;
import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.service.bill.api.RepeatedBillRepository;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.springframework.stereotype.Repository;

/**
 * Repositorio das contas repetidas.
 * @author cardoso
 *
 */
@Repository("repeatedBillRepositoryImp")
public class RepeatedBillRepositoryImp extends HibernateGenericDao<RepeatedBill, Long> implements RepeatedBillRepository {

    @Override
    public RepeatedBill findRepetedBillOfBill(final Bill bill) {
        String queryStr = "SELECT p FROM RepeatedBill p JOIN p.repeatedBills b WHERE b = :paramBill";
        Query query = super.createQuery(queryStr);
        query.setEntity("paramBill", bill);
        List<RepeatedBill> foundEntrys = super.findByQuery(query);
        RepeatedBill foundEntity = null;
        if (!foundEntrys.isEmpty()) {
            foundEntity = foundEntrys.iterator().next();
        }
        return foundEntity;
    }
}
