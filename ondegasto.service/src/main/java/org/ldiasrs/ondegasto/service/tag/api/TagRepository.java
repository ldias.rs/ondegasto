package org.ldiasrs.ondegasto.service.tag.api;

import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Repositorui para uma tag de bill.
 * @author cardoso
 */
public interface TagRepository extends GenericDao<Tag, String> {

    Tag getTagByName(String tagName, User user);

}
