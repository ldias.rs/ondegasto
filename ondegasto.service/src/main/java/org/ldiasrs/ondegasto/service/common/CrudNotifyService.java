package org.ldiasrs.ondegasto.service.common;

import java.util.Collection;

import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;

/**
 * Define a Service to notify CRUD events.
 * @author ld24078
 * @param <T>
 */
public interface CrudNotifyService<T> {

    void notifyListener(Collection<T> list, ChangeType changeType);

    void notifyListener(T entity, ChangeType changeType);

}
