package org.ldiasrs.ondegasto.service.bill.api;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.ParceledBill;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Repositorio de Bill.
 * @author cardoso
 */
public interface ParceledBillRepository extends GenericDao<ParceledBill, Long> {

    ParceledBill findParceledBillOfBill(Bill bill);

}
