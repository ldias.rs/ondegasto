package org.ldiasrs.ondegasto.service.note;

import java.util.Collection;

import org.hibernate.criterion.Restrictions;
import org.ldiasrs.ondegasto.domain.note.Note;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.ldiasrs.ondegasto.service.note.api.NoteRepository;
import org.springframework.stereotype.Repository;

/**
 * Implementacao do repositorio de notas.
 * @author ld24078
 */
@Repository
public class NoteRepositoryImp extends HibernateGenericDao<Note, Long> implements NoteRepository {

    @Override
    public Note getNoteByName(final String name, final User user) {
        return findByCriteriaAsUnicObject(Restrictions.and(Restrictions.eq("name", name), //$NON-NLS-1$
                Restrictions.eq("user", user))); //$NON-NLS-1$
    }

    @Override
    public Collection<Note> getAll(final User user) {
        return findByCriteria(Restrictions.eq("user", user));
    }
}
