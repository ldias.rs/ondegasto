package org.ldiasrs.ondegasto.service.authenticate;

import org.ldiasrs.ondegasto.service.common.AbstractOperationServiceException;



/**
 * Exceção lançada quando o usuário já é existente.
 * @author cardoso
 *
 */
public class UserAlreadyRegisterException extends AbstractOperationServiceException {

    private static final long serialVersionUID = 3048938029442228670L;

    public UserAlreadyRegisterException(final String msg) {
        super(msg);
    }

    public UserAlreadyRegisterException(final String msg, final Throwable e) {
        super(msg, e);
    }
}
