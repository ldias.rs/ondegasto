package org.ldiasrs.ondegasto.service.bankextractor;

import java.util.Collection;
import java.util.Date;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * Define a service to extract bills from a bank extract.
 * @author ld24078
 */
public interface BankBillsExtractorService {

    Collection<Bill> extractBills(User user, String bankExtract, Date value, String[] tagnames);
}
