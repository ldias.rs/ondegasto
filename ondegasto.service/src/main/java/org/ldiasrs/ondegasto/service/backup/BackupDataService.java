package org.ldiasrs.ondegasto.service.backup;

import org.ldiasrs.ondegasto.service.common.OperationServiceException;

/**
 * Define a Service of backup.
 * @author ld24078
 */
public interface BackupDataService {

    String backupData() throws OperationServiceException;

}
