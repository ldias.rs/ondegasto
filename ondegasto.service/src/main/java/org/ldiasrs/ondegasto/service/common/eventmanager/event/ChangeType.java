package org.ldiasrs.ondegasto.service.common.eventmanager.event;

/**
 * Define the tepy of change.
 * @author cardoso
 *
 */
public enum ChangeType {
    UPDATE,
    DELETE,
    INSERT
}
