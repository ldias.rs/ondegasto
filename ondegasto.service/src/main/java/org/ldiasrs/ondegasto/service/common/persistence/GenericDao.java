package org.ldiasrs.ondegasto.service.common.persistence;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface que define um DAO para CRUD genérico. Implementação retirada do
 * artigo <a
 * href="http://www.ibm.com/developerworks/java/library/j-genericdao.html">Don't
 * Repeat the DAO</a>.
 * @author caesar.hoppen
 * @param <T> Tipo do objeto de persistência.
 * @param <PK> Chave primária do Objeto.
 */
public interface GenericDao<T, PK extends Serializable> {

    /**
     * Persiste o objeto no banco de dados.
     */
    PK create(T newInstance);

    /**
     * Recupera objeto do banco de dados.
     */
    T get(PK id);

    /**
     * Salva as mudanças realizadas no objeto no banco de dados.
     */
    void update(T transientObject);

    /**
     * Remove objeto do banco de dados.
     */
    void delete(T persistentObject);

    void deleteAll(Collection<T> list);

    void createOrUpdate(T o);

    Collection<T> getAll();

}
