package org.ldiasrs.ondegasto.service.bill.api;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.bill.RepeatedBill;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Repositório para {@link RepeatedBill}.
 * @author cardoso
 *
 */
public interface RepeatedBillRepository  extends GenericDao<RepeatedBill, Long> {

    RepeatedBill findRepetedBillOfBill(Bill bill);

}
