package org.ldiasrs.ondegasto.service.tagbinder.api;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Define a repository for {@link TagBinder}.
 * @author ld24078
 */
public interface TagBinderRepository extends GenericDao<TagBinder, Long> {

    TagBinder findByUserName(User user, String name);

    Collection<TagBinder> findAllTagBindersByUser(User user);

}
