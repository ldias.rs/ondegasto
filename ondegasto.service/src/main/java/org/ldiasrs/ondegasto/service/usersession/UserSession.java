package org.ldiasrs.ondegasto.service.usersession;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.backup.BackupDataService;
import org.ldiasrs.ondegasto.service.bankextractor.BankBillsExtractorService;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.common.eventmanager.ServiceEventManager;
import org.ldiasrs.ondegasto.service.note.api.NoteList;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagAssociationService;

/**
 * Representa uma sessão do usuário.
 * @author cardoso
 */
public interface UserSession {

    BillList getBillList();

    User getUser();

    TagList getBillTagList();

    NoteList getNoteList();

    ServiceEventManager getServiceEventManager();

    BackupDataService getBackupDataService();

    TagAssociationService getTagAssociationService();

    BankBillsExtractorService getBankBillsExtractorService();

}
