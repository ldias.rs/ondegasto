package org.ldiasrs.ondegasto.service.usersession;

import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.backup.BackupDataService;
import org.ldiasrs.ondegasto.service.bankextractor.BankBillsExtractorService;
import org.ldiasrs.ondegasto.service.bill.api.BillList;
import org.ldiasrs.ondegasto.service.common.eventmanager.ServiceEventManager;
import org.ldiasrs.ondegasto.service.note.api.NoteList;
import org.ldiasrs.ondegasto.service.tag.api.TagList;
import org.ldiasrs.ondegasto.service.tagbinder.api.TagAssociationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Implementação da sessão do usuário.
 * @author cardoso
 */
@Component
@Scope(value = "prototype")
public class UserSessionImp implements UserSession {

    @Autowired
    private BillList billList;

    @Autowired
    private TagList tagList;

    @Autowired
    private NoteList noteList;

    @Autowired
    private ServiceEventManager serviceEventManager;

    @Autowired
    private BackupDataService backupDataService;

    @Autowired
    private TagAssociationService tagAssociationService;

    @Autowired
    private BankBillsExtractorService bankBillsExtractorService;

    private User user;

    public UserSessionImp() {
        this(User.createSystemUser());
    }

    public UserSessionImp(final User userArg) {
        user = userArg;
    }

    @Override
    public BillList getBillList() {
        return billList;
    }

    @Override
    public TagList getBillTagList() {
        return tagList;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public ServiceEventManager getServiceEventManager() {
        return serviceEventManager;
    }

    public void setUser(final User userArg) {
        user = userArg;
    }

    @Override
    public NoteList getNoteList() {
        return noteList;
    }

    @Override
    public BackupDataService getBackupDataService() {
        return backupDataService;
    }

    @Override
    public TagAssociationService getTagAssociationService() {
        return tagAssociationService;
    }

    @Override
    public BankBillsExtractorService getBankBillsExtractorService() {
        return bankBillsExtractorService;
    }
}
