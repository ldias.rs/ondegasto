package org.ldiasrs.ondegasto.service.tagbinder.api;

import java.util.Collection;

import org.ldiasrs.ondegasto.domain.tag.TagBinder;
import org.ldiasrs.ondegasto.domain.user.User;

/**
 * @author ld24078
 */
public interface TagAssociationService {

    void associateBillNameOnTags(User user, String billName, String[] tagNames);

    void unAssociateBillName(User user, String billName);

    Collection<TagBinder> findAllTagsAssociationsByUser(User user);

}
