package org.ldiasrs.ondegasto.service.common;

import java.util.Collection;
import java.util.HashSet;

/**
 * Exceção que controla os erros comuns.
 * @author cardoso
 *
 */
public abstract class AbstractOperationServiceException extends Exception {

    private static final long serialVersionUID = -8291598076152702203L;
    private Collection<String> errorMgs;

    public AbstractOperationServiceException(final String msg) {
        this(msg, null);
    }

    public AbstractOperationServiceException(final String msg, final Throwable e) {
        super(msg, e);
        errorMgs = new HashSet<String>();
        errorMgs.add(msg);
    }

}
