package org.ldiasrs.ondegasto.service.bill.api;

import java.util.Date;
import java.util.List;

import org.ldiasrs.ondegasto.domain.bill.Bill;
import org.ldiasrs.ondegasto.domain.tag.Tag;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.GenericDao;

/**
 * Repositorio de Bill.
 * @author cardoso
 */
public interface BillRepository extends GenericDao<Bill, Long> {

    /**
     * Busca as contas em um determinado intervalo, para um determiado usuário.
     */
    List<Bill> getBillsByInterval(Date startDate, Date endDate, User user);

    /**
     * Busca as contas relacionadas com lista de {@link Tag} de usuário
     * especificadas.
     */
    List<Bill> getBillsByTags(Tag[] tags);

}
