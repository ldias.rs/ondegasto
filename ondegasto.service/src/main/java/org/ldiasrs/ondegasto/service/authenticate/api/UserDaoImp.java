package org.ldiasrs.ondegasto.service.authenticate.api;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.ldiasrs.ondegasto.domain.user.User;
import org.ldiasrs.ondegasto.service.common.persistence.HibernateGenericDao;
import org.springframework.stereotype.Repository;

/**
 * Dao de usuários utilizando Hibernate.
 * @author cardoso
 *
 */
@Repository
public class UserDaoImp extends HibernateGenericDao<User, String> implements UserRepository {

    @Override
    public boolean authenticateUser(final User user) {
        return findByCriteriaAsUnicObject(
                Restrictions.and(
                        Restrictions.eq("loginName", user.getLoginName()), //$NON-NLS-1$
                        Restrictions.eq("password", user.getPassword())//$NON-NLS-1$
            )) != null;
    }

    @Override
    public boolean isUserRegitered(final String userName) {
        String queryStr = String.format("SELECT COUNT(%s) FROM %s WHERE %s = :userName", User.PROPERTY_LOGINNAME, User.ENTITY_NAME,  User.PROPERTY_LOGINNAME); //$NON-NLS-1$
        Query query = createQuery(queryStr);
        query.setString("userName", userName); //$NON-NLS-1$

        @SuppressWarnings("unchecked")
        List<Long> list = query.list();
        return list.iterator().next().longValue() > 0;
    }
}
