package org.ldiasrs.ondegasto.service.common.persistence;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementação do DAO genérico utilizando Hibernate.
 * @author caesar.hoppen
 * @param <T> Objeto persistido.
 * @param <PK> Chave que identifica <code>T</code>.
 */
@Transactional(propagation = Propagation.REQUIRED)
@Repository
public class HibernateGenericDao<T, PK extends Serializable> extends HibernateDaoSupport implements GenericDao<T, PK> {

    private Class<T> type;

    @SuppressWarnings("unchecked")
    public HibernateGenericDao() {
        if (!getClass().equals(HibernateGenericDao.class)) {
            this.setType((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]);
        }
    }

    @Autowired(required = true)
    public void putSessionFactory(final SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @SuppressWarnings("unchecked")
    @Override
    public PK create(final T o) {
        return (PK) this.getHibernateTemplate().save(o);
    }

    @Override
    @Transactional(readOnly = true)
    public T get(final PK id) {
        return this.getHibernateTemplate().get(getType(), id);
    }

    @Override
    public void update(final T o) {
        this.getHibernateTemplate().update(o);
    }

    @Override
    public void createOrUpdate(final T o) {
        this.getHibernateTemplate().saveOrUpdate(o);
    }

    @Override
    public void delete(final T o) {
        this.getHibernateTemplate().delete(o);
    }

    @Override
    public void deleteAll(final Collection<T> list) {
        this.getHibernateTemplate().deleteAll(list);
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    protected List<T> findByCriteria(final Criterion... criterion) {
        try {
            Criteria crit = createCriteria(criterion);
            crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            return crit.list();
        } catch (final HibernateException ex) {
            throw convertHibernateAccessException(ex);
        }
    }

    protected T findByCriteriaAsUnicObject(final Criterion... criterion) {
        List<T> list = findByCriteria(criterion);
        return list.isEmpty() ? null : list.iterator().next();
    }

    @SuppressWarnings("unchecked")
    protected List<T> findByCriteria(final Criteria c) {
        return c.list();
    }

    @SuppressWarnings("unchecked")
    protected List<T> findByQuery(final Query q) {
        return q.list();
    }

    protected Criteria createCriteria(final Criterion... criterion) {
        Criteria crit = createCriteria();
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit;
    }

    public Query createQuery(final String queryString) {
        return this.getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(queryString);
    }

    protected Criteria createCriteria() {
        Criteria crit = this.getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(getType());
        return crit;
    }

    public void setType(final Class<T> typeArg) {
        this.type = typeArg;
    }

    public Class<T> getType() {
        return type;
    }

    @Override
    public Collection<T> getAll() {
        String queryStr = "FROM " + type.getName();
        return findByQuery(createQuery(queryStr));
    }
}
