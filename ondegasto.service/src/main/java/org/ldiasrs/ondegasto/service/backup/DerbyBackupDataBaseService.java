package org.ldiasrs.ondegasto.service.backup;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.sql.DataSource;

import org.ldiasrs.ondegasto.service.common.OperationServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Implements a backup service for Derby.
 * @author ld24078
 */
@Service(value = "backupDataService")
public class DerbyBackupDataBaseService implements BackupDataService {

    @Value("${database_backup_location}")
    private String backupdirectory;
    @Autowired
    private DataSource dataSource;

    private final SimpleDateFormat sd = new SimpleDateFormat("yyyy_MM_dd_HH_mm");

    @Override
    public String backupData() throws OperationServiceException {
        CallableStatement cs = null;
        String finalDyrectoryBkp = String.format("%s_%s", backupdirectory, getSufixFormat());
        try {
            cs = dataSource.getConnection().prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
            cs.setString(1, finalDyrectoryBkp);
            cs.execute();
            System.out.println("backed up database to " + finalDyrectoryBkp);

        } catch (SQLException e) {
            throw new OperationServiceException("Error on execute backup database", e);
        } finally {
            if (cs != null) {
                try {
                    cs.close();
                } catch (SQLException e) {
                    throw new OperationServiceException("Error on execute close CallableStatement on backup database", e);
                }
            }
        }
        return finalDyrectoryBkp;
    }

    private String getSufixFormat() {
        return sd.format(GregorianCalendar.getInstance().getTime());
    }
}
