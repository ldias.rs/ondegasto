package org.ldiasrs.ondegasto.service.common;

import java.util.Collection;

import org.ldiasrs.ondegasto.service.common.eventmanager.ServiceEventManager;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeEvent;
import org.ldiasrs.ondegasto.service.common.eventmanager.event.ChangeType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ld24078
 * @param <T> The type of bean.
 */
@Service
public class CrudNotifyServiceImp<T> implements CrudNotifyService<T> {

    @Autowired
    private ServiceEventManager eventManager;

    @Override
    public void notifyListener(final Collection<T> list, final ChangeType changeType) {
        for (T entity : list) {
            notifyListener(entity, changeType);
        }
    }

    @Override
    public void notifyListener(final T entity, final ChangeType changeType) {
        eventManager.fire(new ChangeEvent<T>(entity, changeType));
    }

}
