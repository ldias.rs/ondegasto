#!/bin/sh
##################################################################
# Executa um UPDATE database com o diff das tabelas
##################################################################
LIQUIBASE_HOME=./liquibase
TARGET_WAR_NAME=../ondegasto.view.vaadin/target/ondegasto.view.vaadin-0.0.1-SNAPSHOT.war
HOMOLOG_DATABASE_URL="jdbc:derby:C:/ondegasto/database"
NOW=$(date +"%Y%m%d-%H%M%S")
FILE_CHANGELOG_NAME=changelogs/generated-changelog-homolog-$NOW.xml
$LIQUIBASE_HOME/liquibase --driver=org.apache.derby.jdbc.EmbeddedDriver \
--classpath=$TARGET_WAR_NAME \
--changeLogFile=$FILE_CHANGELOG_NAME \
--url=$HOMOLOG_DATABASE_URL \
--username=userdb \
--password=userdb \
generateChangeLog
