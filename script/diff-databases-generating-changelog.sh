#!/bin/bash
########################################################################################
# Faz o diff da base de DESENV com a HOMOLOG e gera changelog
########################################################################################
LIQUIBASE_HOME=./liquibase
TARGET_WAR_NAME=../ondegasto.view.vaadin/target/ondegasto.view.vaadin-0.0.1-SNAPSHOT.war
DESENV_DATABASE_URL="jdbc:derby:C:\ondegasto\database"
HOMOLOG_DATABASE_URL="jdbc:derby:C:/ondegasto/desenvdb"
FILE_CHANGELOG_NAME=changelogs/diff-dbs-desenv-homolog-changelog.xml
$LIQUIBASE_HOME/liquibase --driver=org.apache.derby.jdbc.EmbeddedDriver \
--classpath=$TARGET_WAR_NAME \
--url=$DESENV_DATABASE_URL \
--username=userdb \
--password=userdb \
--changeLogFile=$FILE_CHANGELOG_NAME \
diffChangeLog \
--referenceUrl=$HOMOLOG_DATABASE_URL \
--referenceUsername=userdb \
--referencePassword=userdb
