#!/bin/sh
EMPACOTAR="-$1";
#########################################################
# Define Propriedades 
#########################################################
CONFIG_DIR="C:/cygwin2012/home/ld24078/.ondegasto"
PROJECT_DIR=/home/ld24078/ldias-workspace/ondegasto.project
DEPLOY_DIR=/home/ld24078/files/pessoal/ondegasto-deploy
WAR_NAME=ondegasto.view.vaadin-0.0.1-SNAPSHOT.war
NEW_WAR=$PROJECT_DIR/ondegasto.view.vaadin/target/$WAR_NAME
HIBERNATE_FILE=$PROJECT_DIR/ondegasto.service/src/main/resources/database/hibernate.xml 
alias mvn=/home/ld24078/dados/programas/apache-maven/apache-maven-2.2.1/bin/mvn
#########################################################
# Coloca o Hibernate em modo de validacao
#########################################################
echo "Trocando modo hibernate para validacao..."
sed -i 's/hibernate\.hbm2ddl\.auto\=.*/hibernate.hbm2ddl.auto=validate/g' $HIBERNATE_FILE
#########################################################
# Empacota o WAR
#########################################################
if [ "--skp.pkg" != $EMPACOTAR ];
  then
     cd $PROJECT_DIR
     mvn clean package
  else
     echo "Reutilizando pacote existe..."
fi
#########################################################
# Faz o deploy do WAR
#########################################################
if [ -e $NEW_WAR ];
 then
   echo "Apagando deploy antigo..."
   ~/scripts/util/trash.sh $DEPLOY_DIR/$WAR_NAME
   mkdir -p $DEPLOY_DIR
   echo "Copiando deploy novo..."
   cp $NEW_WAR $DEPLOY_DIR/$WAR_NAME
   echo "Criando executavel...."
   echo "#!/bin/sh" >  $DEPLOY_DIR/run.sh
   echo "java -jar -Dondegasto_conf=$CONFIG_DIR $WAR_NAME" >> $DEPLOY_DIR/run.sh
   chmod +x $DEPLOY_DIR/run.sh
   echo "Deploy finalizado \o/"
 else
   echo "FALHA no DEPLOY WAR nao encontrado..."  
fi  
