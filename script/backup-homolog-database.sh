#!/bin/sh
############################################
# Faz backup do banco de dados homolog
############################################
BACKUP_DIR=/home/ld24078/c-windows/ondegasto/database
NOW=$(date +"%Y%m%d-%H%M%S")
BACKUP_FILE=/home/ld24078/c-windows/ondegasto/database-$NOW.tgz
tar -czvf  $BACKUP_FILE $BACKUP_DIR
