package org.ldiasrs.ondegasto.common;

import java.text.Normalizer;
import java.util.HashSet;
import java.util.Set;

public final class StringUtil {

    public static final String COMMA_SEPARATE_CHAR = ",";

    private StringUtil() {
    }

    public static String mergeStringTags(final String[] tagsAsStringList) {
        return mergeStringTags(tagsAsStringList, COMMA_SEPARATE_CHAR, COMMA_SEPARATE_CHAR);
    }

    public static String mergeStringTags(final String[] tagsAsStringList, final String separateChar, final String newSeparateChar) {
        Set<String> distinctTagsChars = new HashSet<String>();
        StringBuffer allTagsAsStringSeparatedWithComma = new StringBuffer();
        for (String tagsAsString : tagsAsStringList) {
            String[] tagsSplited = tagsAsString.split(separateChar);
            for (String tagChar : tagsSplited) {
                String normalizedTagChar = normalizeSpecialCharsAndAccents(tagChar).trim();
                if (hasText(normalizedTagChar)) {
                    if (isCharNotIncluded(normalizedTagChar, distinctTagsChars)) {
                        distinctTagsChars.add(normalizedTagChar);
                        allTagsAsStringSeparatedWithComma.append(normalizedTagChar);
                        allTagsAsStringSeparatedWithComma.append(newSeparateChar);
                    }
                }
            }
        }
        return allTagsAsStringSeparatedWithComma.toString();
    }

    public static boolean hasText(final String str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasLength(final CharSequence str) {
        return str != null && str.length() > 0;
    }

    private static boolean isCharNotIncluded(final String tagChar, final Set<String> distinctTagsChars) {
        return !distinctTagsChars.contains(tagChar);
    }

    private static String removeSpecialChars(final String distictTag) {
        return distictTag.replaceAll("[^\\w\\d]", "");
    }

    public static String normalizeSpecialCharsAndAccents(final String str) {
        return removeSpecialChars(replaceAccents(str));
    }

    public static String replaceAccents(final String str) {
        String normalizedStr = Normalizer.normalize(str, Normalizer.Form.NFD);
        normalizedStr = normalizedStr.replaceAll("[^\\p{ASCII}]", "");
        return normalizedStr;
    }

}
