package org.ldiasrs.ondegasto.common;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Fornece utilitários para Datas.
 * @author cardoso
 */
public final class DateUtil {

    private DateUtil() {
    }

    public static int getCurrentYear() {
        return GregorianCalendar.getInstance().get(GregorianCalendar.YEAR);
    }

    public static int getCurrentMonth() {
        return GregorianCalendar.getInstance().get(GregorianCalendar.MONTH);
    }

    public static Date getStartTimeOfMonth(final int month, final int year) {
        GregorianCalendar startDate = new GregorianCalendar();
        startDate.set(year, month, startDate.getMinimum(GregorianCalendar.DATE), startDate.getMinimum(GregorianCalendar.HOUR),
                startDate.getMinimum(GregorianCalendar.MINUTE));

        return startDate.getTime();
    }

    public static Date getEndTimeOfMonth(final int month, final int year) {
        GregorianCalendar endDate = new GregorianCalendar();
        endDate.set(year, month, endDate.getActualMaximum(GregorianCalendar.DATE), endDate.getActualMaximum(GregorianCalendar.HOUR),
                endDate.getActualMaximum(GregorianCalendar.MINUTE));
        return endDate.getTime();
    }
}
