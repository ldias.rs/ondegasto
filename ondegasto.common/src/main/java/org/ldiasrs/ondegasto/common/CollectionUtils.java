package org.ldiasrs.ondegasto.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Classe de utilidades relacionadas a coleção.
 */
public final class CollectionUtils {
    private static final String APPENDER = ", "; //$NON-NLS-1$
    protected static final String LEFT_DELIMITER = "["; //$NON-NLS-1$
    protected static final String RIGHT_DELIMITER = "]"; //$NON-NLS-1$

    private CollectionUtils() {
    }

    /**
     * Subtrai uma coleção de uma outra.
     * @param subtractFrom da onde os itens serão subtraídos.
     * @param itemsToSubtract items a serem subtraídos da coleção.
     */
    public static <B> Collection<B> subtract(final Collection<B> subtractFrom, final Collection<B> itemsToSubtract) {
        Collection<B> subtractedCollection = new ArrayList<B>(subtractFrom);

        for (B itemToSubtract : itemsToSubtract) {
            subtractedCollection.remove(itemToSubtract);
        }
        return subtractedCollection;
    }

    /**
     * Retorna true se uma Coleção for vazia ou null.
     * @param collection
     * @return boolean
     */
    public static boolean isEmpty(final Collection<?> collection) {
        return collection == null ? true : collection.isEmpty();
    }

    public static <B> boolean isEmpty(final B... items) {
        return items == null ? true : items.length == 0;
    }

    /**
     * Realiza conversão de uma coleção para String porém utilizando labels dos
     * objetos ao invés do {@link Object#toString() toString} padrão.
     */
    public static <B> String toString(final Collection<? extends B> collectionToBeLabeled, final LabelResolver<B> labelResolver) {
        return toString(collectionToBeLabeled, labelResolver, LEFT_DELIMITER, RIGHT_DELIMITER);
    }

    public static <B> String toString(final Collection<? extends B> collectionToBeLabeled, final LabelResolver<B> labelResolver,
            final String leftDelimiter, final String rightDelimiter) {
        if (isEmpty(collectionToBeLabeled)) {
            return getEmptyCollectionToString();
        }
        StringBuilder stringBuilder = new StringBuilder(leftDelimiter);
        int index = 0;

        for (B item : collectionToBeLabeled) {
            stringBuilder.append(labelResolver.getLabel(item));

            if (!isLastElementIndex(index++, collectionToBeLabeled)) {
                stringBuilder.append(APPENDER);
            }
        }

        stringBuilder.append(rightDelimiter);
        return stringBuilder.toString();
    }

    private static boolean isLastElementIndex(final int index, final Collection<?> collection) {
        return index == collection.size() - 1;
    }

    private static String getEmptyCollectionToString() {
        return LEFT_DELIMITER + RIGHT_DELIMITER;
    }

    public static <B> String toString(final LabelResolver<B> labelResolver, final B... itemsToBeLabeled) {
        if (isEmpty(itemsToBeLabeled)) {
            return getEmptyCollectionToString();
        }
        return toString(Arrays.asList(itemsToBeLabeled), labelResolver);
    }

    public static String toString(final Collection<?> collection) {
        if (isEmpty(collection)) {
            return getEmptyCollectionToString();
        }
        return Arrays.toString(collection.toArray());
    }

    public static <T> List<T> emptyListOf(final Class<T> listClass) {
        return new ArrayList<T>();
    }
}
