package org.ldiasrs.ondegasto.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class CalculatePercentage {

    public static float calculatePercentage(final BigDecimal total, final BigDecimal value) {
        if (isTotalBiggerThanZero(total)) {
            return value.multiply(BigDecimal.valueOf(100)).divide(total, 2, RoundingMode.HALF_UP).floatValue();
        }
        throw new RuntimeException("Total must me bigger than zero value informed: " + total);
    }

    private static boolean isTotalBiggerThanZero(final BigDecimal total) {
        return BigDecimal.ZERO.compareTo(total) <= 0;
    }

}
