package org.ldiasrs.ondegasto.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Manage the number normalizations of the system.
 * @author cardoso
 */
public final class NumberNormalizer {

    private NumberNormalizer() {
    }

    public static BigDecimal normalize(final BigDecimal bigDecimal) {
        return bigDecimal.setScale(2, RoundingMode.CEILING);
    }

    public static BigDecimal zero() {
        return normalize(BigDecimal.ZERO);
    }

    public static BigDecimal normalize(final int intValue) {
        return normalize(BigDecimal.valueOf(intValue));
    }

}
