package org.ldiasrs.ondegasto.common;

/**
 * Interface para conversão de um objeto qualquer para um label que o
 * representa.
 * @author caesar.hoppen
 * @param <S>
 */
public interface LabelResolver<S> {
    String getLabel(S objectThatWeWantLabel);
}
