package org.ldiasrs.ondegasto.common;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;

public class CalculatePercentageTest {

    @Test
    public void testCalculatePercentage() {
        Assert.assertEquals(10.0f, CalculatePercentage.calculatePercentage(BigDecimal.valueOf(100), BigDecimal.valueOf(10)));
        Assert.assertEquals(5.55f, CalculatePercentage.calculatePercentage(BigDecimal.valueOf(100), BigDecimal.valueOf(5.55)));
    }
}
