package org.ldiasrs.ondegasto.common;

import junit.framework.Assert;

import org.junit.Test;

public class StringUtilTest {

    @Test
    public void testMergeStringTagsStringArrayStringString() {
        Assert.assertEquals("a,o,b,c,d,e,f,g,", StringUtil.mergeStringTags(new String[] {"á, ô,  b -, c, -, ;;;", "d, c, e, f,g" }));
    }

    @Test
    public void testHasText() {
        Assert.assertEquals(false, StringUtil.hasText(""));
        Assert.assertEquals(false, StringUtil.hasText(null));
        Assert.assertEquals(true, StringUtil.hasText("   a   "));
    }

    @Test
    public void testRemoveAcentos() {
        Assert.assertEquals("voceesta4e", StringUtil.normalizeSpecialCharsAndAccents("vocêestá4-e"));
    }

}
